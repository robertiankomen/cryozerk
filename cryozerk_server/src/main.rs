/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

#[macro_use]
extern crate lazy_static;
use std::sync::Mutex;
use std::net::IpAddr;
use lib_cryozerk_streams as cz;
use cz::{log, structs::packet_data_types::ListItem};
mod data_store;
mod structs;

/// A struct to be passed to the CryoZerk library which contains call-back functions for the library to use to notify the cryozerk_server app
/// of data arriving etc.
pub static MESSAGE_HANDLERS: cz::structs::callbacks::MessageHandlers = cz::structs::callbacks::MessageHandlers {
    on_new_connection: on_new_connection,
    on_arrived: on_arrived,
    on_update: on_update,
    on_retrieve: on_retrieve,
    on_packet_details: on_packet_details,
    on_delete: on_delete,
    on_timedout_delete: on_timedout_delete,
    on_message: on_message,
    on_list_uploads: on_list_uploads
};

/// A struct to be passed to the CryoZerk library which contains call-back functions for the library to use to notify the cryozerk_server app
/// of banning related events
pub static BANNING_HANDLERS : cz::structs::callbacks::BanningHandlers = cz::structs::callbacks::BanningHandlers {
    on_update_ban_details: banning::on_update_ban_details,
    on_check_if_banned: banning::on_check_if_banned
};

lazy_static! { 
    pub static ref UPLOADS: Mutex<Vec<structs::Data>> = Mutex::new(vec![]);
    pub static ref BANNED: std::sync::Mutex<Vec<cz::structs::banning::Ban>> = std::sync::Mutex::new(vec![]);
    pub static ref DELAY_PACKETS_BY: std::time::Duration = std::time::Duration::new(0, cz::CONFIGURATION_SERVER.delay_by_nano_seconds as u32);
}

fn main() {
     start_main();
}

/// Periodically check for active bans that have timed-out and may be deleted
pub async fn clear_bans() {
    loop {
        std::thread::sleep(std::time::Duration::new(10, 0));
        let mut banned_list = crate::BANNED.lock().unwrap();
        let mut unban_list: Vec<(IpAddr, String)> = vec![];
        for ban in banned_list.iter() {
            if cz::general::get_current_datestamp() >= ban.unban_on {
                unban_list.push((ban.ip_address, ban.access_key.to_owned()));
            }
        }
        for unban in unban_list.iter() {
            banned_list.retain(|b| b.ip_address != unban.0 && b.access_key != unban.1);
            let _ = data_store::banning::delete(unban.0, unban.1.to_owned()).await;
            let message = format!("[UNBAN] ip address: '{}', access key: '{}'", unban.0.to_string(), unban.1);
            log!(&message)
        }
    }
}

#[tokio::main]
async fn start_main() {
    unsafe{
        cz::APPLICATION_INSTANCE = cz::enums::Application::Server;
    }
     // Unban Ip addresses and access keys
     let _timedout_bans_thread = tokio::spawn( async {
        let _ = futures::executor::block_on(clear_bans());
    });

    let mut may_continue = true;
    if cz::CONFIGURATION_SERVER.persist_to_mongodb {
        // Load all data from Mongodb
        let result = data_store::files::get_all().await;
        if result.is_ok() {
            cz::display!(&format!("Retrieved {} file records", result.clone().unwrap().len()));
            let mut uploads = crate::UPLOADS.lock().unwrap();
            uploads.append(&mut result.unwrap());
            drop(uploads);
        } else {
            cz::display!("Error retrieving the data from persisted storage", result.unwrap_err());
            may_continue = false;
        }
        
        if may_continue {
            let result = data_store::banning::get_all().await;
            if result.is_ok() {
                cz::display!(&format!("Retrieved {} ban records", result.clone().unwrap().len()));
                let mut bans = crate::BANNED.lock().unwrap();
                bans.append(&mut result.unwrap());
                drop(bans);
            } else {
                cz::display!("Error retrieving the banning data from persisted storage", result.unwrap_err());
                may_continue = false;
            }
        } else {
            let message = "An error prevented the cryozerk server from executing, please check the log files";
            cz::display!(message);
            std::process::exit(1);
        }
    } else {
        cz::display!("Note: Data persistence with MongoDB not enabled in the config file");
    }

    if may_continue {
        let _ = cz::server::start_new_server(&MESSAGE_HANDLERS, &BANNING_HANDLERS, Some(*DELAY_PACKETS_BY)).await;
    }
}

/// A new connection has been made, verify the connection client_id for allowed access and return the number of bytes they have already uploaded
pub fn on_new_connection(initial_handshake: &cz::structs::InitialHandshake) -> Result<(cz::structs::configuration::ServerAccess, usize), String>{
    // Check client_id and respond if success, else just ignore and let connection die
    let option_has_access = cz::CONFIGURATION_SERVER.access_keys.iter().filter(|ak| 
        cz::general::append_vector_to_length(ak.access_key.as_bytes().to_vec(), cz::MAXIMUM_LENGTH_ACCESS_KEY) == initial_handshake.access_key).nth(0);
    
    if option_has_access.is_some() {
        //Access allowed
        let data = crate::UPLOADS.lock().unwrap();
        let mut total_bytes_already_uploaded = 0_usize;
        for upload in data.iter() {
            if upload.access_key == initial_handshake.access_key {
                total_bytes_already_uploaded = total_bytes_already_uploaded + upload.data_packet_bytes_length as usize;
            }
        }

        Ok((option_has_access.unwrap().clone(), total_bytes_already_uploaded))
    } else {
        Err(String::from("Access_key is denied access to the server"))
    }
}

/// Check if given unique code already exists in the community indicated by the access_key
fn check_if_unique_code_is_already_in_use(access_key: &Vec<u8>, unique_code: Vec<u8>) -> bool {
    let uploads = UPLOADS.lock().unwrap();
    let option_upload_found = uploads.iter().find(|&u|&u.access_key==access_key && u.unique_code==unique_code);
    
    option_upload_found.is_some()
}

/// Create a new unique code in the community indicated by the access_key
fn get_unique_code(access_key: &Vec<u8>) -> Vec<u8> {
    let uploads = UPLOADS.lock().unwrap();
    loop{ //Aaaah another beautiful infinite loop
        let unique_code = format!("{}{}{}", cz::general::get_random_word(), cz::general::get_random_alphabetic_characters(1), cz::general::get_a_random_number_in_range(100, 999)).as_bytes().to_vec();
        let option_upload_found = uploads.iter().find(|&u|&u.access_key==access_key && u.unique_code==unique_code);
        if option_upload_found.is_none() {
            return unique_code;
        }
    }
}

/// Receive the File packet bytes and store it
pub fn on_arrived(data_packet: cz::structs::DataPacket, initial_handshake: cz::structs::InitialHandshake) -> Result<cz::structs::packet_data_types::UploadResponse, String> {
    let arriving_file_data = cz::structs::packet_data_types::File::from_bytes(data_packet.clone().data)?;
    let mut unique_code: Vec<u8> = cz::general::strip_zeros(&arriving_file_data.unique_code);
    let allow_custom_unique_codes: bool = cz::CONFIGURATION_SERVER.access_keys.iter().find(|c|c.access_key==cz::general::vector_stripped_to_string(&initial_handshake.access_key) && c.allow_custom_unique_codes==true).is_some();
    if unique_code.len()==0 || !allow_custom_unique_codes { //Unique code was not supplied so generate a new one
        unique_code = get_unique_code(&initial_handshake.access_key);
    } else {
        let unique_code_vector = cz::general::strip_zeros(&unique_code);
        if check_if_unique_code_is_already_in_use(&initial_handshake.access_key, unique_code_vector.clone()) {
            //Unique code already exists so generate a new one
            unique_code = get_unique_code(&initial_handshake.access_key);
        }
    }

    if data_packet.finalised {
        cache_bytes(&unique_code, &initial_handshake.access_key, &data_packet.data)?;
        let _ = cz::files::pin_temporary_data_file(&unique_code, &initial_handshake.access_key)?;
        let result_file = cz::structs::packet_data_types::File::from_bytes(data_packet.data.to_owned());
        if result_file.is_ok() {
            let file = result_file.unwrap();
            let result_new_file = structs::Data::new(
                &file.layer,
                &file.description,
                &cz::general::get_current_datestamp(), 
                &initial_handshake.access_key.to_owned(), 
                &initial_handshake.registration_access_key.to_owned(),
                &unique_code, &file.verification_key, &file.protected_key, &cz::enums::PacketType::File, 
                &overriden_drop_on_retrieval(&file.drop_on_retrieval, &cz::general::vector_stripped_to_string(&initial_handshake.access_key)),
                &initial_handshake, 
                &data_packet.data);
            if result_new_file.is_ok() {
                let new_file = result_new_file.unwrap();
                let mut uploads = UPLOADS.lock().unwrap();
                let result = futures::executor::block_on(on_arrived_task(&new_file));

                if result.is_ok() {
                    uploads.push(new_file);
                    drop(uploads);
                    let mut will_be_deleted_on_retrieve = file.drop_on_retrieval;
                    let mut user_message = String::from("Success").as_bytes().to_vec();
                    if file.drop_on_retrieval==false {
                        if overriden_drop_on_retrieval(&file.drop_on_retrieval ,&cz::general::vector_stripped_to_string(&initial_handshake.access_key)) {
                            // Override client persistance request
                            user_message=String::from("This CryoZerk community does not allow for permanent file storage").as_bytes().to_vec();
                            will_be_deleted_on_retrieve = true;
                        } else {
                            user_message=String::from("Your uploaded file will not be automatically deleted from the server when a 'get' is done").as_bytes().to_vec();
                        }
                    }
                    let upload_response = cz::structs::packet_data_types::UploadResponse::new(cz::enums::Status::Ok, &unique_code, &user_message, will_be_deleted_on_retrieve);
                    Ok(upload_response)

                } else {
                    cz::log!("Error: ", result.unwrap_err());
                    Err("Could not store the file".into())
                }
            } else {
                cz::log!("Error: ", result_new_file.unwrap_err());
                Err("Could not generate an unique code".into())
            }
        } else {
            cz::log!("Error: ", result_file.unwrap_err());
            Err("Could not extract the file data".into())
        }
    } else {
        cache_bytes(&unique_code, &initial_handshake.access_key, &data_packet.data)?;

        Ok(cz::structs::packet_data_types::UploadResponse::new_empty())
    }
}


/// Does this server allow for permanent storage of uploads?
/// See also client command-line argument `-x`
fn overriden_drop_on_retrieval(drop_on_retrieval_client_request: &bool, access_key: &str) -> bool {
    if *drop_on_retrieval_client_request==false {
        return !cz::CONFIGURATION_SERVER.access_keys.iter().find(|a|a.access_key==access_key && a.allow_extended_storage).is_some()
    } else {
        return true;
    }
}

/// Cache the file bytes. This will be used as the final files data contents too.
fn cache_bytes(unique_code: &Vec<u8>, access_key: &Vec<u8>, bytes_to_cache: &Vec<u8>) -> Result<(), String> {
    let path_to_file = cz::files::get_path_to_temporary_data_file(&unique_code, &access_key)?;
    if cz::files::write_to_file(&path_to_file, &bytes_to_cache, true) {
        Ok(())
    } else {
        cz::log!("Could not create temporary cache file for arriving bytes", path_to_file);
        Err("Server cache write failed".into())
    }
}

/// Replace an existing upload with the new File packet bytes
pub fn on_update(data_packet: cz::structs::DataPacket, initial_handshake: cz::structs::InitialHandshake) -> Result<cz::structs::packet_data_types::UploadResponse, String> {
    let result_new_file = cz::structs::packet_data_types::File::from_bytes(data_packet.data.to_owned());
    if result_new_file.is_ok() {
        let arriving_file_data = cz::structs::packet_data_types::File::from_bytes(data_packet.clone().data)?;
        let unique_code = arriving_file_data.unique_code;
        if data_packet.finalised {
            let file = result_new_file.unwrap();
            let verification_key = file.verification_key;
            let stripped_unique_code = cz::general::strip_zeros(&file.unique_code);
            let stripped_verification_key = cz::general::strip_zeros(&verification_key.to_owned());
            let serialised_verification_key = serde_json::to_string(&stripped_verification_key).unwrap();
            let uploads = UPLOADS.lock().unwrap();
            let option_upload_found = uploads.iter().find(|&u|u.access_key==initial_handshake.access_key && u.verification_key==serialised_verification_key && u.unique_code==stripped_unique_code);
        
            if option_upload_found.is_some() {
                let found_upload = option_upload_found.unwrap();
                let stripped_protected_key = cz::general::strip_zeros(&arriving_file_data.protected_key);
                let protected_key = found_upload.protected_key.clone();
                if protected_key.len() == 0 || protected_key == serde_json::to_string(&stripped_protected_key).unwrap() {
                    cache_bytes(&unique_code, &initial_handshake.access_key, &data_packet.data)?;
                    let _ = cz::files::pin_temporary_data_file(&unique_code, &initial_handshake.access_key)?;
                    let will_be_deleted_on_retrieve = found_upload.drop_on_retrieval;
                    let user_message = String::from("Success").as_bytes().to_vec();
                    
                    let upload_response = cz::structs::packet_data_types::UploadResponse::new(cz::enums::Status::Ok, &unique_code, &user_message, will_be_deleted_on_retrieve);
                    Ok(upload_response)
                } else {
                    cz::log!("Error: The protection key validation failed");
                    Err("The protection key validation failed".into())
                }
                
            } else {
                Err("Could not find the file to replace".into())
            }
        } else {
            cache_bytes(&unique_code, &initial_handshake.access_key, &data_packet.data)?;

            Ok(cz::structs::packet_data_types::UploadResponse::new_empty())
        }
    } else {
        cz::log!("Error: ", result_new_file.unwrap_err());
        Err("Could not extract the file data".into())
    }
}

pub async fn on_arrived_task(new_file: &crate::structs::Data) -> Result<(), String> { 
    if cz::CONFIGURATION_SERVER.persist_to_mongodb {
        let result_added = crate::data_store::files::add(&new_file).await;
        if result_added.is_ok() {
            Ok(())
        } else {
            Err(result_added.unwrap_err())
        }
    } else {
        Ok(())
    }
}

/// Retrieve a File with the supplied unique code
pub fn on_retrieve(_new_packet_symmetric_encryption_key: Vec<u8>, access_key: Vec<u8>, unique_code: Vec<u8>, verification_key: Vec<u8>, protected_key: Vec<u8>) -> Result<(String, Vec<u8>), String> {
    if verification_key.len() == 0 { return Err("Access was denied".into()); }
    let uploads = UPLOADS.lock().unwrap();
    let serialised_verification_key = serde_json::to_string(&verification_key).unwrap();
    let option_upload_found = uploads.iter().find(|&u| u.access_key==access_key && u.verification_key==serialised_verification_key && u.unique_code==unique_code);
    let original_packet_symmetric_encryption_key: Vec<u8>;

    if option_upload_found.is_some() {
        let data = option_upload_found.unwrap();
        let data_bytes: Vec<u8>;
        let result = cz::files::read_from_file(&data.file_name);
        if result.is_some() {
            original_packet_symmetric_encryption_key = data.handshake.packet_symmetric_encryption_key.clone();
            data_bytes = result.unwrap();
        } else {
            return Err(format!("Could not retrieve file bytes for access_key: {}, unique_code: {}", 
            cz::general::vector_stripped_to_string(&access_key), 
            cz::general::vector_stripped_to_string(&unique_code)))
        }
        let deletion_message: String;
        if data.drop_on_retrieval {
            let stripped_protected_key = cz::general::strip_zeros(&protected_key);
            let protected_key = data.protected_key.clone();
            drop(uploads);
            if protected_key.len() == 0 || protected_key == serde_json::to_string(&stripped_protected_key).unwrap() {
                let _ = on_delete(access_key, unique_code, verification_key, stripped_protected_key);
                deletion_message="File deleted from server".into();
            } else {
                deletion_message="File not deleted from server due to additional protection password".into();
            }
        } else {
            deletion_message="File not deleted from server".into();
        }
        let data_bytes_length = data_bytes.len();
        let unscrambled_bytes = lib_cryozerk_streams::encryption::scrambler(&false, data_bytes, original_packet_symmetric_encryption_key, data_bytes_length);
        return Ok((deletion_message, unscrambled_bytes.to_owned()));
    } else {
        return Err(format!("No data found for access_key: {}, unique_code: {}", 
        cz::general::vector_stripped_to_string(&access_key), 
        cz::general::vector_stripped_to_string(&unique_code)));
    };
}

/// Retrieve a list of uploads available for the specified access_key and optional layer
pub fn on_list_uploads(access_key: &Vec<u8>, layer: &Vec<u8>) -> Result<cz::structs::packet_data_types::ListUploads, String> {
    let access_key_string = cz::general::vector_stripped_to_string(access_key);
    let layer_string = cz::general::vector_stripped_to_string(layer);
    let option_server_access = cz::CONFIGURATION_SERVER.access_keys.iter().find(|a|a.access_key==access_key_string);

    if option_server_access.is_some() {
        let server_access = option_server_access.unwrap();
        let mut items_found = cz::structs::packet_data_types::ListUploads::new(&access_key, &layer, &vec![]);
        let uploads = UPLOADS.lock().unwrap();

        let c = |u: &crate::structs::Data| {
            if u.access_key==*access_key {
                if layer_string.len() == 0 || layer_string=="ALL_LAYERS" {
                    return true;
                } else if layer_string=="/" {
                    return u.layer=="/";
                } else if layer_string.ends_with("/") {
                    let mut inner_layer_string = layer_string.to_owned();
                    let _ = inner_layer_string.remove(layer_string.len()-1);
                    return u.layer.starts_with(&inner_layer_string)
                } else {
                    return u.layer == layer_string;
                };
            }

            false
        };
        let mut iter_uploads_found = uploads.iter().filter(|&u|c(u));
        let mut items_counter: usize = 0;
        loop {
            let option_item = iter_uploads_found.next();
            if option_item.is_some() {
                let data_item = option_item.unwrap();
                let is_protected = data_item.protected_key.len() > 0;
                let new_list_item = cz::structs::packet_data_types::ListItem::new(data_item.unique_code.to_vec(), is_protected, data_item.layer.as_bytes().to_vec(), data_item.description.as_bytes().to_vec(), data_item.drop_on_retrieval, 0, data_item.creation_time_stamp, server_access.automatic_deletion_after_seconds, data_item.data_packet_bytes_length);
                items_counter+=1;
                if layer_string == "ALL_LAYERS" && items_counter > 50 
                { 
                    //Do not return more than 50 items if iterating over ALL layers
                    let empty_list_item: ListItem = ListItem::new_empty(data_item.data_packet_bytes_length);
                    items_found.list.push(empty_list_item);
                } else {
                    items_found.list.push(new_list_item);
                } 
            } else {
                break;
            }
        }
        drop(uploads);

        Ok(items_found)
    } else {
        Err("Access key denied".into())
    }
}

/// Retrieve a previously uploaded File's details
pub fn on_packet_details(access_key: Vec<u8>, unique_code: Vec<u8>) -> Result<cz::structs::packet_data_types::PacketDetails, String> {
    let uploads = UPLOADS.lock().unwrap();
    let option_upload_found = uploads.iter().find(|&u|u.access_key==access_key && u.unique_code==unique_code);
    if option_upload_found.is_some() {
        let file = option_upload_found.unwrap();
        let packet_details = cz::structs::packet_data_types::PacketDetails::new(file.data_packet_bytes_length, file.handshake.enable_compression);
        drop(uploads);
        return Ok(packet_details);
    } else {
        return Err(format!("No data found for access_key: {:?}, unique_code: {:?}", &access_key, &unique_code));
    };
}

/// Delete a File data packet with the supplied unique code and access_key
pub fn on_delete(access_key: Vec<u8>, unique_code: Vec<u8>, verification_key: Vec<u8>, protected_key: Vec<u8>) -> Option<String> {
    if verification_key.len() == 0 { 
        return None; 
    } else {
        let serialised_verification_key = serde_json::to_string(&cz::general::strip_zeros(&verification_key.to_owned())).unwrap();
        let mut serialised_protected_key = serde_json::to_string(&cz::general::strip_zeros(&protected_key.to_owned())).unwrap();
        if serialised_protected_key == String::from("[]") { serialised_protected_key = String::new() };

        do_delete(access_key, unique_code, &serialised_verification_key, &serialised_protected_key)
    }
}

fn do_delete(access_key: Vec<u8>, unique_code: Vec<u8>, serialised_verification_key: &str, serialised_protected_key: &str) -> Option<String> {
    let mut uploads = UPLOADS.lock().unwrap();
    let option_upload_found = uploads.iter().find(|&u|u.access_key==access_key && u.verification_key==serialised_verification_key && (u.protected_key==serialised_protected_key || u.protected_key==String::new()) && u.unique_code==unique_code);
    if option_upload_found.is_some() {
        let mut deleted = false;
        if cz::CONFIGURATION_SERVER.persist_to_mongodb {
            deleted = futures::executor::block_on(data_store::files::delete(&access_key, &unique_code, &serialised_verification_key));
        }
        if deleted {
            if serialised_verification_key.len() > 0 {
                // ! We should check that the delete actually succeeded!
                uploads.retain(|u| !(u.verification_key==serialised_verification_key && (u.protected_key==serialised_protected_key || u.protected_key==String::new()) && u.access_key==access_key && u.unique_code==unique_code));
            } else {
                uploads.retain(|u| !(u.access_key==access_key && u.unique_code==unique_code));
            }
            drop(uploads);

            //Delete from disk
            let file_name_with_path = crate::cz::files::get_path_to_data_file(&unique_code, &access_key);
            let file_exists = file_name_with_path.unwrap();
            if !crate::cz::files::delete_file(&file_exists) {
                crate::cz::log!("Could not delete file from disk, orphaned", file_exists);
                // Dont return error though, clean up orphaned files in some other process
            }

            None
        } else {
            Some(String::from("Error when trying to delete"))
        }
    } else {
        Some(String::from("Nothing deleted"))
    }
}

/// Delete data packets for a specific access_key that have timed-out
pub fn on_timedout_delete(access_key: Vec<u8>, timeout_seconds: &i64) -> Option<String> {
    let uploads = UPLOADS.lock().unwrap();
    let mut to_delete: Vec<(Vec<u8>, Vec<u8>)> = vec![];
    // Collect items to delete
    for data in uploads.iter() {
        if data.access_key==access_key && data.creation_time_stamp.signed_duration_since(cz::general::get_current_datestamp()) > chrono::Duration::seconds(*timeout_seconds) {
            to_delete.push((data.access_key.to_owned(), data.unique_code.to_owned()));
        }
    }

    // Delete them
    for tuple in to_delete.iter() {
        if do_delete(tuple.0.to_owned(), tuple.1.to_owned(), "", "").is_none() {
            crate::log!(&format!("Error deleting for time-out, access_key: {:?}, unique_code: {:?}", tuple.0, tuple.1));
        }
    }

    drop(uploads);

    Some("Complete".into())
}

/// A normal message has arrived
pub fn on_message(_access_key: &Vec<u8>, message: &cz::structs::packet_data_types::Message) -> Result<(String, Vec<u8>), String> {
    if message.bytes.len()==3 {
        crate::log!("Upgrade request from version", message.bytes);
        let option_upgrade_info = cz::CONFIGURATION_SERVER.files_for_upgrade.iter().find(|&u|
            u.version_major == message.bytes[0] &&
            u.version_minor == message.bytes[1] &&
            u.version_revision > message.bytes[2]
        );
        if option_upgrade_info.is_some() {
                let upgrade_info = option_upgrade_info.unwrap();
                let option_bytes = cz::files::read_from_file(&upgrade_info.file_name);
                if option_bytes.is_some() {
                let mut new_file_name: Vec<u8> = vec![];
                new_file_name.append(&mut upgrade_info.version_major.to_string().as_bytes().to_vec());
                new_file_name.push(b'.');
                new_file_name.append(&mut upgrade_info.version_minor.to_string().as_bytes().to_vec());
                new_file_name.push(b'.');
                new_file_name.append(&mut upgrade_info.version_revision.to_string().as_bytes().to_vec());
                new_file_name.push(b'-');
                new_file_name.append(&mut upgrade_info.file_name.as_bytes().to_vec());
                crate::log!("Sending upgrade file:", String::from(std::str::from_utf8(&new_file_name).unwrap()));
                let file_bytes_to_send =cz::structs::packet_data_types::File::new(&option_bytes.unwrap(), std::str::from_utf8(&new_file_name).unwrap(), "","", true, &vec![], &vec![], &vec![], false).into_bytes().unwrap();
                return Ok((String::from(std::str::from_utf8(&new_file_name).unwrap()), file_bytes_to_send));
            } else {
                return Err("A server error occured when trying to access the upgrade file".into());
            }
        } else {
            return Err("Server response: You are on the latest version already".into());
        }
    } else {
        return Err("Server response: A client upgrade was requested but the sent version data was not in the required format".into());
    }
}

mod banning {
    use std::net::IpAddr;
    use chrono::{Duration};
    use lib_cryozerk_streams as cz;
    use super::log;

    /// A failed connection was attempted so check to see if an ip address or access key is in the ban list and update its details accordingly
    pub fn on_update_ban_details(ip_address: &IpAddr, access_key: &Option<Vec<u8>>) {
        let new_access_key = match access_key.is_some() {
            true => cz::general::vector_stripped_to_string(&access_key.to_owned().unwrap()),
            false => String::new()
        };
        let banned_list= &mut crate::BANNED.lock().unwrap();
        let option_found_ban = banned_list.iter_mut().find(|b|b.ip_address == *ip_address || b.access_key == new_access_key);

        if option_found_ban.is_none() {
            let new_ban = cz::structs::banning::Ban::new(ip_address.clone(), access_key.clone(), cz::general::get_current_datestamp() + Duration::seconds(cz::CONFIGURATION_SERVER.first_ban_time_in_seconds as i64));
            banned_list.push(new_ban.to_owned());
            drop(banned_list);
            let _ = futures::executor::block_on(crate::data_store::banning::add(new_ban.to_owned()));

            return;
        }

        let mut update_db: bool = false;
        let banned = option_found_ban.unwrap();
        let mut message = format!("[BAN COUNTER] '{}' is being monitored for possible banning", banned.ip_address.to_string());
        banned.counter += 1;
        if banned.counter > (cz::CONFIGURATION_SERVER.ban_after_handshake_fails_count_for_ip_address as f32 * 1.25) as i32 {
            banned.times_banned += 1;
            banned.unban_on = cz::general::get_current_datestamp() + Duration::seconds(cz::CONFIGURATION_SERVER.second_ban_time_in_seconds as i64);
            update_db = true;
            message = format!("[BAN] Level 2: '{}' has exceeded its' limitation on unsuccessful connection attempts", banned.ip_address.to_string());
        } else if banned.counter > cz::CONFIGURATION_SERVER.ban_after_handshake_fails_count_for_ip_address {
            banned.times_banned += 1;
            banned.unban_on = cz::general::get_current_datestamp() + Duration::seconds(cz::CONFIGURATION_SERVER.first_ban_time_in_seconds as i64);
            update_db = true;
            message = format!("[BAN] '{}' has exceeded its' limitation on unsuccessful connection attempts", banned.ip_address.to_string());
        }
        if update_db {
            let _ = futures::executor::block_on(crate::data_store::banning::update(banned.to_owned()));
        }

        drop(banned_list);

        log!(&message);
    }

    /// Check if a ban has been applied to the current Ip address or access_key
    pub fn on_check_if_banned(ip_address: &IpAddr, access_key: &Option<Vec<u8>>) -> bool {
        let is_banned;
        {
            let new_access_key = match access_key.is_some() {
                true => cz::general::vector_stripped_to_string(&access_key.to_owned().unwrap()),
                false => String::new()
            };
            let banned_list= &mut crate::BANNED.lock().unwrap();
            is_banned = banned_list.iter_mut().find(|b|b.times_banned > 0 && (b.ip_address == *ip_address || b.access_key == new_access_key)).is_some();
            drop(banned_list);
        }

        if is_banned {
            // Update more ban setting details
            on_update_ban_details(&ip_address, &access_key);
            std::thread::sleep(std::time::Duration::from_millis(1000));
        }

        is_banned
    }
}