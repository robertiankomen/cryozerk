/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

use mongodb::{Client, options::ClientOptions};
pub mod files;
pub mod banning;

/// Connect to mongodb instance
async fn connect() -> Result<mongodb::Database, String> {
    let result_client_options = ClientOptions::parse(&lib_cryozerk_streams::CONFIGURATION_SERVER.mongodb_connection_string).await;
    if result_client_options.is_ok() {
        let mut client_options = result_client_options.unwrap();
        client_options.app_name = Some("cryozerk_App".to_string());
        let result_client = Client::with_options(client_options);
        if result_client.is_ok() {
            let client = result_client.unwrap();
            let db = client.database("cryozerk_db");

            Ok(db)
        } else {

            Err("Could not create the app".into())
        }
    } else {

        Err("Database client options settings gave an error".into())
    }
}