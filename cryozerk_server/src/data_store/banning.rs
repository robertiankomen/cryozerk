/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

use serde::{Serialize, Deserialize};
use mongodb::{bson, bson::{doc, Bson}};
use std::net::IpAddr;
use chrono::{DateTime, Local};
use futures::stream::StreamExt;
use lib_cryozerk_streams as cz;

const COLLECTION_NAME: &str = "banning";

#[derive(Serialize, Deserialize, Debug)]
struct Ban {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    id: Option<bson::oid::ObjectId>,
    pub ip_address: IpAddr,
    pub access_key: String,
    pub times_banned: i32,
    pub counter: i32,
    pub unban_on: DateTime<Local>
}


pub async fn get_all() -> Result<Vec<cz::structs::banning::Ban>, String> {
    let database = super::connect().await.unwrap();
    let banning = database.collection(COLLECTION_NAME);
    let mut cursor = banning.find(None, None).await.unwrap();
    let mut bans: Vec<cz::structs::banning::Ban> = vec![];
    while let Some(ban_item) = cursor.next().await {
        let retreived_ban: Ban = bson::from_bson(Bson::Document(ban_item.unwrap())).unwrap();
        let ban = cz::structs::banning::Ban {
            access_key: retreived_ban.access_key,
            counter: retreived_ban.counter,
            ip_address: retreived_ban.ip_address,
            times_banned: retreived_ban.times_banned,
            unban_on: retreived_ban.unban_on
        };
        bans.push(ban);
    }

    Ok(bans)
}

/// Add ban to mongodb
pub async fn add(new_ban: lib_cryozerk_streams::structs::banning::Ban) -> Result<(), String> {
    let database = super::connect().await.unwrap();
    let client_files = database.collection(COLLECTION_NAME);
    let serialised = serde_json::to_string(&new_ban).unwrap();
    let result_serialised_data =  bson::to_bson(&serialised);
    if result_serialised_data.is_ok() {
        let files_document = Ban {
            id: None,
            access_key: new_ban.access_key.to_owned(),
            counter: new_ban.counter.to_owned(),
            ip_address: new_ban.ip_address.to_owned(),
            times_banned: new_ban.times_banned,
            unban_on: new_ban.unban_on
        };
        let result_serialised_data = bson::to_bson(&files_document);
        if result_serialised_data.is_ok() {
            let serialised_data = result_serialised_data.unwrap();
            let option_document = serialised_data.as_document();
            if option_document.is_some() {
                let result = client_files.insert_one(option_document.unwrap().to_owned(), None).await;
                if result.is_ok() {
                    return Ok(())
                } else {
                    return Err("Error adding ban to persisted storage".into());
                }
            } else {
                return Err("Error adding ban to persisted storage, document serialisation failed".into());
            };
        } else {
            return Err(format!("(Error adding ban to persisted storage: {}", result_serialised_data.unwrap_err()));
        }
    } else {
        Err(format!("Error adding ban to persisted storage: {:?}", result_serialised_data.unwrap_err()))
    }
}

pub async fn update(updated_ban: cz::structs::banning::Ban) -> bool {
    let database = super::connect().await.unwrap();
    let client_files = database.collection(COLLECTION_NAME);
    let serialised = serde_json::to_string(&updated_ban).unwrap();
    let result_serialised_data =  bson::to_bson(&serialised);
    if result_serialised_data.is_ok() {
        let files_document = Ban {
            id: None,
            access_key: updated_ban.access_key.to_owned(),
            counter: updated_ban.counter.to_owned(),
            ip_address: updated_ban.ip_address.to_owned(),
            times_banned: updated_ban.times_banned,
            unban_on: updated_ban.unban_on
        };
        let result_serialised_data = bson::to_bson(&files_document);
        if result_serialised_data.is_ok() {
            let serialised_data = result_serialised_data.unwrap();
            let option_document = serialised_data.as_document();
            if option_document.is_some() {
                let result = client_files.find_one_and_update(
                doc! { 
                    "access_key": updated_ban.access_key.to_owned(),
                    "ip_address": updated_ban.ip_address.to_string()
                },
                doc!{"$set": option_document.unwrap().to_owned()},
                None).await;

                if result.is_err() {
                    crate::log!("Could not update ban in data store: {}", result.unwrap_err());
                    return false;
                }
            } else {
                return false;
            }

            true
        } else {
            false
        }
    } else {
        false
    }
}

/// Delete a file from database and disk
pub async fn delete(ip_address: IpAddr, access_key: String) -> bool {
    let database = super::connect().await.unwrap();
    let bans = database.collection(COLLECTION_NAME);
    let result = bans.delete_one(
        doc! { 
            "access_key": &access_key.to_owned(),
            "ip_address": ip_address.to_string()
        }
        ,None).await;
    
    if result.is_err() {
        crate::log!("Could not delete ban from data store");
        false
    } else {
        true
    }
}