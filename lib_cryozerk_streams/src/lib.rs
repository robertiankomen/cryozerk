/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD
    General Public License along with this program. If not,
    see <https://www.dimension15.co.za> under the licenses section.
*/
//! An implementation for secure file and message transfers using AES and RSA handshaking.
//!
//! This is the processing centre of CryoZerk and CryoZerk Server

extern crate bincode;
extern crate flate2;
extern crate regex;
// extern crate progress;
extern crate rand;
extern crate rsa;
extern crate serde_json;
extern crate tokio;
#[macro_use]
extern crate lazy_static;

pub mod encryption;
pub mod enums;
pub mod files;
pub mod general;
pub mod log;
pub mod streams;
pub mod structs;
pub mod tests;
pub mod traits;
pub mod server;
pub mod client;

pub const VERSION_INFO: &str = "3.1.4";

pub const DELAY_INITIAL_STREAM_CALL_MILLIS: u64 = 250;

pub const PROGRESSBAR_DISPLAY_THRESHOLD_BYTES_COUNT: usize = 512;
pub const CONCURRENT_PROCESSES_COUNT: usize = 8; // Arbitrary value, feel free to change, but make sure the server **!!AND!!** client are compiled with this same value.

pub const MAXIMUM_LENGTH_DATA_PACKET: u64 = std::u64::MAX;
pub const MAXIMUM_LENGTH_RSA_4096_PUBLIC_KEY: usize = 512;
pub const MAXIMUM_LENGTH_RSA_4096_P2P_SECRET: usize = 512;
pub const MAXIMUM_LENGTH_FILE_NAME: usize = 250;
pub const MAXIMUM_LENGTH_LAYER: usize = 100;
pub const MAXIMUM_LENGTH_ACCESS_KEY: usize = 35;
pub const MAXIMUM_LENGTH_P2P_KEY_NAME: usize = 35;
pub const MAXIMUM_LENGTH_REGISTRATION_KEY: usize = 45;
pub const MINIMUM_LENGTH_REGISTRATION_KEY: usize = 5;
/// Must be >= MAXIMUM_LENGTH_ACCESS_KEY and <= LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY
pub const MAXIMUM_LENGTH_VERIFICATION_KEY: usize = 128;
pub const MAXIMUM_LENGTH_DESCRIPTION_FIELD: usize = 100;
pub const MAXIMUM_LENGTH_OF_UNIQUE_CODE: usize = 20;

const MAXIMUM_LENGTH_HANDSHAKE_PACKET: u16 = 5*1024;
const MAXIMUM_LENGTH_SESSION_KEY: u8 = 128;
const MAXIMUM_PACKET_LENGTH: usize = 1024*512;
/// 32-bits
const _CRC_BYTES_LENGTH: usize = 4;

pub const LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY: usize = 256;
pub const MINIMUM_LENGTH_OF_USER_PASSWORD: usize = 12;
const LENGTH_OF_P2P_SECRET: usize = 256;
pub const LENGTH_OF_ENCRYPTION_SALT: usize = 64;
pub const LENGTH_OF_DATESTAMP_FIELD: u16 = 38;
const LENGTH_OF_U64_NUMBER_IN_BYTES: usize = 8;
const LENGTH_OF_U32_NUMBER_IN_BYTES: usize = 4;
const LENGTH_OF_U16_NUMBER_IN_BYTES: usize = 2;
const LENGTH_OF_DATA_PACKET_TYPE_IN_BYTES: usize = 1;
//const LENGTH_OF_FINALISED_IN_BYTES: usize = 1;
const LENGTH_OF_DATA_PACKET_HEADER: usize = LENGTH_OF_U64_NUMBER_IN_BYTES + LENGTH_OF_DATA_PACKET_TYPE_IN_BYTES;
/// Run in a less verbose message reporting mode
pub static mut QUIET_MODE: u8 = 1;

/// This gets set and overriden by the client and server applications accordingly
pub static mut APPLICATION_INSTANCE: enums::Application = enums::Application::Server;
pub static mut ENABLE_LOGGING: bool = true;
/// Disable all persisted logging
pub static mut DARK_MODE: bool = false;

lazy_static! {
    pub static ref LABEL_HANDSHAKE: String = "Handshake".into();
    pub static ref LABEL_SENDING: String = "Send".into();
    pub static ref LABEL_RECEIVING: String = "Receive".into();
    pub static ref LABEL_ENCRYPTING: String = "Encrypt".into();
    pub static ref LABEL_DECRYPTING: String = "Decrypt".into();
    pub static ref LABEL_COMPRESSING: String = "Compress".into();
    pub static ref LABEL_DECOMPRESSING: String = "Decompress".into();
    pub static ref BENCHMARKS_PROGRESS_BAR: crate::structs::progress_bar::ProgressBar = crate::structs::progress_bar::ProgressBar::new("", 0_f32);

    #[derive(Debug)]
    pub static ref LOG_FILE_NAME: String = "cryozerk.log".into();
    #[derive(Debug)]
    pub static ref RSA_KEYS_FILE_NAME_SERVER: String = "cz_rsa_server.json".into();
    #[derive(Debug)]
    pub static ref RSA_KEYS_FILE_NAME_CLIENT_PRIVATE: String = "cz_rsa_client_private.json".into();
    #[derive(Debug)]
    pub static ref RSA_KEYS_FILE_NAME_CLIENT: String = "cz_rsa_client.json".into();
    #[derive(Debug)]
    pub static ref CONFIG_FILE_NAME_SERVER: String = "cz_server.json".into();
    #[derive(Debug)]
    pub static ref CONFIG_FILE_NAME_CLIENT: String = "cz_client.json".into();
    #[derive(Debug)]
    pub static ref BENCHMARK_FILE_NAME: String = "cz_benchmarks.json".into();

    /// Servers' public and private RSA key pairs
    pub static ref RSA_KEY_PAIRS_SERVER: structs::encryption::RSAKeyPairs = files::get_rsa_keys_server();

    /// Clients' public and private RSA key pairs as well as the public keys of other clients for secure encryption specific to a client
    pub static ref RSA_KEY_PAIRS_CLIENT_PRIVATE: structs::encryption::PrivateCommunicationRSAKeyPairs = files::get_rsa_keys_client_private();

    /// Public server RSA keys used by a client to communicate with a server
    pub static ref RSA_KEY_PAIRS_CLIENT: structs::encryption::AllRSAKeyPairs = files::get_rsa_keys_client();

    /// Configuration settings for a server
    pub static ref CONFIGURATION_SERVER: structs::configuration::Server = files::get_server_configuration();

    /// Configuration settings for a client
    pub static ref CONFIGURATION_CLIENT: structs::configuration::Client = files::get_client_configuration();

    /// Benchmark values to predict future encryption and compression speeds
    pub static ref BENCHMARKS: std::sync::Mutex<structs::Benchmarks> = files::get_benchmarks();
}
