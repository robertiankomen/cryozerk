/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! All traits used by the system reside here.

use crate::trace;

/// Interface for objects performing byte serialisation
pub trait Serialisable {
    /// Serialise into bytes
    fn into_bytes(&mut self) -> Result<Vec<u8>, String>;

    /// Serialise from bytes
    fn from_bytes<T>(bytes: &Vec<u8>) -> Result<T, String>;
}

/// Serialise or deserialites bytes and objects
pub struct ByteSerialiser {}
impl ByteSerialiser {
    /// Serialise an object into bytes
    pub fn into<T: serde::Serialize>(object: &T) -> Result<Vec<u8>, String> {
        let result_serialisation = bincode::serialize(&object);
        if result_serialisation.is_ok() {
            let serialised = result_serialisation.unwrap();
            trace!(&format!("Serialised: {:?}, {}", serialised, serialised.len()));
            Ok(serialised)
        } else {
            Err(String::from("Could not serialise object to bytes"))
        }
    }

    /// Deserialse bytes into an object
    pub fn from<T: serde::de::DeserializeOwned>(bytes_to_deserialise: &Vec<u8>) -> Result<T, String> {
        let result_deserialisation = bincode::deserialize::<T>(&bytes_to_deserialise[..]).unwrap();
        Ok(result_deserialisation) // ! TODO : Check for a result if failed
    }
}

/// A handler that data will be sent to as it arrives from a stream
pub trait BytesReceiveHandler {
    fn receive_bytes(&mut self, bytes: &mut Vec<u8>);
    fn get_buffer_with_received_data(&self) -> &Vec<u8>;
    fn get_expected_length(&self) -> usize;
    fn get_current_length(&self) -> usize;
    fn receive_complete(&mut self);
}

/// A simple monitor for progress of a data transfer
pub trait SimpleProgressMonitor {
    fn set_maximum_value(&mut self, value: u64);
    fn set_current_value(&mut self, value: u64);
    fn get_current_progress_percentage_float(&self) -> f32;
    fn get_current_progress_percentage_integer(&self) -> u8;
    fn completed(&mut self);
}

/// A monitor for progress of a data transfer
pub trait ProgressMonitor: SimpleProgressMonitor {
    fn new<T: ProgressMonitorReceiver>(label: &str, progress_monitor_receiver: &T);
}

/// An object that gets notified of changes from a `ProgressMonitor` and then indicates the percent change to a handler
pub trait ProgressMonitorReceiver {
    fn set_current_percentage(&mut self, value: f32);
    fn completed(&mut self);
}
