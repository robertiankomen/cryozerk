/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

/// Methods to be used to serialise a struct
#[derive(Copy, Clone, PartialEq, Debug)]
#[repr(u8)]
pub enum MaxFieldLength {
    U8 =1,
    U16=2,
    U32=4,
    U64=8
}
impl MaxFieldLength {
    pub fn from(indicator: u8) -> Result<MaxFieldLength, String> {
        match indicator {
            1 => Ok(MaxFieldLength::U8),
            2 => Ok(MaxFieldLength::U16),
            4 => Ok(MaxFieldLength::U32),
            8 => Ok(MaxFieldLength::U64),
            _ => Err("Invalid field length indicator".into())
        }
    }

    pub fn cast_length(&self, length: usize) -> Vec<u8> {
        match self {
            MaxFieldLength::U8 => (length as u8).to_be_bytes().to_vec(),
            MaxFieldLength::U16 => (length as u16).to_be_bytes().to_vec(),
            MaxFieldLength::U32 => (length as u32).to_be_bytes().to_vec(),
            MaxFieldLength::U64 => (length as u64).to_be_bytes().to_vec()
        }
    }
}

pub struct Serialise {
    serialised_fields: Vec<u8>,
    /// A description to show in errors
    description: String
}
impl Serialise {
    pub fn new(description: &str) -> Serialise {
        Serialise {
            serialised_fields: vec![],
            description: description.into()
        }
    }

    pub fn add_field(&mut self, max_field_length: MaxFieldLength, mut bytes: &mut Vec<u8>)-> Result<(), String> {
        let max_length: usize = 2_usize.pow(8*max_field_length as u32) as usize - 1;
        if bytes.len() <= max_length {
            self.serialised_fields.append(&mut (max_field_length as u8).to_be_bytes().to_vec());
            self.serialised_fields.append(&mut max_field_length.cast_length(bytes.len()));
            self.serialised_fields.append(&mut bytes);
            Ok(())
        } else {
            Err(format!("Serialisation error '{}', max bytes length allowed is: {}", self.description, 2_usize.pow(8*max_field_length as u32) as usize - 1))
        }
    }

    pub fn into_bytes(&self) -> Result<Vec<u8>, String> {
        if self.serialised_fields.len() as u64 + crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64 <= std::u64::MAX {
            let mut length = (self.serialised_fields.len() + crate::LENGTH_OF_U64_NUMBER_IN_BYTES).to_be_bytes().to_vec();
            let mut new_vec: Vec<u8> = vec![];

            new_vec.append(&mut length);
            new_vec.extend(self.serialised_fields.iter());

            Ok(new_vec)
        } else {

            Err(format!("Serialisation error '{}', max length of fully serialised structure allowed is: 2^64", self.description))
        }
    }
}

/// Methods to be used to deserialise a struct
pub struct Deserialise {
    serialised_fields: Vec<u8>,
    current_field_pointer: usize,
    /// A description to show in errors
    description: String
}
impl Deserialise {
    pub fn new(serialised_fields: &Vec<u8>, description: &str) -> Deserialise {
        Deserialise {
            serialised_fields: serialised_fields.to_owned(),
            current_field_pointer: crate::LENGTH_OF_U64_NUMBER_IN_BYTES,
            description: description.into()
        }
    }

    pub fn extract_field(&mut self)-> Result<Vec<u8>, String> {
        if self.current_field_pointer <= self.serialised_fields.len() {
            let field_length_holder = MaxFieldLength::from(self.serialised_fields[self.current_field_pointer])? as usize;
            self.current_field_pointer += 1;

            let mut bytes = self.serialised_fields[self.current_field_pointer..self.current_field_pointer + field_length_holder].to_vec();
            let length_bytes = Deserialise::prefix_to(crate::LENGTH_OF_U64_NUMBER_IN_BYTES, &mut bytes);
            let field_bytes_length = crate::general::get_64bit_number_from(&length_bytes) as usize;
            self.current_field_pointer += field_length_holder;
            
            let extracted_field: Vec<u8> = self.serialised_fields[self.current_field_pointer..self.current_field_pointer + field_bytes_length as usize].to_vec();
            self.current_field_pointer += field_bytes_length;

            Ok(extracted_field)
        } else {
            Err(format!("Deserialisation error: {} - bytes: {}, current pointer: {}", self.description, self.serialised_fields.len(), self.current_field_pointer))
        }
    }

    fn prefix_to(field_length: usize, bytes: &mut Vec<u8>) -> Vec<u8> {
        let mut return_bytes: Vec<u8> = vec![];
        for _ in 0..field_length-bytes.len() {
            return_bytes.push(0);
        }

        return_bytes.append(bytes);

        return_bytes
    }

}