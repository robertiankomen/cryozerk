/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Structs which handle the receiving and storage of RSA keys

use crate::encryption;
use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RSAKeyPair {
    pub name: String,
    pub private_key: Vec<u8>,
    pub public_key: Vec<u8>,
}
impl RSAKeyPair {
    /// Generate a new empty RSA key pair
    pub fn new(name: &str) -> RSAKeyPair {
        RSAKeyPair {
            name: String::from(name),
            private_key: vec![],
            public_key: vec![],
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RSAKeyPairs {
    pub id: u16,
    pub rsa_4096_key_pair: RSAKeyPair,
    pub rsa_2048_key_pair: RSAKeyPair,
}
impl RSAKeyPairs {
    /// Generate new key pairs
    pub fn new(generate_keys: bool, id: &u16) -> RSAKeyPairs {
        match generate_keys {
            true => RSAKeyPairs {
                id: *id,
                rsa_4096_key_pair: encryption::rsa_generate_key_pair("RSA4096", super::enums::RsaKeyLength::RSA4096 as u16), // ! Change "RSA4096" to get string from formatting the enum name
                rsa_2048_key_pair: encryption::rsa_generate_key_pair("RSA2048", super::enums::RsaKeyLength::RSA2048 as u16),
            },
            false => RSAKeyPairs {
                id: *id,
                rsa_4096_key_pair: RSAKeyPair::new("RSA4096"),
                rsa_2048_key_pair: RSAKeyPair::new("RSA2048"),
            },
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PublicRSAKey {
    pub name: String,
    pub public_key: Vec<u8>
}
impl PublicRSAKey {
    pub fn new(name: &str) -> Result<PublicRSAKey, String> {
        if name.len()<1 || name.len()>40 {
            Err("Name must be 1 to 40 ascii characters".into())
        } else {
            Ok(PublicRSAKey {
                name: String::from(name),
                public_key: vec![]
            })
        }
    }
}

/// A clients own RSA Key pair and also a collection of public RSA keys so as to communicate securely with other specific clients
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PrivateCommunicationRSAKeyPairs {
    pub client_rsa_4096_key_pair: RSAKeyPair,
    pub public_rsa_4096_keys: Vec<PublicRSAKey>
}
impl PrivateCommunicationRSAKeyPairs {
    /// Generate new key pair
    pub fn new() -> PrivateCommunicationRSAKeyPairs {
        PrivateCommunicationRSAKeyPairs {
            client_rsa_4096_key_pair: encryption::rsa_generate_key_pair("RSA4096", super::enums::RsaKeyLength::RSA4096 as u16),
            public_rsa_4096_keys: vec![]
        }
    }

    /// Write self to file
    pub fn persist(&self, local_file_name: &str) -> Result<(), String> {
        let path_to_file_buff = crate::files::get_path_to_local_file(local_file_name);
        let path_to_file = path_to_file_buff.to_str().unwrap();
        let serialised_private_rsa_key_pairs = serde_json::to_string_pretty(self).unwrap().into_bytes().to_vec();
        crate::files::write_to_file(path_to_file, &serialised_private_rsa_key_pairs, false);

        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct AllRSAKeyPairs {
    pub rsa_key_pairs: Vec<RSAKeyPairs>
}
impl AllRSAKeyPairs {
    /// Create a new empty object
    pub fn new() -> AllRSAKeyPairs {

        AllRSAKeyPairs {
            rsa_key_pairs: vec![]
        }
    }

    /// Add a new RSAKeyPairs to the list
    pub fn add_key_pair(&mut self, new_rsa_key_pair: &RSAKeyPairs) {
        self.delete_key_pair(&new_rsa_key_pair.id); // Delete it in case it already exists

        self.rsa_key_pairs.push(new_rsa_key_pair.to_owned());
    }
    /// Delete a RSA key pair from the list of all key pairs
    pub fn delete_key_pair(&mut self, id: &u16) {
        self.rsa_key_pairs.retain(|k|k.id != *id);
    }

    /// Find the KeyPairs with the given Id
    pub fn get_key_pair(&self, id: &u16) -> Result<&RSAKeyPairs, String> {
        let option_found = self.rsa_key_pairs.iter().find(|k|k.id==*id);
        if option_found.is_some() {
            Ok(option_found.unwrap())
        } else {
            Err(format!("RSAKeyPair for id={} not found", id))
        }
    }
}
