/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! All structs of the system which are not specific to the client or server applications reside here

pub mod encryption;
pub mod packet_data_types;
pub mod configuration;
pub mod progress_bar;
pub mod banning;
pub mod callbacks;
pub mod serialisation;
pub mod bytes_processing;
pub mod byte_serialiser;

use tokio::net::TcpStream;
use std::{sync::mpsc::channel};
use serde::{Deserialize, Serialize};
use serialisation::{Deserialise, Serialise};

use super::enums;
use super::general;
use super::traits::{ProgressMonitorReceiver, SimpleProgressMonitor};
use super::log;
use super::traits;
use super::structs;

/// A packet of data that when selialised is what actually will be sent to the stream.
///
/// A DataPacket is the most ancestral object that gets sent between a client and the server (other than the
/// (InitialHandshake)[InitialHandshake]) and the first few bytes are always scrambled - (scrambler)[crate::encryption::scrambler] 
/// with the encryption_key. Any additional encryption needed must be done by the (packet)[packet_data_types] that this 
/// master packet will envelope and send.
///
/// So a File packet must encrypt its own contents and not rely on this DataPaket to do it
#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct DataPacket {
    pub length: u64,
    pub finalised: bool,
    pub packet_type: enums::PacketType,
    pub data: Vec<u8>,
}
impl DataPacket {
    /// Serialise into bytes
    pub fn into_bytes(&mut self, _packet_type: &enums::PacketType, packet_symmetric_encryption_key: Vec<u8>) -> Result<Vec<u8>, String> {
        let mut processed_bytes: Vec<u8> = super::encryption::scrambler(&true, self.data.to_owned(), packet_symmetric_encryption_key.clone(), self.data.len());

        let mut serialiser = Serialise::new("DataPacket");
        let mut finalised = super::encryption::scrambler(&true, vec![self.finalised as u8], packet_symmetric_encryption_key.clone(), 1);
        let mut packet_type = super::encryption::scrambler(&true, vec![self.packet_type as u8], packet_symmetric_encryption_key.clone(), 1);
        serialiser.add_field(&mut finalised, 1)?;
        serialiser.add_field(&mut packet_type, 1)?;
        serialiser.add_field(&mut processed_bytes, 0)?;

        return Ok(serialiser.into_bytes());
    }

    /// Serialise from bytes
    pub fn from_bytes(bytes: &Vec<u8>, packet_symmetric_encryption_key: Vec<u8>) -> Result<DataPacket, String> {
        let mut deserialiser = Deserialise::new(bytes, "DataPacket");
        let length = general::get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?);
        let finalised = super::encryption::scrambler(&false, deserialiser.extract_field(1)?, packet_symmetric_encryption_key.clone(), 1) == vec![1];
        let packet_type = super::enums::PacketType::from_byte(&(super::encryption::scrambler(&false, deserialiser.extract_field(1)?, packet_symmetric_encryption_key.clone(), 1)[0] as u8));

        let bytes_to_process = deserialiser.extract_field_to_end()?;
        let processed_bytes: Vec<u8> = super::encryption::scrambler(&false, bytes_to_process.to_vec(), packet_symmetric_encryption_key.clone(), bytes_to_process.len());

        let data_packet = DataPacket {
            length: length as u64,
            finalised: finalised,
            packet_type: packet_type,
            data: processed_bytes
        };

        return Ok(data_packet);
    }

    /// Create a data packet as bytes to send
    pub fn new(packet_type: &enums::PacketType, bytes_to_send: &Vec<u8>, packet_symmetric_encryption_key: Vec<u8>) -> Result<Vec<u8>, String> {
        let length = bytes_to_send.len() as u64 + crate::LENGTH_OF_DATA_PACKET_HEADER as u64 + 1;
        let mut new_packet = DataPacket {
            length: length,
            finalised: (length <= crate::MAXIMUM_LENGTH_DATA_PACKET as u64),
            packet_type: *packet_type,
            data: bytes_to_send.to_owned(),
        };

        new_packet.into_bytes(&packet_type, packet_symmetric_encryption_key)
    }

    /// Create a new status message data packet
    pub fn new_status_message(message: &str, packet_symmetric_encryption_key: Vec<u8>) -> Result<Vec<u8>, String> {
        DataPacket::new(&enums::PacketType::Status, &message.as_bytes().to_vec(), packet_symmetric_encryption_key)
    }
}

/// A struct to send when doing an initial handshake send to the server. A DataPacket is the master struct useed to send data between a clinet and server, but this is the only exception.
#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct InitialHandshake {
    pub length: u64,
    pub status: super::enums::Status,
    pub session_id: Vec<u8>,
    pub access_key: Vec<u8>,
    pub registration_access_key: Vec<u8>,
    pub encryption_scheme: super::enums::EncryptionScheme,
    pub enable_compression: super::enums::EnableCompression,
    pub packet_symmetric_encryption_key: Vec<u8>,
}
impl InitialHandshake {
    /// Serialise into bytes
    pub fn into_bytes(&self, rsa_key_pair_id: &u16) -> Result<Vec<u8>, String> {
        let mut access_key_to_use = self.access_key.to_owned();
        if self.access_key.len() > crate::MAXIMUM_LENGTH_ACCESS_KEY {
            let error_message = format!("Length of access key bytes exceeds maximum of {}", crate::MAXIMUM_LENGTH_ACCESS_KEY);
            log!(&error_message);
            return Err(error_message);
        } else {
            // Append 0's to end of file name so that it fills upto MAXIMUM_LENGTH_FILE_NAME
            access_key_to_use = general::append_vector_to_length(access_key_to_use, crate::MAXIMUM_LENGTH_ACCESS_KEY);
        }

        let mut registration_access_key_to_use = self.registration_access_key.to_owned();
        if self.registration_access_key.len() > crate::MAXIMUM_LENGTH_REGISTRATION_KEY {
            let error_message = format!("Length of registration_access_key bytes exceeds maximum of {}", crate::MAXIMUM_LENGTH_REGISTRATION_KEY);
            log!(&error_message);
            return Err(error_message);
        } else {
            registration_access_key_to_use = general::append_vector_to_length(registration_access_key_to_use, crate::MAXIMUM_LENGTH_REGISTRATION_KEY);
        }

        let mut packet_symmetric_encryption_key_to_use: Vec<u8>;
        if self.packet_symmetric_encryption_key.len() > crate::MAXIMUM_LENGTH_RSA_4096_PUBLIC_KEY {
            let error_message = format!("Length of symmetric aes encryption key with length {} exceeds maximum of {}", self.packet_symmetric_encryption_key.len(), crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
            log!(&error_message);
            return Err(error_message);
        } else {
            packet_symmetric_encryption_key_to_use = self.packet_symmetric_encryption_key.to_owned();
        }
        
        let unencrypted_session_id = general::get_random_alphanumeric_characters(crate::MAXIMUM_LENGTH_SESSION_KEY as u16);
        let mut new_vector: Vec<u8> = vec![];

        let mut scrambled_access_key: Vec<u8> = access_key_to_use;
        let mut scrambled_registration_access_key: Vec<u8> = registration_access_key_to_use;
        let mut scrambled_error_type: Vec<u8> = vec![self.status as u8];
        let mut scrambled_enable_compression: Vec<u8> = vec![self.enable_compression as u8];

        // Scramble needed fields
        scrambled_access_key = super::encryption::scrambler(&true, scrambled_access_key, packet_symmetric_encryption_key_to_use.clone(), crate::MAXIMUM_LENGTH_ACCESS_KEY);
        scrambled_registration_access_key = super::encryption::scrambler(&true, scrambled_registration_access_key, packet_symmetric_encryption_key_to_use.clone(), crate::MAXIMUM_LENGTH_REGISTRATION_KEY);
        scrambled_error_type = super::encryption::scrambler(&true, scrambled_error_type, packet_symmetric_encryption_key_to_use.clone(), 1);
        scrambled_enable_compression = super::encryption::scrambler(&true, scrambled_enable_compression, packet_symmetric_encryption_key_to_use.clone(), 1);

        if self.encryption_scheme!=enums::EncryptionScheme::None && self.encryption_scheme!=enums::EncryptionScheme::Unknown {
            let use_public_key: bool = self.encryption_scheme==crate::enums::EncryptionScheme::Rsa2048Aes128
            || self.encryption_scheme==crate::enums::EncryptionScheme::Rsa2048Aes256
            || self.encryption_scheme==crate::enums::EncryptionScheme::Rsa4096Aes128
            || self.encryption_scheme==crate::enums::EncryptionScheme::Rsa4096Aes256;

            packet_symmetric_encryption_key_to_use = match use_public_key {
                true => crate::enums::EncryptionScheme::encrypt_with_rsa_if_needed(&packet_symmetric_encryption_key_to_use.to_owned(), &self.encryption_scheme, use_public_key, rsa_key_pair_id).unwrap(),
                _ => general::append_vector_to_length(packet_symmetric_encryption_key_to_use.to_owned(), crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY)
            };
        } else {
            packet_symmetric_encryption_key_to_use = general::append_vector_to_length(packet_symmetric_encryption_key_to_use, crate::MAXIMUM_LENGTH_RSA_4096_PUBLIC_KEY);
        }


        let mut length: Vec<u8> = (crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64 + crate::MAXIMUM_LENGTH_ACCESS_KEY as u64 + crate::MAXIMUM_LENGTH_REGISTRATION_KEY as u64 + crate::MAXIMUM_LENGTH_SESSION_KEY as u64  + 3 + crate::MAXIMUM_LENGTH_RSA_4096_PUBLIC_KEY as u64).to_be_bytes().to_vec();
        new_vector.append(&mut length);
        new_vector.append(&mut scrambled_access_key);
        new_vector.append(&mut scrambled_registration_access_key);
        new_vector.append(&mut general::append_vector_to_length(unencrypted_session_id.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_SESSION_KEY as usize));
        new_vector.push(scrambled_error_type[0]);
        new_vector.push(self.encryption_scheme as u8);
        new_vector.push(scrambled_enable_compression[0]);
        new_vector.append(&mut packet_symmetric_encryption_key_to_use.clone());

        Ok(new_vector)
    }


    /// Serialise from bytes
    pub fn from_bytes(bytes: &Vec<u8>) -> Result<InitialHandshake, String> {
        let mut deserialiser  = Deserialise::new(bytes, "InitialHandshake");
        let _length = deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize)?;
        let mut access_key = deserialiser.extract_field(crate::MAXIMUM_LENGTH_ACCESS_KEY)?;
        let mut registration_access_key = deserialiser.extract_field(crate::MAXIMUM_LENGTH_REGISTRATION_KEY)?;
        let unencrypted_session_id = deserialiser.extract_field(crate::MAXIMUM_LENGTH_SESSION_KEY as usize)?;
        let mut status = deserialiser.extract_field(1)?;
        let encryption_scheme = enums::EncryptionScheme::from_byte(&deserialiser.extract_field(1)?[0]);
        let enable_compression_byte: u8 = deserialiser.extract_field(1)?[0] as u8;
        let mut packet_symmetric_encryption_key = deserialiser.extract_field_to_end()?;
        
        let use_private_key= enums::EncryptionScheme::use_public_or_private_key(&encryption_scheme);
             
        if encryption_scheme!=enums::EncryptionScheme::None && encryption_scheme!=enums::EncryptionScheme::Unknown {
            let option_deprocessed_packet_symmetric_encryption_key = enums::EncryptionScheme::decrypt_with_rsa_if_needed(&packet_symmetric_encryption_key, &encryption_scheme, use_private_key);
            if option_deprocessed_packet_symmetric_encryption_key.is_none() {
                return Err("Decryption error".into());
            }
            
            packet_symmetric_encryption_key = option_deprocessed_packet_symmetric_encryption_key.unwrap();            
            
        }

        // Descramble needed fields
        access_key = super::encryption::scrambler(&false, access_key, packet_symmetric_encryption_key.clone(), crate::MAXIMUM_LENGTH_ACCESS_KEY);
        registration_access_key = super::encryption::scrambler(&false, registration_access_key, packet_symmetric_encryption_key.clone(), crate::MAXIMUM_LENGTH_REGISTRATION_KEY);
        status = super::encryption::scrambler(&false, status, packet_symmetric_encryption_key.clone(), 1);
        let enable_compression = enums::EnableCompression::from_byte(&super::encryption::scrambler(&false, vec![enable_compression_byte], packet_symmetric_encryption_key.clone(), 1)[0]);

        let initial_handshake = InitialHandshake {
            length: bytes.len() as u64,
            access_key: access_key,
            registration_access_key: registration_access_key,
            session_id: unencrypted_session_id,
            status: super::enums::Status::from_byte(status[0] as u8),
            encryption_scheme: encryption_scheme,
            enable_compression: enable_compression,
            packet_symmetric_encryption_key: packet_symmetric_encryption_key.to_owned()
        };
        
        Ok(initial_handshake)
    }

    /// Create a Header for a new session handshake
    pub fn new_session(access_key: &str, registration_access_key: &str, session_id: &str, 
        encryption_scheme: super::enums::EncryptionScheme, 
        enable_commpression: super::enums::EnableCompression, 
        packet_symmetric_encryption_key: Vec<u8>) 
        -> InitialHandshake {        
            
        let padded_access_key = general::append_vector_to_length(access_key.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_ACCESS_KEY);
        let padded_registration_access_key= general::append_vector_to_length(registration_access_key.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_REGISTRATION_KEY);
        
        InitialHandshake {
            access_key: padded_access_key,
            registration_access_key: padded_registration_access_key,
            session_id: general::append_vector_to_length(session_id.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_SESSION_KEY as usize),
            status: super::enums::Status::Ok,
            encryption_scheme: encryption_scheme.to_owned(),
            enable_compression: enable_commpression.to_owned(),
            length: 0,
            packet_symmetric_encryption_key: packet_symmetric_encryption_key,
        }
    }

    /// A new session handler for a first time receiving of a new session request on the server (ONLY on the server)
    pub fn new_null_session() -> InitialHandshake {
        InitialHandshake {
            access_key: vec![],
            registration_access_key: vec![],
            session_id: vec![],
            status: super::enums::Status::Ok,
            encryption_scheme: super::enums::EncryptionScheme::None,
            enable_compression: super::enums::EnableCompression::No,
            length: 0,
            packet_symmetric_encryption_key: vec![],
        }
    }
}

/// An object which allows for sending data in a connected session to a server.
///
/// Without a session being established successfully client and server commincations will be blocked.
#[derive(Debug)]
pub struct Session{
    pub key: String,
    pub user_password: Vec<u8>,
    pub initial_handshake: InitialHandshake,
    pub connection_params: ConnectionParams
} 

/// One place to store information required to use newtork stream services
#[derive(Debug)]
pub struct ConnectionParams {
    pub delay_by: Option<std::time::Duration>,
    pub tcp_stream: TcpStream,
}
impl ConnectionParams {
    pub fn new(delay_by: Option<std::time::Duration>, tcp_stream: TcpStream) -> ConnectionParams {
        ConnectionParams {
            delay_by: delay_by,
            tcp_stream: tcp_stream
        }
    }
}

/// A struct for receiving bytes into memory
pub struct BytesReceiving<'a> {
    pub buffer_with_received_data: Vec<u8>,
    expected_length: usize,
    progress_bar: &'a mut ProgressBar,
    display_progress: bool,
}
impl <'a>traits::BytesReceiveHandler for BytesReceiving<'a> {
    /// Receive bytes
    fn receive_bytes(&mut self, bytes: &mut Vec<u8>) {
        self.buffer_with_received_data.append(bytes);
        if self.expected_length == 0 {
            self.expected_length = general::get_64bit_number_from(&self.buffer_with_received_data) as usize;
            self.progress_bar.set_maximum_value(self.expected_length as u64)
        }
        if self.display_progress || self.expected_length >= crate::PROGRESSBAR_DISPLAY_THRESHOLD_BYTES_COUNT {
            self.display_progress = true;
            if self.expected_length == 0 {
                
            }
            self.progress_bar.set_current_value(self.buffer_with_received_data.len() as u64);
        }
    }

    // Return the buffer with all the currently received byte data
    fn get_buffer_with_received_data(&self) -> &Vec<u8> {
        &self.buffer_with_received_data
    }

    /// Read the first few bytes of the already received data and extract the u64 packet length from it
    fn get_expected_length(&self) -> usize {
        self.expected_length
    }

    /// Get the current number of bytes read
    fn get_current_length(&self) -> usize {
        self.buffer_with_received_data.len() as usize
    }

    /// Inicate that data transfer is complete
    fn receive_complete(&mut self) {
        if self.display_progress {
            self.progress_bar.completed();
        }
    }
}
impl <'a>BytesReceiving<'a> {
    /// Create and return a new byte receiver
    pub fn new(progress_bar: &mut ProgressBar, display_progress: bool) -> BytesReceiving{
        BytesReceiving {
            buffer_with_received_data: vec![],
            expected_length: 0,
            progress_bar: progress_bar,
            display_progress: display_progress
        }
    }
}

/// A struct that can be used purely to indicate the progress of a data transfer
pub struct ProgressBar {
    maximum_value: u64,
    current_value: u64,
    progress_monitor_indicator: IndicateTransferProgress,
}
impl ProgressBar {
    /// Create and return a new ProgressBar
    pub fn new(label: &str, maximum_value: u64) -> ProgressBar {
        ProgressBar {
            maximum_value: maximum_value,
            current_value: 0,
            progress_monitor_indicator: IndicateTransferProgress::new(label, maximum_value),
        }
    }
}
impl traits::SimpleProgressMonitor for ProgressBar {
    /// Set the maximum value or 'cap' of the progress monitor
    fn set_maximum_value(&mut self, value: u64) {
        self.maximum_value = value;
    }

    /// Set the current value of the progress monitor
    fn set_current_value(&mut self, value: u64) {
        self.current_value = value;
        let current_percentage = self.get_current_progress_percentage_float();
        self.progress_monitor_indicator.set_current_percentage(current_percentage as f32);
    }

    /// Get the current percentage completed as a float
    fn get_current_progress_percentage_float(&self) -> f32 {
        (self.current_value as f32 / self.maximum_value as f32) * 100_f32
    }

    /// Get the current percentage completed as an integer
    fn get_current_progress_percentage_integer(&self) -> u8 {
        self.get_current_progress_percentage_float().round() as u8
    }

    fn completed(&mut self) {
        self.progress_monitor_indicator.completed();
    }
}

/// An object that displays the progress of a transfer a visual progress bar
pub struct IndicateTransferProgress {
    progress_bar: progress_bar::ProgressBar,
}
impl traits::ProgressMonitorReceiver for IndicateTransferProgress {
    fn set_current_percentage(&mut self, value: f32) {
        self.progress_bar.set_percent(value);
    }

    fn completed(&mut self) {
        self.progress_bar.completed();
    }
}
impl IndicateTransferProgress {
    pub fn new(label: &str, maximum_value: u64) -> IndicateTransferProgress {
        IndicateTransferProgress {
            progress_bar: progress_bar::ProgressBar::new(label, maximum_value as f32)
        }
    }
}

/// A struct for receiving bytes to file
pub struct BytesToFile {
    pub file_name: String,
    pub buffer_with_received_data: Vec<u8>,
    pub expected_length: usize
}
impl traits::BytesReceiveHandler for BytesToFile {
    /// Receive bytes
    fn receive_bytes(&mut self, bytes: &mut Vec<u8>) {
        self.buffer_with_received_data.append(bytes);
        if self.expected_length == 0 {
            self.expected_length =
                general::get_64bit_number_from(&self.buffer_with_received_data) as usize;
        }
    }

    // Return the buffer with all the currently received byte data
    fn get_buffer_with_received_data(&self) -> &Vec<u8> {
        &self.buffer_with_received_data
    }

    /// Read the first few bytes of the already received data and extract the u64 packet length from it
    fn get_expected_length(&self) -> usize {
        self.expected_length
    }

    fn get_current_length(&self) -> usize {
        self.buffer_with_received_data.len() as usize
    }

    fn receive_complete(&mut self) {
        self.file_name = self.get_file_name();
    }
}
impl BytesToFile {
    /// Create and return a new byte receiver
    pub fn new() -> BytesToFile {
        BytesToFile {
            file_name: String::new(),
            buffer_with_received_data: vec![],
            expected_length: 0
        }
    }

    // ! Duplicate code here?
    /// Extract the original file name out of the bytes
    fn get_file_name(&self) -> String {
        let mut file_name: Vec<u8> = self.buffer_with_received_data
            [(crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize)..self.buffer_with_received_data.len()]
            .to_vec();
        file_name.retain(|&b| b != 0);

        String::from_utf8_lossy(&file_name).to_string()
    }
}

/// Byte counts to limit bytes received from a byte stream
pub struct BytesControl {
    /// Total count of bytes already uploaded to server
    pub already_uploaded: usize,
    /// Maximum bytes count allowed to be uploaded: typically per access key
    pub total_allowed_to_upload: usize
}
impl BytesControl {
    pub fn new(already_uploaded: usize, total_allowed_to_upload: usize) -> BytesControl {
        BytesControl {
            already_uploaded: already_uploaded,
            total_allowed_to_upload: total_allowed_to_upload
        }
    }
}


/// Holds estimated values for encryption and compression speeds.
//
/// This is used to predict (an average over time this program was ever used) to the user the time it might take to compress\decompress or encrypt\decrypt data.
/// Trying to teach the system on already compressed data and then deciding to transfer a text file will obviously not be very accurate, but over time it may average out
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Benchmarks {
    pub aes_128_bytes_per_second: f32,
    pub aes_128_counter: u32,
    pub aes_256_bytes_per_second: f32,
    pub aes_256_counter: u32,
    pub compression_bytes_per_second: f32,
    pub compression_count: u32,
    pub decompression_bytes_per_second: f32,
    pub decompression_count: u32,
}
impl Benchmarks {
    /// Generate new benchmarks with some default starting values
    pub fn new() -> Benchmarks {
        Benchmarks {
            aes_128_bytes_per_second: 4_000_000_f32,
            aes_128_counter: 1,
            aes_256_bytes_per_second: 3_500_000_f32,
            aes_256_counter: 1,
            compression_bytes_per_second: 3_000_000_f32,
            compression_count: 1,
            decompression_bytes_per_second: 40_000_000_f32,
            decompression_count: 1
        }
    }
    
    /// Show a progress bar to the user. Currently disabled on Windows.
    pub fn show_predicted_progress(label: &str, maximum_value: f32, bytes_per_second: f32) -> Option<std::sync::mpsc::Sender<bool>> {
        if cfg!(windows) { 

            None
        } else {
            let mut progress_bar = structs::progress_bar::ProgressBar::new(label, maximum_value);
            let (sender, receiver) = channel();
            let _ = std::thread::spawn(move || {
                    loop {
                        std::thread::sleep(std::time::Duration::from_secs(1));
                        progress_bar.update_progress(bytes_per_second);
                        let kill = receiver.try_recv();
                        if kill.is_ok() && kill.unwrap() {
                            progress_bar.completed();
                            break;
                        }
                    }
                }
            );
            
            Some(sender)
        }
    }
}

/// Holds the resultant data from an encryption or compression process.
///
/// his data will be persisted to file on the client using the ```on_benchmarks_handler``` callback function.
pub struct BenchmarkResult{
    pub result_type: enums::BenchmarkType,
    pub scheme: u8,
    pub bytes_per_second: f32
}
impl BenchmarkResult {
    pub fn new(result_type: enums::BenchmarkType, scheme: u8, bytes_count: usize, duration: std::time::Duration) -> BenchmarkResult {
        let bytes_per_second: f32 = bytes_count as f32 / duration.as_secs_f32();
        BenchmarkResult {
            result_type: result_type,
            scheme: scheme,
            bytes_per_second: bytes_per_second
        }
    }
}

/// A list of subscribers to be notified of an event
pub struct Notifier<E> {
    subscribers: Vec<Box<dyn Fn(&E)>>,
}
impl<E> Notifier<E> {
    pub fn new() -> Notifier<E> {
        Notifier {
            subscribers: Vec::new(),
        }
    }

    /// Register a new subscriber
    pub fn register<F>(&mut self, callback: F)
    where F: 'static + Fn(&E),
    {
        self.subscribers.push(Box::new(callback));
    }

    /// Notify all subscibers of an event
    pub fn notify(&self, event: E) {
        for callback in &self.subscribers {
            callback(&event);
        }
    }
}