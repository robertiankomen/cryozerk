/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Functionality dealing with multi-threaded encryption and comression of File data

use std::thread;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use crate::enums;

use super::serialisation::{*};
use super::general::get_64bit_number_from;

/// A block of data Processed typically from the original bytes due to encryption or compression
#[derive(Clone, Debug)]
pub struct ProcessedBytesBlock {
    pub length: u64,
    pub bytes: Vec<u8>
}
impl ProcessedBytesBlock {
    pub fn new(bytes: &Vec<u8>) -> ProcessedBytesBlock {
        ProcessedBytesBlock {
            length: crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64 + bytes.len() as u64,
            bytes: bytes.clone()
        }
    }

    /// Serialise into bytes
    pub fn into_bytes(&mut self) -> Result<Vec<u8>, String> {
        let mut serialise = Serialise::new("ProcessedBytesBlock");
        serialise.add_field(&mut self.bytes, 0)?;

        Ok(serialise.into_bytes())
    }

    /// Serialise from bytes
    pub fn from_bytes(bytes: &Vec<u8>) -> Result<ProcessedBytesBlock, String> {
        let mut deserialiser = Deserialise::new(&bytes, "ProcessedBytesBlock");

        let new_block = ProcessedBytesBlock {
            length: bytes.len() as u64,
            bytes: deserialiser.extract_field_to_end()?
        };

        Ok(new_block)
    }
}

/// A collection of one or more ProcessedBytesBlock items
#[derive(Clone, Debug)]
pub struct ProcessedBlocks {
    /// The length of the actual number of bytes contained in the blocks
    pub length: u64,
    /// The actual bytes extracted into blocks to process
    blocks: Vec<ProcessedBytesBlock>,
    //The salt used to encrypt/decrypt with
    pub salt: Vec<u8>
}
impl ProcessedBlocks {
    /// Never instantiate a struct directly, always use this method to instantiate
    pub fn new() -> ProcessedBlocks {
        ProcessedBlocks {
            length: crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64,
            blocks: vec![],
            salt: crate::encryption::generate_random_vector(crate::LENGTH_OF_ENCRYPTION_SALT as u32)
        }
    }

    /// Add a new Process block to the collection
    pub fn add(&mut self, processed_bytes_block: ProcessedBytesBlock) {
        self.length+=processed_bytes_block.length + crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64;
        self.blocks.push(processed_bytes_block);
    }

    /// Serialise into bytes
    pub fn into_bytes(&mut self) -> Result<Vec<u8>, String> {
        let mut serialise = Serialise::new("ProcessedBlocks");
        // serialise.add_field(&mut self.salt, 0)?;
        for block in self.blocks.iter_mut() {
            serialise.add_field(&mut block.into_bytes()?, 0)?;
        }

        Ok(serialise.into_bytes())
    }

    /// Serialise from bytes. 
    ///
    /// Note: the original bytes are altered after processing and can no longer be used
    pub fn from_bytes(bytes: &mut Vec<u8>) -> Result<ProcessedBlocks, String> {
        let mut deserialiser = Deserialise::new(&bytes, "ProcessedBlocks");

        let salt = deserialiser.extract_field(crate::LENGTH_OF_ENCRYPTION_SALT)?;
        let length = get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?);
        
        let mut extracted_blocks: Vec<ProcessedBytesBlock> = vec![];
        let mut counter: u64 = 0;
        if length > crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64 {
            while (counter + crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64) < length {
                let block_length = get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?);
                let bytes_to_convert = deserialiser.extract_field(block_length as usize - crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize)?;
                extracted_blocks.push(ProcessedBytesBlock::new(&bytes_to_convert));
                counter+=block_length as u64;
            } 
        }

        let processed_blocks = ProcessedBlocks {
            length: counter + crate::LENGTH_OF_U64_NUMBER_IN_BYTES as u64,
            blocks: extracted_blocks,
            salt: salt
        };

        if processed_blocks.blocks.len() > ProcessedBlocks::get_concurrency() {
            let error_message = format!("Error: A request of {} blocks was received instead of the maximum of {}", processed_blocks.blocks.len(), ProcessedBlocks::get_concurrency());
            crate::log!(&error_message);
            return Err(error_message);
        };

        Ok(processed_blocks)
    }
    
    /// We can be pretty sure that there will be a difference in the number of cpu's different clients, so we hard code a value here.
    ///
    /// I specifically decided to not make this a config setting
    pub fn get_concurrency() -> usize {
        crate::CONCURRENT_PROCESSES_COUNT
    }

    /// Given a block of bytes... extract smaller work blocks out of it, perform threaded encryption on each and create a collection of ProcessedBytesBlock's. 
    ///
    /// Ownership is taken because the original input bytes are cleared in the conversion process to conserve RAM usage
    pub fn encrypt(encryption_scheme: enums::EncryptionScheme, bytes: &mut Vec<u8>, user_password: Vec<u8>, on_benchmarks_handler: fn(benchmarks: super::structs::BenchmarkResult))-> Result<ProcessedBlocks, String> {
        let bytes_count = bytes.len();
        let mut task_blocks: Vec<Vec<u8>> = ProcessedBlocks::extract_task_blocks(bytes, ProcessedBlocks::get_concurrency());
        let mut completed_blocks = ProcessedBlocks::new();
        completed_blocks.do_tasks_encrypt(encryption_scheme, bytes_count, &mut task_blocks, user_password, on_benchmarks_handler)?;
        
        Ok(completed_blocks)
    }

    /// Decrypt the data
    pub fn decrypt(&mut self, encryption_scheme: enums::EncryptionScheme, user_password: Vec<u8>, on_benchmarks_handler: fn(benchmarks: super::structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
        self.do_tasks_decrypt(encryption_scheme, user_password, on_benchmarks_handler)
    }

    /// Do the concurrent encryption tasks
    fn do_tasks_encrypt(&mut self, encryption_scheme: enums::EncryptionScheme, bytes_count: usize, task_blocks: &mut Vec<Vec<u8>>, user_password: Vec<u8>, on_benchmarks_handler: fn(benchmarks: super::structs::BenchmarkResult)) -> Result<(), String> {
        let mut handles: Vec<thread::JoinHandle<()>> = vec![];
        let mut results: Vec<(usize, Result<Vec<u8>, String>)> = vec![];

        let option_kill_sender = crate::structs::Benchmarks::show_predicted_progress(&crate::LABEL_ENCRYPTING, bytes_count as f32, crate::BENCHMARKS.lock().unwrap().aes_256_bytes_per_second);
        let start = std::time::Instant::now();
        let (sender, receiver): (Sender<(usize, Result<Vec<u8>, String>)>, Receiver<(usize, Result<Vec<u8>, String>)>) = mpsc::channel();
        for current_index in 0..task_blocks.len() {
            let thread_block = task_blocks[current_index].clone();
            task_blocks[current_index].clear(); //No longer needed so save RAM by clearing it
            let thread_sender = sender.clone();
            let thread_password = user_password.clone();
            let thread_salt = self.salt.clone();
            let handle = thread::spawn(move|| {
                let option_processed_bytes = match encryption_scheme {
                    enums::EncryptionScheme::Rsa4096Aes256 | enums::EncryptionScheme::Rsa2048Aes256 | enums::EncryptionScheme::Aes256 => crate::encryption::aes_256_encrypt(&thread_block, &thread_salt, &thread_password),
                    enums::EncryptionScheme::Rsa4096Aes128 | enums::EncryptionScheme::Rsa2048Aes128 | enums::EncryptionScheme::Aes128 => crate::encryption::aes_128_encrypt(&thread_block, &thread_salt, &thread_password),
                    _ => Some(thread_block)
                };
                if option_processed_bytes.is_some() {
                    let _= thread_sender.send((current_index, Ok(option_processed_bytes.unwrap())));
                } else {
                    let _ = thread_sender.send((current_index, Err(String::from("Error encrypting threaded AES data"))));
                }
            });
            
            handles.push(handle);
        }

        for handler in handles {
            let completed_bytes = receiver.recv().unwrap();
            results.push(completed_bytes);
            handler.join().unwrap();
        };

        if option_kill_sender.is_some() {
            let _ = option_kill_sender.unwrap().send(true);
        };
        let result = crate::structs::BenchmarkResult::new(enums::BenchmarkType::Encryption, enums::EncryptionScheme::Aes256 as u8, bytes_count, start.elapsed());
        on_benchmarks_handler(result);

        results.sort_by(|a, b| a.0.cmp(&b.0));
        for index in 0..results.len() {
            if results[index].1.is_ok() {
                let bytes = results[index].1.clone().unwrap();
                let result = ProcessedBytesBlock::from_bytes(&bytes);
                if result.is_ok() {
                    self.add(result.unwrap());
                } else {
                    return Err(result.unwrap_err())
                }
            } else {
                return Err(results[index].1.clone().unwrap_err());
            }
        };

        Ok(())
    }

    /// Do the concurrent decryption tasks
    fn do_tasks_decrypt(&mut self, encryption_scheme: enums::EncryptionScheme, user_password: Vec<u8>, on_benchmarks_handler: fn(benchmarks: super::structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
        let mut handles: Vec<thread::JoinHandle<()>> = vec![];
        let mut results: Vec<(usize, Result<Vec<u8>, String>)> = vec![];
        let mut to_return: Vec<u8> = vec![];

        let option_kill_sender = crate::structs::Benchmarks::show_predicted_progress(&crate::LABEL_DECRYPTING, self.length as f32, crate::BENCHMARKS.lock().unwrap().aes_256_bytes_per_second);
        let start = std::time::Instant::now();

        let (sender, receiver): (Sender<(usize, Result<Vec<u8>, String>)>, Receiver<(usize, Result<Vec<u8>, String>)>) = mpsc::channel();
        for current_index in 0..self.blocks.len() {
            let thread_block = self.blocks[current_index].bytes.clone();
            self.blocks[current_index].bytes = vec![]; //No longer needed so save RAM by clearing it
            let thread_sender = sender.clone();
            let thread_password = user_password.clone();
            let thread_salt = self.salt.clone();
            let handle = thread::spawn(move|| {
                let option_processed_bytes = match encryption_scheme {
                    enums::EncryptionScheme::Rsa4096Aes256 | enums::EncryptionScheme::Rsa2048Aes256 | enums::EncryptionScheme::Aes256 => crate::encryption::aes_256_decrypt(&thread_block, &thread_salt, &thread_password),
                    enums::EncryptionScheme::Rsa4096Aes128 | enums::EncryptionScheme::Rsa2048Aes128 | enums::EncryptionScheme::Aes128 => crate::encryption::aes_128_decrypt(&thread_block, &thread_salt, &thread_password),
                    _ => Some(thread_block)
                };
                if option_processed_bytes.is_some() {
                    let _=  thread_sender.send((current_index, Ok(option_processed_bytes.unwrap())));
                } else {
                    let _ = thread_sender.send((current_index, Err(String::from("Error decrypting threaded AES data"))));
                }                
            });
            
            handles.push(handle);
        }

        for handler in handles {
            let completed_bytes = receiver.recv().unwrap();
            results.push(completed_bytes);
            handler.join().unwrap();
        }

        if option_kill_sender.is_some() {
            let _ = option_kill_sender.unwrap().send(true);
        };
        let result = crate::structs::BenchmarkResult::new(enums::BenchmarkType::Encryption, enums::EncryptionScheme::Aes256 as u8, self.length as usize, start.elapsed());
        on_benchmarks_handler(result);
        

        results.sort_by(|a, b| a.0.cmp(&b.0));
        for index in 0..results.len() {
            if results[index].1.is_ok() {
                let mut bytes = results[index].1.clone().unwrap();
                to_return.append(&mut bytes);
            } else {
                return Err(results[index].1.clone().unwrap_err());
            }
        }

        Ok(to_return)
    }

    /// Takes a vector and extracts smaller blocks out of it.
    ///
    /// Note that the original bytes are tuncated and can no longer be used.
    pub fn extract_task_blocks(bytes: &mut Vec<u8>, task_blocks_count: usize) -> Vec<Vec<u8>> {
        let task_block_size = match task_blocks_count>1 {
            true => bytes.len() / (task_blocks_count-1),
            false => 1
        };
        let last_task_block_size = bytes.len() - ((task_blocks_count-1)*task_block_size);
        let mut extracted_blocks: Vec<Vec<u8>> = vec![];
       
        bytes.reverse(); // This reverse idea... there must be a more efficiant way, take 5min to duckduckgo it.

        //let loop_counter = (original_bytes.len() as f32 / task_block_size as f32).ceil() as usize;
        for index in 0..task_blocks_count {
            let length_to_extract = if (bytes.len()/task_block_size)==0 {
                bytes.len()
            } else {
                if index != task_blocks_count-1 {
                    task_block_size
                } else {
                    if last_task_block_size != 0 {
                        last_task_block_size
                    } else {
                        break;
                    }
                }
            };

            let mut new_block: Vec<u8> = bytes[bytes.len()-length_to_extract..bytes.len()].to_vec();
            let new_block_length = new_block.len();
            new_block.reverse();
            extracted_blocks.push(new_block);
            bytes.truncate(bytes.len()-new_block_length); //Get rid of the now extracted bytes so as to save RAM
        }

        extracted_blocks
    }
}