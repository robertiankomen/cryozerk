/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! A visual progress bar to display to the user (disabled on Microsoft Windows)

pub struct ProgressBar {
    maximum_value: f32,
    current_value: f32,
    bar_display: pbr::ProgressBar<std::io::Stdout>,
}

//#[cfg(unix)]
impl ProgressBar {
    /// Create a new progress bar
    pub fn new(label: &str, maximum_value: f32) -> ProgressBar {
        let mut bar = ProgressBar {
            maximum_value: maximum_value,
            current_value: 0_f32,
            bar_display: pbr::ProgressBar::new(100)
        };
        if unsafe{crate::DARK_MODE==false} {
            bar.bar_display.message(&format!("{} ", label));
            //bar.bar_display.format("╢▌▌ ╟");
            bar.bar_display.format("[=> ]");
            bar.bar_display.show_percent = false;
        } else {
            bar.bar_display.show_bar=false;
        }
        bar
    }

    /// Update the progress bar
    pub fn update_progress(&mut self, increment_by: f32) {
        if unsafe{crate::DARK_MODE} { return; }
        self.current_value = self.current_value + increment_by;//((increment_by / self.maximum_value as f32) * 100.0);
        let percent = (self.current_value / self.maximum_value) * 100_f32;
        self.bar_display.set(percent as u64);
    }

    /// Update the progress bar
    pub fn set_percent(&mut self, percent: f32) {
        if unsafe{crate::DARK_MODE} { return; }
        self.bar_display.set(percent as u64);
    }

    /// Update the progress bar
    pub fn set_value(&mut self, value: f32) {
        if unsafe{crate::DARK_MODE} { return; }
        let percent = (value / self.maximum_value as f32) * 100.0;
        self.bar_display.set(percent as u64);
    }

    /// Set the progress bar to a completed state - ie. no longer in use
    pub fn completed(&mut self) {
        if unsafe{crate::DARK_MODE} { return; }
        self.bar_display.is_finish = true;
    }
}

// Progress bars disabled on Microsoft Windows
// #[cfg(windows)]
// impl ProgressBar {
//     pub fn new(label: &str, maximum_value: f32) -> ProgressBar {
//         let mut bar = ProgressBar {
//             maximum_value: maximum_value,
//             current_value: 0_f32,
//             bar_display: pg_bar::new(100)
//         };

//         if maximum_value == 100_f32 || maximum_value > 1024_f32*512_f32 { crate::display!(label,"..."); }

//         bar
//     }

//     pub fn update_progress(&mut self, increment_by: f32) { }
//     pub fn set_percent(&mut self, percent: f32) { }
//     pub fn completed(&mut self) { }
// }