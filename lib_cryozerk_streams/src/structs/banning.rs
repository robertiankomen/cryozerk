/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Structs to enable the banning of possible hack attempts etc.

use chrono::{DateTime, Local};
use std::net::IpAddr;
use serde::{Serialize, Deserialize};

/// A struct to hold client banning and unbanning information
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Ban {
    pub ip_address: IpAddr,
    pub access_key: String,
    pub times_banned: i32,
    pub counter: i32,
    pub unban_on: DateTime<Local>
}
impl Ban {
    /// Create a new banned Ip or access_key struct
    pub fn new(ip_address: IpAddr, access_key: Option<Vec<u8>>, unban_on: DateTime<Local>) -> Ban {
        Ban {
            ip_address: ip_address,
            access_key: match access_key.is_some() {
                true => {
                    let stripped_access_key = &super::general::strip_zeros(&access_key.unwrap());
                    std::str::from_utf8(&stripped_access_key).unwrap().into()
                }
                false => String::new()
            },
            times_banned: 0,
            counter: 1,
            unban_on: unban_on
        }
    }
}