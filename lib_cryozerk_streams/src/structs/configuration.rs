/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! SAtructs used to persist and retrieve config settings to and from the actual files on disk

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct IpAddress {
    pub ip_address: String
}

/// The configuration needed for a server instance
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ServerAccess {
    pub access_key: String,
    pub allow_extended_storage: bool,
    pub allow_custom_unique_codes: bool,
    pub max_transfer_size_bytes: u64,
    pub max_total_of_all_uploads_bytes: u64,
    pub automatic_deletion_after_seconds: u64,
    pub aes_password_with_no_rsa: String,
    pub first_ban_time_in_seconds: u32,
    pub second_ban_time_in_seconds: u32,
    pub ip_address_white_list: Vec<IpAddress>,
    pub allowed_registration_access_keys: Vec<RegistrationAccessKey>
}

/// Information about a file that a client may upgrade to if they have a lower version
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UpgradeClientFile {
    pub version_major: u8,
    pub version_minor: u8,
    pub version_revision: u8,
    pub file_name: String
}

/// Indicates a servers listening Ip address and port
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RegistrationAccessKey {
    pub name: String,
    pub id: String
}

/// Indicates a servers listening Ip address and port
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ServerTuple {
    pub ip_address: String,
    pub port: u16
}

/// CURRENTLY NOT USED - Proxy information for if the server must act as a forwarding proxy instead of handling client requests for data etc
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Proxy {
    pub act_as_proxy: bool,
    pub forward_to_options: Vec<ServerTuple>
}
impl Proxy {
    pub fn new_default() -> Proxy {
        let mut forward_to: Vec<ServerTuple> = vec![];
        forward_to.push(super::structs::configuration::ServerTuple { ip_address: "127.0.0.1".into(), port: 25453 });
        forward_to.push(super::structs::configuration::ServerTuple { ip_address: "168.0.1.100".into(), port: 25453 });

        Proxy {
            act_as_proxy: false,
            forward_to_options: forward_to
        }
    }
}

/// The configuration needed for a server instance
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Server {
    pub listening_ip: String,
    pub listening_port: u16,
    /// Is the server open to receiving new client registrations?
    pub allow_new_registrations: bool,
    /// The number of milliseconds to delay between sending or receiving network data packets
    ///
    /// Helpful with fast networks like when the server and client are running on the same machine
    pub delay_by_nano_seconds: u32,
    pub files_for_upgrade: Vec<UpgradeClientFile>,
    pub access_keys: Vec<ServerAccess>,
    pub persist_to_mongodb: bool,
    pub mongodb_connection_string: String,
    pub ban_after_handshake_fails_count_for_ip_address: i32,
    pub first_ban_time_in_seconds: u32,
    pub second_ban_time_in_seconds: u32,
    /// Writing to log files is disabled
    pub logging_enabled: bool,
    /// All writing to log files and also Ban related logging to database is dissabled. Effectively no logging of cient interactions
    /// with the server is done.
    /// Ban logging is still stored in RAM so the banning engine still works (all bans will be cleared if the cryozerk_server restarts though)
    pub dark_mode: bool
    //pub proxy: Proxy,
}
impl Server {
    pub fn new_empty() -> Server {

        Server {
            listening_ip: "".into(),
            listening_port: 0,
            allow_new_registrations: false,
            delay_by_nano_seconds: 0,
            files_for_upgrade: vec![],
            access_keys: vec![],
            ban_after_handshake_fails_count_for_ip_address: 0,
            dark_mode: false,
            first_ban_time_in_seconds: 0,
            logging_enabled: false,
            mongodb_connection_string: "".into(),
            persist_to_mongodb: false,
            second_ban_time_in_seconds: 0
        }
    }
}


/// A specific access_key available to use by a client
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ClientAccessKeys {
    pub server_ip: String,
    pub server_port: u16,
    /// The human readable name for the community this access key refers to
    pub access_key: String,
    /// The server may allow registration by only particular clients as denoted by a shared registration_access_key
    pub registration_access_key: String,
    pub local_community_id: u16,
    /// The number of milliseconds to delay between sending or receiving network data packets
    ///
    /// Helpful with fast networks like when the server and client are running on the same machine
    pub delay_by_nano_seconds: u32,
    pub aes_password_with_no_rsa: String
}
/// The configuration needed for a client instance
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Client {
    pub access_keys: Vec<ClientAccessKeys>,
    pub logging_enabled: bool
}