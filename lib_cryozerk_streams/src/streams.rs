/*
    cryozerk, cryozerk_server and libCryoZerkStreams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! General abstrated functions for network communications reside here.
//!
//! The actual network communications functions are in the sub module [stream](stream)

#[allow(unused_assignments)]

/// Abstracted funtions related to the sending and receiving of bytes over a stream
use super::structs;
use super::enums;
use super::traits;
use super::log;
use super::general;
use structs::DataPacket;
use tokio::net::TcpStream;

/// Create a new stream to a server
pub async fn create_stream(server_ip: &str, server_port: &u16) -> Result<TcpStream, String> {
    let tcp_stream = TcpStream::connect(&format!("{}:{}", server_ip, server_port)).await;
    if tcp_stream.is_ok() {
        return Ok(tcp_stream.unwrap());
    } else {
        log!("Error", tcp_stream.unwrap_err());
    }

    Err(String::from(&format!("Could not connect to: {}:{}", server_ip, server_port)))
}

/// Receive a data packet from the stream
pub async fn receive_data_packet(bytes_control: &structs::BytesControl, label: &str, application_instance: &enums::Application, packet_type: &enums::PacketType, session: &mut structs::Session) -> Result<structs::DataPacket, String> {
    let mut progress_bar = structs::ProgressBar::new(label, 0_64);
    let mut received_handler = structs::BytesReceiving::new(&mut progress_bar, bytes_control.total_allowed_to_upload>=crate::PROGRESSBAR_DISPLAY_THRESHOLD_BYTES_COUNT);
    receive_data_packet_from_stream(&bytes_control, &application_instance, &packet_type, &mut received_handler, &mut session.initial_handshake, &mut session.connection_params).await
}

/// Receive an expected last action status message
pub async fn receive_status_message(application_instance: &enums::Application, packet_type: &enums::PacketType, session: &mut structs::Session) -> Result<DataPacket, String> {
    let bytes_control = structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_HANDSHAKE_PACKET as usize);
    receive_data_packet(&bytes_control,&crate::LABEL_RECEIVING, application_instance, packet_type, session).await
}

/// A more detailed way to receive a data packet from the stream
pub async fn receive_data_packet_from_stream<T: traits::BytesReceiveHandler>(bytes_control: &structs::BytesControl, application_instance: &enums::Application, packet_type: &enums::PacketType, received_handler: &mut T, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) -> Result<structs::DataPacket, String> {
    let mut max_transfer_size_bytes: u64 = crate::MAXIMUM_LENGTH_HANDSHAKE_PACKET as u64;
    if *application_instance==enums::Application::Server {
        let server_access = crate::CONFIGURATION_SERVER.access_keys.iter().find
        (
            |ak| {
                general::append_vector_to_length(ak.access_key.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_ACCESS_KEY)
                ==initial_handshake.access_key
            }
        );

        if server_access.is_some() {
            max_transfer_size_bytes = server_access.unwrap().max_transfer_size_bytes;
        }
    }
    else {
        max_transfer_size_bytes = crate::MAXIMUM_LENGTH_DATA_PACKET;
    }

    let _ = stream::receive_bytes_from_stream(bytes_control, &packet_type, received_handler, max_transfer_size_bytes, connection_params).await?;
    if received_handler.get_buffer_with_received_data().len()==0 {
        return Err(String::from("No data was returned from server"));
    }

    let result_data_packet = structs::DataPacket::from_bytes( &received_handler.get_buffer_with_received_data(), initial_handshake.packet_symmetric_encryption_key.clone());

    if result_data_packet.is_ok() {
        let data_packet = result_data_packet.unwrap();
        return Ok(data_packet);
    } else {
        return Err(String::from("Error retreiving data packet from stream"));
    }
}

/// A more detailed way to receive a data packet from the stream
pub async fn receive_bytes_from_stream<T: traits::BytesReceiveHandler>(bytes_control: &structs::BytesControl, application_instance: &enums::Application, packet_type: &enums::PacketType, received_handler: &mut T, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) -> Result<Vec<u8>, String> {
    let mut max_transfer_size_bytes: u64 = crate::MAXIMUM_LENGTH_HANDSHAKE_PACKET as u64;
    if *application_instance==super::enums::Application::Server {
        let server_access = crate::CONFIGURATION_SERVER.access_keys.iter().find
        (
            |ak| {
                general::append_vector_to_length(ak.access_key.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_ACCESS_KEY)
                ==initial_handshake.access_key
            }
        );

        if server_access.is_some() {
            max_transfer_size_bytes = server_access.unwrap().max_transfer_size_bytes;
        }
    } else {
        max_transfer_size_bytes = crate::MAXIMUM_LENGTH_DATA_PACKET;
    }
    let _ = stream::receive_bytes_from_stream(&bytes_control, &packet_type, received_handler, max_transfer_size_bytes, connection_params).await?;

    Ok(received_handler.get_buffer_with_received_data().to_vec())
}

/// Send a request message to retreive an upgraded client file
pub async fn send_upgrade_message(session: &mut structs::Session) -> Result<(), String>  {
    let bytes_to_send_tuple = general::get_version_tuple();
    let bytes_to_send: Vec<u8> = vec![bytes_to_send_tuple.0, bytes_to_send_tuple.1, bytes_to_send_tuple.2];

    let message = structs::packet_data_types::Message::new(enums::PacketType::Upgrade, &bytes_to_send);
    let serialised_message_bytes = message.into_bytes()?;
    let result = send_data_packet(&enums::PacketType::Message, &serialised_message_bytes, session).await;

    result
}

/// Send a Message to the stream
pub async fn send_message(bytes_to_send: &Vec<u8>, session: &mut structs::Session) -> Result<(), String> {
    let message = structs::packet_data_types::Message::new(enums::PacketType::Message, &bytes_to_send);
    let serialised_message_bytes = message.into_bytes()?;
    let result = send_data_packet(&enums::PacketType::Message, &serialised_message_bytes, session).await;

    result
}

/// Create and send a data packet to the stream
pub async fn send_data_packet(packet_type: &enums::PacketType, bytes_to_send: &Vec<u8>, session: &mut structs::Session) -> Result<(), String> {
    let data_packet = structs::DataPacket::new(packet_type, &bytes_to_send, session.initial_handshake.packet_symmetric_encryption_key.clone())?; 
    send_data_to_stream(&packet_type, &data_packet, &mut session.connection_params).await
}

/// Send an OK status message
pub async fn send_ok(message: &str, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) -> Result<(), String> {
    send_status(super::enums::Status::Ok, &message, &initial_handshake, connection_params).await
}

/// Send an Error status message
pub async fn send_err(message: &str, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) -> Result<(), String> {
    send_status(super::enums::Status::Error, &message, &initial_handshake, connection_params).await
}

/// Send a Status message
pub async fn send_status(status: super::enums::Status, message: &str, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) -> Result<(), String> {
    let mut bytes_to_send: Vec<u8> = vec!();
    bytes_to_send.push(status as u8);
    bytes_to_send.append(&mut message.as_bytes().to_vec());
    send_data_packet_to_stream(&enums::PacketType::Status, &vec!(super::enums::Status::Ok as u8), initial_handshake, connection_params).await
}

/// Send an already serialised data packet to the stream
pub async fn send_data_packet_to_stream(packet_type: &enums::PacketType, bytes_to_send: &Vec<u8>, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) -> Result<(), String> {
    let data_packet = structs::DataPacket::new(packet_type, &bytes_to_send, initial_handshake.packet_symmetric_encryption_key.clone())?;
    send_data_to_stream(&packet_type, &data_packet, connection_params).await
}

/// A more detailed way of sending a data packet to the stream.
///
/// Should only be used directly when sending a first InitialHandshake request
pub async fn send_data_to_stream(_packet_type: &enums::PacketType, bytes_to_send: &Vec<u8>, connection_params: &mut structs::ConnectionParams) -> Result<(), String> {
    stream::send_bytes_to_stream(&"", &bytes_to_send, connection_params).await
}

/// Functions specific to actual stream communication
pub mod stream {
    use tokio::{io::{AsyncReadExt, AsyncWriteExt}};

    use super::traits::SimpleProgressMonitor;
    use super::structs;
    use super::traits;

    /// Read bytes from stream.
    //
    /// You typically dont call this method directly
    pub async fn receive_bytes_from_stream<T: traits::BytesReceiveHandler>(bytes_control: &structs::BytesControl, packet_type: &super::enums::PacketType, received_handler: &mut T, maximum_receive_length: u64, connection_params: &mut structs::ConnectionParams) -> Result<(), String> {
        let mut expected_stream_length: usize = 0;
        let mut buffer: Box<[u8]> = Box::new([0; crate::MAXIMUM_PACKET_LENGTH as usize]);
        let mut n;
        //let _ = connection_params.tcp_stream.flush().await;
        std::thread::sleep(std::time::Duration::from_millis(crate::DELAY_INITIAL_STREAM_CALL_MILLIS));
        loop {
            if connection_params.delay_by.is_some() {
                std::thread::sleep(connection_params.delay_by.unwrap())
            }

            n = match connection_params.tcp_stream.read(&mut buffer).await {
                Ok(n) if n == 0 => break,
                Ok(n) => n,
                Err(_e) => 0,
            };
            //Waiting to be able to use tokoi 0.3.x
            // let start = crate::general::get_current_datestamp();
            // loop {
            //     if start.signed_duration_since(crate::general::get_current_datestamp()) >= chrono::Duration::seconds(10) {n=0; break;}
            //     n = match connection_params.tcp_stream.try_read(&mut buffer) {
            //         Ok(n) if n == 0 => break,
            //         Ok(n) => n,
            //         Err(_e) => 0,
            //     };
            // }
            // if n == 0 {
            //     break;
            // }

            if n > crate::LENGTH_OF_U64_NUMBER_IN_BYTES {
                received_handler.receive_bytes(&mut buffer[0..n].to_vec());

                if *packet_type==super::enums::PacketType::InitialHandshake {
                    if n > crate::MAXIMUM_LENGTH_HANDSHAKE_PACKET as usize {
                        return Err(format!("Packet size exceeded maximum size allowed of {} bytes", maximum_receive_length));
                    }
                }

                if expected_stream_length == 0 {
                    expected_stream_length = received_handler.get_expected_length();
                    if received_handler.get_expected_length() + bytes_control.already_uploaded > bytes_control.total_allowed_to_upload {
                        crate::trace!(&format!("Current bytes control: total_bytes_already_uploaded: {}, total_allowed_upload_for_access_key: {}", bytes_control.already_uploaded, bytes_control.total_allowed_to_upload));
                        return Err(format!("Packet receiving will exceed the access_keys maximum allowed upload bytes of {}, receive length was indicate to be {}", bytes_control.total_allowed_to_upload, received_handler.get_expected_length()));
                    } else if expected_stream_length > maximum_receive_length as usize {
                        return Err(format!("Packet size exceeded maximum size allowed of {} bytes, receive length was indicate to be {}", maximum_receive_length, received_handler.get_expected_length()));
                    }
                };

                if received_handler.get_current_length() > received_handler.get_expected_length()
                || received_handler.get_current_length() > maximum_receive_length as usize {
                    return Err(format!("Packet size exceeded maximum allowed"));
                }

                if received_handler.get_current_length() == received_handler.get_expected_length() {
                    received_handler.receive_complete();
                    let _ = connection_params.tcp_stream.flush().await;
                    return Ok(());
                };
            } else {
                return Err(String::from("No data received"));
            };
        }

        Err(String::from("Nothing was received"))
    }


    /// Write bytes to stream.
    ///
    /// You typically don't call this method directly
    pub async fn send_bytes_to_stream(label: &str, bytes_to_send: &Vec<u8>, connection_params: &mut structs::ConnectionParams) -> Result<(), String> {
        let packet_length = bytes_to_send.len();
        let mut progress_bar = structs::ProgressBar::new(&format!("Send {}", label), packet_length as u64);
        let mut packet_length_sent = 0;

        std::thread::sleep(std::time::Duration::from_millis(crate::DELAY_INITIAL_STREAM_CALL_MILLIS));
        for i in 0..(packet_length / crate::MAXIMUM_PACKET_LENGTH) + 1 as usize {
            let mut length_to_read = (i * crate::MAXIMUM_PACKET_LENGTH) + crate::MAXIMUM_PACKET_LENGTH;
            if length_to_read > packet_length {
                length_to_read = packet_length;
            }
            let range = std::ops::Range {
                start: i * crate::MAXIMUM_PACKET_LENGTH,
                end: length_to_read,
            };

            let _ = connection_params.tcp_stream.write_all(&bytes_to_send[range.clone()]).await;
            packet_length_sent = packet_length_sent + crate::MAXIMUM_PACKET_LENGTH;
            progress_bar.set_current_value(packet_length_sent as u64);
            if connection_params.delay_by.is_some() {
                std::thread::sleep(connection_params.delay_by.unwrap())
            }
        }
        progress_bar.completed();

        Ok(())
    }
}
