
/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Client specific functionaity.

use super::enums;
use super::encryption;
use super::files;
use super::log;
use super::streams;
use super::structs;
use super::general;
use super::structs::packet_data_types as packets;
use super::structs::InitialHandshake;

/// Starts a new session with a server and returns a unique session key
pub async fn start_new_session<'a>(local_community_id: &u16, user_password: Vec<u8>, enable_compression: &enums::EnableCompression, 
    encryption_scheme: &enums::EncryptionScheme) 
    -> Result<structs::Session, String> {

    let use_public_key= enums::EncryptionScheme::use_public_or_private_key(&encryption_scheme);
    let packet_symmetric_encryption_key: Vec<u8> = match use_public_key {
        true => encryption::generate_random_vector(crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY as u32),
        false => encryption::generate_random_vector(crate::MAXIMUM_LENGTH_RSA_4096_PUBLIC_KEY as u32)
    };

    let option_community = crate::CONFIGURATION_CLIENT.access_keys.iter().find(|a|a.local_community_id==*local_community_id);
    if option_community.is_none() {
        return Err("Local community id supplied was not found in configuration file".into());
    }
    let community = option_community.unwrap();

    if user_password.len() > crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY {
        return Err(format!("User password may not be longer than {} bytes", crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY));
    }

    let delay_by = Some(std::time::Duration::new(0, community.delay_by_nano_seconds as u32));
    let mut initial_handshake = InitialHandshake::new_session(&community.access_key, &community.registration_access_key, "", *encryption_scheme,*enable_compression, packet_symmetric_encryption_key.clone());
    let mut connection_params = structs::ConnectionParams::new(delay_by.to_owned(), streams::create_stream(&community.server_ip, &community.server_port).await?);

    // Send first handshake request
    let initial_handshake_bytes = initial_handshake.into_bytes(&local_community_id.to_owned())?;
    let result_send_bytes = streams::send_data_to_stream(&enums::PacketType::InitialHandshake, &initial_handshake_bytes, &mut connection_params).await;

    if result_send_bytes.is_ok() {
        initial_handshake.packet_symmetric_encryption_key = packet_symmetric_encryption_key.to_vec();
        let mut session = structs::Session {
            key: String::from(""),
            user_password: user_password.to_owned(),
            connection_params: connection_params,
            initial_handshake: initial_handshake
        };

        let result_server_response = streams::receive_data_packet(
            &structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_HANDSHAKE_PACKET as usize), 
            &crate::LABEL_RECEIVING, &enums::Application::Client, 
            &enums::PacketType::Message, &mut session).await;

        if result_server_response.is_ok() {
            session.initial_handshake.session_id = result_server_response.unwrap().data.to_vec();
            // Send back session_id to server for validation
            let _ = streams::send_message(&session.initial_handshake.session_id.to_owned(), &mut session).await?;
            let status_message = streams::receive_status_message(unsafe { &crate::APPLICATION_INSTANCE }, &enums::PacketType::Status, &mut session).await?;
            if enums::Status::from_byte(status_message.data[0])==enums::Status::Ok {
                crate::log!("");
                crate::log!("Session handshake success");
                return Ok(session)
            } else {
                crate::log!("");
                crate::log!("Session handshake declined");
                return Err("Could not establish a session with the server, it was declined".into());
            }
        } else {
            return Err(format!("Could not receive bytes from the server when attempting a handshake: {}", result_server_response.unwrap_err()));
        }
    } else {
        return Err(format!("Could not send the bytes to the server when attempting a handshake: {}", result_send_bytes.unwrap_err()));
    }
}

/// Send a file to the server and expect a unique code to be used in a future download
pub async fn send_file(local_community_id: u16, file_name: &str, option_unique_code: Option<String>, layer: &str, description: &str, drop_on_retrieval: &bool, enable_compression: &enums::EnableCompression,
    encryption_scheme: &enums::EncryptionScheme, user_password_not_to_length: Vec<u8>, protected_password_not_to_length: Vec<u8>, 
    option_pkp_key_name: Option<String>, generate_file_hash: bool, on_benchmarks_handler: fn(structs::BenchmarkResult)) 
    -> Result<structs::Session, String> {
    
    let option_file_bytes = files::read_from_file(&file_name);

    if option_file_bytes.is_some() {
        let encrypted_file_name = encryption::scrambler(&true, file_name.as_bytes().to_vec(), user_password_not_to_length.clone(), file_name.len());
        send_file_common(local_community_id, encrypted_file_name, option_unique_code,&mut option_file_bytes.unwrap(), layer, description, drop_on_retrieval, enable_compression,
            encryption_scheme, user_password_not_to_length, protected_password_not_to_length, 
            option_pkp_key_name, generate_file_hash, on_benchmarks_handler).await
    } else {
        Err("An error occured when trying to access the file".into())
    }
}

/// Send the clients' RSA public key file to the server and expect a unique code to be used in a future download
pub async fn send_file_with_public_key(local_community_id: u16, option_unique_code: Option<String>, layer: &str, description: &str, drop_on_retrieval: &bool, enable_compression: &enums::EnableCompression,
    encryption_scheme: &enums::EncryptionScheme, user_password_not_to_length: Vec<u8>, protected_password_not_to_length: Vec<u8>, option_p2p_client_key: Option<String>, 
    generate_file_hash: bool, on_benchmarks_handler: fn(structs::BenchmarkResult)) 
    -> Result<structs::Session, String> {
    
    let mut public_key_bytes = crate::RSA_KEY_PAIRS_CLIENT_PRIVATE.client_rsa_4096_key_pair.public_key.clone();
    let file_name = "PUBLIC_KEY";
    let encrypted_file_name = encryption::scrambler(&true, file_name.as_bytes().to_vec(), user_password_not_to_length.clone(), file_name.len());
    
    send_file_common(local_community_id, encrypted_file_name, option_unique_code, &mut public_key_bytes, layer, description, drop_on_retrieval, enable_compression,
        encryption_scheme, user_password_not_to_length, protected_password_not_to_length, option_p2p_client_key, 
        generate_file_hash, on_benchmarks_handler).await
}

/// Send a file to the server and expect a unique code to be used in a future download
pub async fn send_file_common(local_community_id: u16, encrypted_file_name: Vec<u8>, option_unique_code: Option<String>, file_bytes: &mut Vec<u8>, layer: &str, description: &str, drop_on_retrieval: &bool, enable_compression: &enums::EnableCompression,
    encryption_scheme: &enums::EncryptionScheme, user_password_not_to_length: Vec<u8>, protected_password_not_to_length: Vec<u8>, 
    option_p2p_key_name: Option<String>, generate_file_hash: bool, on_benchmarks_handler: fn(structs::BenchmarkResult)) 
    -> Result<structs::Session, String> {

    let hash = match generate_file_hash {
        true => crate::encryption::generate_sha_512_hash(&file_bytes),
        _ => vec![]
    };

    let mut p2p_secret: Vec<u8> = crate::encryption::generate_random_vector(crate::LENGTH_OF_P2P_SECRET as u32);
    let mut processed_bytes: Vec<u8>;

    if option_p2p_key_name.is_some() {
        // Doubly encrypted        
        let p2p_client_key_name = option_p2p_key_name.clone().unwrap();
        
        processed_bytes = encryption::process_file_bytes(false, file_bytes, p2p_secret.clone(), encryption_scheme.to_owned(), &enable_compression, on_benchmarks_handler)?;        
        processed_bytes = encryption::process_file_bytes(false, &mut processed_bytes, user_password_not_to_length.clone(), encryption_scheme.to_owned(), &enable_compression, on_benchmarks_handler)?;        
        p2p_secret = enums::EncryptionScheme::encrypt_with_client_public_rsa_key(&p2p_secret, &p2p_client_key_name)?.unwrap();

    } else {
        processed_bytes = encryption::process_file_bytes(false, file_bytes, user_password_not_to_length.clone(), encryption_scheme.to_owned(), &enable_compression, on_benchmarks_handler)?;
    }    

    let mut session = start_new_session(&local_community_id, user_password_not_to_length.clone(), &enums::EnableCompression::No, &encryption_scheme).await?;

    let verification_key = generate_verification_key(&session.initial_handshake.access_key, &user_password_not_to_length);
    let protected_key = generate_verification_key(&session.initial_handshake.access_key, &protected_password_not_to_length);
    let padding_length = general::calculate_padding_length(processed_bytes.len());

    let unique_code = match option_unique_code.is_some() {
        true => general::append_vector_to_length(option_unique_code.unwrap().as_bytes().to_vec(), crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE),
        _ => general::append_vector_to_length(vec![], crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE)
    };

    let mut file = packets::File {
        length: 0,
        padding_length: padding_length,
        padding_bytes: crate::encryption::generate_random_vector(padding_length),
        verification_key: verification_key,
        unique_code: unique_code,
        protected_key: protected_key,
        packet_type: enums::PacketType::File,
        is_compressed: *enable_compression,
        drop_on_retrieval: *drop_on_retrieval,
        file_name: encrypted_file_name,
        description: String::from(description),
        layer: String::from(layer),
        is_p2p_encrypted: option_p2p_key_name.is_some(),
        p2p_secret: p2p_secret,
        hash: hash,
        bytes: processed_bytes
    };

    let serialised_file_bytes = file.into_bytes()?;
    let _ = streams::send_data_packet(&enums::PacketType::File, &serialised_file_bytes, &mut session).await?;

    Ok(session)    
}

/// Retrieve a file from the server
pub async fn retrieve_file(unique_code: &str, user_password: &str, protected_password: &str, session: &mut structs::Session, on_benchmarks_handler: fn(structs::BenchmarkResult)) -> Result<packets::File, String> {
    let mut verification_key_scrambled = generate_verification_key(&session.initial_handshake.access_key, &user_password.as_bytes().to_vec());
    let mut protected_key_scrambled = generate_verification_key(&session.initial_handshake.access_key, &protected_password.as_bytes().to_vec());
    
    let mut bytes: Vec<u8> = vec![];
    bytes.append(&mut verification_key_scrambled);
    bytes.append(&mut protected_key_scrambled);
    bytes.append(&mut unique_code.as_bytes().to_vec());

    let message = structs::packet_data_types::Message::new(enums::PacketType::Retrieve, &bytes);
    let serialised_file_bytes = message.into_bytes()?;
    let _ = streams::send_data_packet(&enums::PacketType::Retrieve, &serialised_file_bytes, session).await?;
    let data_packet = streams::receive_data_packet(&structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_DATA_PACKET as usize), &crate::LABEL_RECEIVING, unsafe { &crate::APPLICATION_INSTANCE }, &enums::PacketType::File, session).await?;
    match data_packet.packet_type {
        enums::PacketType::File => {
            let mut file = packets::File::from_bytes(data_packet.data)?;

            let mut processed_bytes = encryption::process_file_bytes(true, &mut file.bytes, session.user_password.clone(), session.initial_handshake.encryption_scheme, &file.is_compressed, on_benchmarks_handler)?;
            if file.is_p2p_encrypted {
                // Doubly decrypted
                let p2p_secret = enums::EncryptionScheme::decrypt_with_client_private_rsa_key(&file.p2p_secret)?.unwrap();
                processed_bytes = encryption::process_file_bytes(true, &mut processed_bytes, p2p_secret, session.initial_handshake.encryption_scheme, &file.is_compressed, on_benchmarks_handler)?;
            }

            let file_name_stripped = general::strip_zeros(&file.file_name);
            let file_name_length = file_name_stripped.len();
            let processed_file_name = encryption::scrambler(&false, file.file_name, session.user_password.clone(), file_name_length);
            file.file_name = processed_file_name;
            file.bytes = processed_bytes;
            
            return Ok(file);
        },
        enums::PacketType::Message => {
            let message = packets::Message::from_bytes(data_packet.data)?;
            return Err(String::from(std::str::from_utf8(&message.bytes).unwrap()));
        },
        _ => return Err(String::from("Unrecognised server response"))
    }
}

/// Update a file on the server with new file bytes data
pub async fn update_file(unique_code: &str, local_community_id: u16, file_name: &str, enable_compression: &enums::EnableCompression, 
    encryption_scheme: &enums::EncryptionScheme, user_password_not_to_length: Vec<u8>, protected_password_not_to_length: Vec<u8>, 
    generate_file_hash: bool, on_benchmarks_handler: fn(structs::BenchmarkResult)) 
    -> Result<structs::Session, String> {

    let option_file_bytes = files::read_from_file(&file_name);
    if option_file_bytes.is_some() {
        let mut session = start_new_session(&local_community_id, user_password_not_to_length.clone(), &enums::EnableCompression::No, &encryption_scheme).await?;
        let mut file_bytes = option_file_bytes.unwrap();
        let file_hash = match generate_file_hash {
            true => crate::encryption::generate_sha_512_hash(&file_bytes),
            _ => vec![]
        };
        let processed_bytes = encryption::process_file_bytes(false, &mut file_bytes, user_password_not_to_length.clone(), session.initial_handshake.encryption_scheme, &enable_compression, on_benchmarks_handler)?;
        let verification_key = generate_verification_key(&session.initial_handshake.access_key, &user_password_not_to_length);
        let protected_key = generate_verification_key(&session.initial_handshake.access_key, &protected_password_not_to_length);
        let encrypted_file_name = encryption::scrambler(&true, file_name.as_bytes().to_vec(), user_password_not_to_length.clone(), file_name.len());
        let padding_length = general::calculate_padding_length(processed_bytes.len());

        let mut file = packets::File {
            length: 0,
            padding_length: padding_length,
            padding_bytes: crate::encryption::generate_random_vector(padding_length),
            verification_key: verification_key,
            unique_code: general::append_vector_to_length(unique_code.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE),
            protected_key: protected_key,
            packet_type: enums::PacketType::File,
            is_compressed: *enable_compression,
            drop_on_retrieval: false,
            file_name: encrypted_file_name,
            description: String::new(),
            layer: String::new(),
            is_p2p_encrypted: false,
            p2p_secret: vec![],
            hash: file_hash,
            bytes: processed_bytes
        };

        let serialised_file_bytes = file.into_bytes()?;
        let _ = streams::send_data_packet(&enums::PacketType::Update, &serialised_file_bytes, &mut session).await?;

        Ok(session)
    } else {
        Err("An error occured when trying to access the file".into())
    }
}

/// Get a list of all uploads on the server for the current access key
pub async fn get_list_of_uploads(local_community_id: &u16, layer: &str) -> Result<structs::packet_data_types::ListUploads, String> {
    let mut session = start_new_session(local_community_id, vec![], &enums::EnableCompression::No, &enums::EncryptionScheme::Rsa4096Aes256).await?;

    let layer_as_bytes = general::append_vector_to_length(layer.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_LAYER);
    let request = structs::packet_data_types::ListUploads::new(&session.initial_handshake.access_key, &layer_as_bytes, &vec![]);
    let serialised_bytes = request.into_bytes()?;

    let _ = streams::send_data_packet(&enums::PacketType::ListUploads, &serialised_bytes, &mut session).await?;
    let result_received_list = streams::receive_data_packet(&structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_DATA_PACKET as usize), &crate::LABEL_RECEIVING, unsafe { &crate::APPLICATION_INSTANCE }, &enums::PacketType::ListUploads, &mut session).await;
    if result_received_list.is_ok() {
        let list_uploads = structs::packet_data_types::ListUploads::from_bytes(result_received_list.unwrap().data)?;
        Ok(list_uploads)
    } else {
        Err("Send failed".into())
    }
}

/// Delete an already uploaded item on the server. The parameters needed to do a server delete are: unique_code, password, access_key
pub async fn delete_upload(local_community_id: &u16, unique_code_not_to_length: &Vec<u8>, user_password_not_to_length: &Vec<u8>, protected_password_not_to_length: &Vec<u8>) -> Result<String, String> {
    let mut session = start_new_session(local_community_id, user_password_not_to_length.clone(), &enums::EnableCompression::No, &enums::EncryptionScheme::Rsa4096Aes256).await?;
    let mut padded_verification_key = generate_verification_key(&session.initial_handshake.access_key, &user_password_not_to_length);
    let mut padded_protected_key = generate_verification_key(&session.initial_handshake.access_key, &protected_password_not_to_length);
    let mut bytes: Vec<u8> = vec![];
    
    bytes.append(&mut padded_verification_key);
    bytes.append(&mut padded_protected_key);
    bytes.append(&mut unique_code_not_to_length.to_owned());
    
    let request = structs::packet_data_types::Message::new(enums::PacketType::Drop, &bytes);
    let serialised_bytes = request.into_bytes()?;

    let _ = streams::send_data_packet(&enums::PacketType::Drop, &serialised_bytes, &mut session).await?;
    let result_received_list = streams::receive_data_packet(&structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_DATA_PACKET as usize), &crate::LABEL_RECEIVING, unsafe { &crate::APPLICATION_INSTANCE }, &enums::PacketType::ListUploads, &mut session).await;
    if result_received_list.is_ok() {
        Ok(std::str::from_utf8(&result_received_list.unwrap().data).unwrap().into())
    } else {
        Err("Error on receiving server response".into())
    }
}

/// Retrieve the details about a previously sent data packet
pub async fn retrieve_packet_details(unique_code: &str, session: &mut structs::Session) -> Result<packets::PacketDetails, String> {
    let message = packets::Message::new(enums::PacketType::PacketDetails, &unique_code.as_bytes().to_vec());
    let serialised_file_bytes = message.into_bytes()?;
    let _ = streams::send_data_packet(&enums::PacketType::PacketDetails, &serialised_file_bytes, session).await?;
    let data_packet = streams::receive_data_packet(&structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_DATA_PACKET as usize),&crate::LABEL_RECEIVING, unsafe { &crate::APPLICATION_INSTANCE }, &enums::PacketType::Message, session).await?;
    match data_packet.packet_type {
        enums::PacketType::PacketDetails => {
            let packet_details = packets::PacketDetails::from_bytes(data_packet.data)?;
            return Ok(packet_details);
        },
        enums::PacketType::Message => {
            let message = packets::Message::from_bytes(data_packet.data)?;
            return Err(String::from(std::str::from_utf8(&message.bytes).unwrap()));
        },
        _ => return Err(String::from(format!("Unrecognised server response: {:?}", data_packet.packet_type)))
    }
}

/// Generate a password verification key : verifications are used before doing a download etc.
fn generate_verification_key (key: &Vec<u8>, password_not_to_length: &Vec<u8>) -> Vec<u8> {
    if password_not_to_length.len() == 0 {
        general::append_vector_to_length(vec![], crate::MAXIMUM_LENGTH_VERIFICATION_KEY)
    } else {
        let stripped_key = general::strip_zeros(&key);
        let user_password_padded = general::append_vector_to_length(password_not_to_length.to_owned(), crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
        let mut key_rotated = general::append_vector_to_length(vec![], crate::MAXIMUM_LENGTH_VERIFICATION_KEY);
        let mut k: usize = 0;
        for i in 0..key_rotated.len() {
            key_rotated[i] = stripped_key[k];
            k+=1;
            if k==stripped_key.len() {
                k = 0;
            }
        }

        let scrambled = encryption::scrambler(&true, key_rotated, user_password_padded.clone(), crate::MAXIMUM_LENGTH_VERIFICATION_KEY);

        scrambled
    }
}

/// Retreive the servers RSA public keys so that secure key exchanges may take place in future RSA based communications
pub async fn register_with_server_for_rsa_communications(rsa_key_id: &u16) -> Result<(), String> {
    let result_rsa_4096 = retrieve_key_pair_from_server(&rsa_key_id, &enums::RsaKeyLength::RSA4096).await;
    //let result_rsa_2048 = retrieve_key_pair_from_server(&rsa_key_id, &enums::RsaKeyLength::RSA2048).await;

    if result_rsa_4096.is_ok() { //} && result_rsa_2048.is_ok() {
        let mut current_keys = crate::RSA_KEY_PAIRS_CLIENT.to_owned();
        let mut rsa_key_pairs = structs::encryption::RSAKeyPairs::new(false, &rsa_key_id);
        rsa_key_pairs.rsa_4096_key_pair = result_rsa_4096.unwrap();
        //rsa_key_pairs.rsa_2048_key_pair = result_rsa_2048.unwrap();

        current_keys.add_key_pair(&rsa_key_pairs);

        let serialised_rsa_key_pairs = serde_json::to_string_pretty(&current_keys).unwrap();

        let path = files::get_path_to_local_file(&crate::RSA_KEYS_FILE_NAME_CLIENT);
        if files::write_to_file(&path.as_os_str().to_str().unwrap(),&serialised_rsa_key_pairs.as_bytes().to_vec(), false) {
            return Ok(());
        } else {
            let error_message = "Could not persist the retrieved public RSA keys";
            log!(error_message);
            return Err(String::from(error_message));
        }
    } else {
        if result_rsa_4096.is_err() {
            crate::display!("RSA 4096", result_rsa_4096.unwrap_err());
        }
    }
    let error_message = "Could not retrieve the public RSA keys from the server";
    crate::display!(error_message);
    Err(String::from(error_message))
}

pub async fn retrieve_key_pair_from_server(local_community_id: &u16, rsa_key_length: &enums::RsaKeyLength) -> Result<structs::encryption::RSAKeyPair, String> {
    let mut session = start_new_session(local_community_id, vec![], &enums::EnableCompression::No,&enums::EncryptionScheme::None).await?;
    let rsa_public_key = packets::RsaPublicKey::new(*rsa_key_length, vec![]);
    let serialised_file_bytes = rsa_public_key.into_bytes()?;
    let result_send = streams::send_data_packet(&enums::PacketType::RsaPublicKeyGeneric,&serialised_file_bytes, &mut session).await;
    if result_send.is_ok() {
        let mut progress_bar = structs::ProgressBar::new(&crate::LABEL_RECEIVING.to_owned(), 0_64);
        let mut received_handler = structs::BytesReceiving::new(&mut progress_bar, true);
        let bytes_control = structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_DATA_PACKET as usize);
        let received_data_packet = streams::receive_data_packet_from_stream(&bytes_control, unsafe { &crate::APPLICATION_INSTANCE }, &enums::PacketType::RsaPublicKeyGeneric, &mut received_handler, &session.initial_handshake, &mut session.connection_params).await;
        if received_data_packet.is_ok() {
            let data_packet = received_data_packet.unwrap();
            if data_packet.packet_type == enums::PacketType::RsaPublicKeyGeneric {
                if *rsa_key_length == enums::RsaKeyLength::RSA4096 || *rsa_key_length == enums::RsaKeyLength::RSA2048 {
                    let _ = packets::RsaPublicKey::new(*rsa_key_length, data_packet.data.to_vec());
                    let mut rsa_key_pair = structs::encryption::RSAKeyPair::new(&format!("{:?}", *rsa_key_length));
                    rsa_key_pair.public_key = data_packet.data;
                    return Ok(rsa_key_pair);
                } else {
                    return Err(format!("Unrecognised public key returned from server: {:?}", rsa_key_length));
                };
            } else if data_packet.packet_type==enums::PacketType::Status {
                let status = enums::Status::from_byte(data_packet.data[0]);
                let message = general::vector_stripped_to_string(&data_packet.data[1..data_packet.data.len()].to_vec());
                return Err(format!("{:?}: {}", status, message));
            } else {
                return Err(format!("Did not receive the expected RsaPublicKey data packet: {:?}", data_packet.packet_type));
            }
        } else {
            return Err(format!("An error ocurred when deserialising a received data packet: {}", received_data_packet.unwrap_err()));
        }
    } else {
        return Err(result_send.unwrap_err().into());
    }
}

// /// Get a random password encrypted with the personal public key of a client
// pub fn get_p2p_encrypted_password(p2p_client_name: &str, random_password_length: u16) -> Result<Option<Vec<u8>>, String> {
//     let mut random_password = encryption::generate_random_vector(random_password_length as u32);
//     let option_public_key = crate::RSA_KEY_PAIRS_CLIENT_PRIVATE.public_rsa_4096_keys.iter().find(|k| &k.name==p2p_client_name);
//     if option_public_key.is_some() {
//         let result_encrypted_random_password = enums::EncryptionScheme::encrypt_with_client_public_rsa_key(&random_password, &enums::EncryptionScheme::Rsa4096Aes256, &p2p_client_name);
//         if result_encrypted_random_password.is_ok() {
//             Ok(result_encrypted_random_password.unwrap())
//         } else {
//             Err(format!("Error when encrypting with Public RSA key for {}", p2p_client_name))
//         }
//     } else {
//         Err(format!("Public key for {} not found, was it imported?", p2p_client_name))
//     }
// }