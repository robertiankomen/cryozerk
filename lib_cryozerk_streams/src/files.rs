/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! General file related functions that is used system wide.
//!
//! The Log files and are also written here and the config files are initialised with dedault values on first creation

use crate::log;
use std::fs::File;
use super::structs;
use super::general;
use super::files;
use std::io::prelude::*;

/// Strip directory name form file path, leaving only file name
pub fn get_only_file_name(path: &str) -> String {
    let path_buff = std::path::PathBuf::from(path);

    path_buff.components().last().unwrap().as_os_str().to_string_lossy().into()
}

/// Return only the extension of a file name in a path
pub fn get_only_extension(path: &str) -> Option<&str> {
    std::path::Path::new(path).extension().and_then(std::ffi::OsStr::to_str)
}

/// Get the full path to a file existing in the directory of the current cryozerk executable or if 
/// on Linux then path to the current users bin directory

#[cfg(not(unix))]
pub fn get_path_to_local_file(file_name: &str) -> std::path::PathBuf {
    let file_name_to_use;
    if cfg!(debug_assertions) {
        file_name_to_use = format!("debug-{}", file_name);
    } else {
        file_name_to_use = file_name.into();
    }
    let exe_path = std::env::current_exe().unwrap();
    let mut path = std::path::PathBuf::new();
    path.push(exe_path);
    let parent = path.parent().unwrap();

    parent.join(std::path::Path::new(&file_name_to_use))
}

#[cfg(unix)]
pub fn get_path_to_local_file(file_name: &str) -> std::path::PathBuf {
    let mut file_name_to_use;

    if cfg!(debug_assertions) {
        file_name_to_use = format!("debug-{}", file_name);
    } else {
        file_name_to_use = file_name.into();
    }
    let user_home: String = std::env::var("HOME").unwrap();
    let mut path = std::path::PathBuf::from(format!("{}/.config/cryozerk", user_home));
    if file_name == crate::LOG_FILE_NAME.as_str() {
        path = std::path::PathBuf::from(r#"/var/log/cryozerk"#);
        unsafe {
            if crate::APPLICATION_INSTANCE==crate::enums::Application::Server {
                file_name_to_use = format!("server-{}", file_name_to_use);
            } else {
                file_name_to_use = format!("client-{}", file_name_to_use);
            }
        }
    }
    let directory_name = path.as_os_str().to_str().unwrap();
    if !path_exists(directory_name) {
        let result_directory = files::create_directory_all(false, directory_name);
        if result_directory.is_err() {
            panic!("Could not create path: {}", path.to_str().unwrap())
        }
    }

    path.join(std::path::Path::new(&file_name_to_use))
}

/// Create the path to a file and creating the directory structure if it doesn't exist
pub fn get_path_to_data_file(unique_code: &Vec<u8>, access_key: &Vec<u8>) -> Result<String, String> {
    let stripped_access_key = general::strip_zeros(&access_key.to_owned());
    let stripped_unique_code = general::strip_zeros(&unique_code.to_owned());

    let mut file_name_vec = std::str::from_utf8(&stripped_access_key.to_owned()).unwrap().as_bytes().to_vec();
    file_name_vec.push(b'-');
    file_name_vec.append(&mut stripped_unique_code.to_owned());
    let file_name: String = std::str::from_utf8(&file_name_vec).unwrap().to_string();

    let mut directory_name = format!("files/{}", std::str::from_utf8(&stripped_access_key).unwrap());
    if cfg!(windows) {
        directory_name = format!("files\\{}", std::str::from_utf8(&stripped_access_key).unwrap());
    }
    let directory = files::create_directory_all(true, &directory_name)?;
    let full_path = std::path::Path::new(&directory);
    let combined_path = full_path.join(file_name);

    Ok(String::from(combined_path.to_str().unwrap()))
}

/// Create the path to a temporary file and creating the directory structure if it doesn't exist
pub fn get_path_to_temporary_data_file(unique_code: &Vec<u8>, access_key: &Vec<u8>) -> Result<String, String> {
    let path = get_path_to_data_file(unique_code, access_key)?;

    Ok(format!("{}_tmp", path))
}

/// Convert a temporary data file to a final accepted pinned version
pub fn pin_temporary_data_file(unique_code: &Vec<u8>, access_key: &Vec<u8>) -> Result<(), String> {
    let temp_path = get_path_to_temporary_data_file(unique_code, access_key)?;
    let pinned_path = get_path_to_data_file(unique_code, access_key)?;
    if path_exists(&temp_path) {
        if path_exists(&pinned_path) {
            let success = delete_file(&pinned_path);
            if !success {
                log!("Could not delete existing pinned file", temp_path);
                return Err("Server error".into());
            }
        }
        let result_rename = std::fs::rename(&temp_path, &pinned_path);
        if result_rename.is_ok() {
            Ok(())
        } else {
            log!("Could not rename temporary upload file", temp_path);
            return Err("Server error".into());
        }
    } else {
        Err("Temporary file does not exist".into())
    }
}

/// Read a files contents and return it as a vector
pub fn read_from_file(path: &str) -> Option<Vec<u8>> {
    let mut file_data: Vec<u8> = vec![];

    let file_handle = File::open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(error) =>  {crate::log!("Error opening file: ", error); return None },
    };

    let results = file_handle.read_to_end(&mut file_data,);
    let _ = match results {
        Ok(results) => results,
        Err(error) => {crate::log!("Error reading from file locally", error); return None },
    };

    Some(file_data)
}

/// Read a files contents and return it as a string
pub fn read_from_file_as_string(path: &str) -> Option<String> {
    let mut file_data = String::new();

    let file_handle = File::open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(_) => {
            return None;
        }
    };

    let results = file_handle.read_to_string(&mut file_data);
    let _ = match results {
        Ok(results) => results,
        Err(error) => {
            log!(&format!("Error reading file '{}': {:?}", path, error));
            return None;
        }
    };

    Some(file_data)
}

/// Write a vector to a file
pub fn write_to_file(path: &str, file_data: &Vec<u8>, append_to_file: bool) -> bool {
    if !append_to_file && path_exists(&path) {
        let _ = delete_file(&path);
    }
    let file_handle = std::fs::OpenOptions::new().create(true).write(true).append(append_to_file).open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(error) => {
            log!(&format!("Error creating the file '{}': Error:{:?}", path, error));
            return false;
        }
    };

    let results = file_handle.write(&file_data);
    let _ = match results {
        Ok(results) => results,
        Err(error) => {
            log!(&format!("Error writing file '{}': {:?}", path, error));
            return false;
        }
    };

    true
}

/// Delete a file from disk
pub fn delete_file(file_path: &str) -> bool {
    if path_exists(&file_path) {
        if std::fs::remove_file(&file_path).is_ok() {
            true
        } else {
            crate::log!("Could not delete file", file_path);

            false
        }
    } else {
        // File doesn't exist, so a delete success is reported
        true
    }
}

/// Does a file exist?
pub fn path_exists(path: &str) -> bool {
    let path_to_file_buff = std::path::PathBuf::from(path);
    let path_to_file = path_to_file_buff.to_str().unwrap();

    std::path::Path::new(path_to_file).exists()
}

/// Does a file exist in the directory of the current executable?
pub fn local_file_exists(file_name: &str) -> Result<String, String> {
    let path_to_file_buff = get_path_to_local_file(file_name);
    let path_to_file = path_to_file_buff.to_str().unwrap();
    if std::path::Path::new(path_to_file).exists() {
        Ok(String::from(path_to_file))
    } else {
        Err(String::from(path_to_file))
    }
}

/// Create a new directory tree
pub fn create_directory_all(relative: bool, directory_name: &str) -> Result<String, String> {
    if relative {
        let current_directory = std::env::current_dir().unwrap();
        let full_directory_name = current_directory.join(directory_name);
        let result_create = std::fs::create_dir_all(full_directory_name.to_owned());
        if result_create.is_ok() {
            return Ok(full_directory_name.to_str().unwrap().into());
        } else {
            return Err(format!("Could not create directory: {}, Error: {}", directory_name, result_create.unwrap_err()));
        }
    } else {
        if std::fs::create_dir_all(directory_name).is_ok() {
            return Ok(format!("{}", directory_name));
        } else {
            return Err(format!("Could not create directory: {}", directory_name));
        }
    };
}

/// Get the server configuration from file, if it doesn't exist the create one with default settings information
pub fn get_server_configuration() -> structs::configuration::Server {
    let result_path = local_file_exists(&crate::CONFIG_FILE_NAME_SERVER);
    if result_path.is_ok() {
        let path = result_path.unwrap();
        let serialised_configuration = read_from_file_as_string(&path);
        if serialised_configuration.is_some() {
            let result_configuration = serde_json::from_str::<structs::configuration::Server>(&serialised_configuration.unwrap());
            if result_configuration.is_ok() {
                let configuration =  result_configuration.unwrap();
                let result = verify_all_ip_white_list_regular_expressions(&configuration);
                if result.is_ok() {
                    unsafe{
                        crate::ENABLE_LOGGING=configuration.logging_enabled;                        
                        crate::DARK_MODE=configuration.dark_mode;
                    };
                    return configuration;
                } else {
                    crate::display!("{}", result.unwrap_err());
                }
            } else {
                crate::display!("Error in the server configuration file, exiting", path);
            }
        } else {
            crate::display!("Could not read the server configuration file, exiting", path);
        }
    } else {
        let path = result_path.unwrap_err();
        crate::display!("Create default server configuration file", path);
        let mut access_keys: Vec<super::structs::configuration::ServerAccess> = vec![];
        let mut ip_address_white_list: Vec<super::structs::configuration::IpAddress> = vec![];
        ip_address_white_list.push(super::structs::configuration::IpAddress { ip_address: "127.0.0.1".into()});
        ip_address_white_list.push(super::structs::configuration::IpAddress { ip_address: "10.*".into()});
        ip_address_white_list.push(super::structs::configuration::IpAddress { ip_address: "192.168.*".into()});

        access_keys.push(super::structs::configuration::ServerAccess { 
            access_key: String::from("PrivateCommunity"),
            allow_extended_storage: true,
            allow_custom_unique_codes: true,
            max_transfer_size_bytes: 1024*1024*500, //500MiB
            max_total_of_all_uploads_bytes: 1024*1024*1024*100, //100GiB
            automatic_deletion_after_seconds: 0, //Never
            aes_password_with_no_rsa: "".into(), //Currently unused
            first_ban_time_in_seconds: 900,
            second_ban_time_in_seconds: 86400,
            ip_address_white_list: ip_address_white_list,
            allowed_registration_access_keys: vec![
                super::structs::configuration::RegistrationAccessKey{
                    name: "registereduser".into(), // Or the actual name of user
                    id: "JohnSmith123".into()
                }
            ]
        });
        access_keys.push(super::structs::configuration::ServerAccess { 
            access_key: String::from("PublicCommunity"),
            allow_extended_storage: true,
            allow_custom_unique_codes: false,
            max_transfer_size_bytes: 1024*1024*25, //25MiB
            max_total_of_all_uploads_bytes: 1024*1024*1024*3, //5GiB
            automatic_deletion_after_seconds: 2592000, //30 days
            aes_password_with_no_rsa: "".into(), //Currently unused
            first_ban_time_in_seconds: 900,
            second_ban_time_in_seconds: 86400,
            ip_address_white_list: vec![],
            allowed_registration_access_keys: vec![]
        });
        access_keys.push(super::structs::configuration::ServerAccess { 
            access_key: String::from("GetRsaKey"), 
            allow_extended_storage: true,
            allow_custom_unique_codes: false,
            max_transfer_size_bytes: 4096,
            max_total_of_all_uploads_bytes: 4096,
            automatic_deletion_after_seconds: 120,
            aes_password_with_no_rsa: "Z9xXDd#jFdR5G".into(), //Currently unused
            first_ban_time_in_seconds: 86400,
            second_ban_time_in_seconds: 315360000,
            ip_address_white_list: vec![],
            allowed_registration_access_keys: vec![]
        });

        let version_tuple = general::get_version_tuple();

        let mut files_for_upgrade: Vec<super::structs::configuration::UpgradeClientFile> = vec![];
        if cfg!(windows) {
            files_for_upgrade.push(super::structs::configuration::UpgradeClientFile { version_major:version_tuple.0, version_minor:version_tuple.1, version_revision:version_tuple.2, file_name: String::from("cryozerk.exe")});
        } else {
            files_for_upgrade.push(super::structs::configuration::UpgradeClientFile { version_major:version_tuple.0, version_minor:version_tuple.1, version_revision:version_tuple.2, file_name: String::from("cryozerk")});
        }

        let new_configuration = super::structs::configuration::Server {
            listening_ip: String::from("0.0.0.0"),
            listening_port: 16700,
            allow_new_registrations: true,
            files_for_upgrade: files_for_upgrade,
            access_keys: access_keys,
            delay_by_nano_seconds: 5000,
            persist_to_mongodb: false,
            mongodb_connection_string: "mongodb://localhost:27017".into(),
            ban_after_handshake_fails_count_for_ip_address: 35,
            first_ban_time_in_seconds: 900,
            second_ban_time_in_seconds: 21600,
            logging_enabled: true,
            dark_mode: false
            //proxy: super::structs::configuration::Proxy::new_default()
        };
        let serialised_configuration = serde_json::to_string_pretty(&new_configuration).unwrap();
        if write_to_file(&path,&serialised_configuration.as_bytes().to_vec(), false) {
            write_service_script_file();
            unsafe{
                crate::ENABLE_LOGGING=new_configuration.logging_enabled;
                crate::DARK_MODE=new_configuration.dark_mode;
            };
            return new_configuration;
        } else {
            log!("Could not create the server configuration file, exiting", path);
        }
    }
    std::process::exit(1);
}

/// Dont write service file on non-linux systems
#[cfg(windows)]
fn write_service_script_file() { }

/// Write an example systemd service file to the output directory
#[cfg(unix)]
fn write_service_script_file() {
    let user_home: String = std::env::var("HOME").unwrap();
    let user: String = std::env::var("USER").unwrap();
    let script = format!("[Unit]
Description=CryoZerk 3.x.x Service
After=network.target
StartLimitInterval=60
StartLimitBurst=11

[Service]
User={0}
WorkingDirectory={1}/bin/
Type=simple
ExecStart={1}/bin/cryozerk_server
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target", user, user_home);

    let path = files::get_path_to_local_file("cryozerk.service");
    let _ = write_to_file(path.as_os_str().to_str().unwrap(), &script.as_bytes().to_vec(), false);
}

/// Check that all the white list ip addresses are valid regular expressions
fn verify_all_ip_white_list_regular_expressions(server_configuration: &structs::configuration::Server) -> Result<(), String> {
    for server_access in server_configuration.access_keys.iter() {
        for ip_address in server_access.ip_address_white_list.iter() {
            if regex::Regex::new(&ip_address.ip_address).is_err() {
                return Err(format!("Invalid regular expression in server config: access_key:{}, Ip address: {}", server_access.access_key, ip_address.ip_address));
            }
        }
    }

    Ok(())
}

/// Check that there are no duplicate access_key id's
pub fn check_access_keys_ids(configuration: &super::structs::configuration::Client) -> bool {
    let initial_count = configuration.access_keys.len();
    let mut copy = configuration.access_keys.clone();
    copy.dedup_by(|a,b|a.local_community_id==b.local_community_id);

    initial_count==copy.len()
}

/// Get the client configuration from file, if it doesn't exist then create one with default settings information
pub fn get_client_configuration() -> structs::configuration::Client {
    let result_path = local_file_exists(&crate::CONFIG_FILE_NAME_CLIENT);
    if result_path.is_ok() {
        let path = result_path.unwrap();
        let serialised_configuration = read_from_file_as_string(&path);
        if serialised_configuration.is_some() {
            let result_configuration = serde_json::from_str::<structs::configuration::Client>(&serialised_configuration.unwrap());
            if result_configuration.is_ok() {
                let configuration = result_configuration.unwrap();
                if check_access_keys_ids(&configuration) {
                    unsafe{crate::ENABLE_LOGGING=configuration.logging_enabled};
                    return configuration;
                } else {
                    crate::display!("Error in the configuration client file, duplicate access_key ids found, exiting", path);
                }
            } else {
                crate::display!("Error in the configuration client file, exiting", path);
            }
        } else {
            log!("Could not read the client configuration file, exiting", path);
        }
    } else {
        let path = result_path.unwrap_err();
        crate::display!("Creating default client configuration file", path);
        let mut access_keys: Vec<structs::configuration::ClientAccessKeys> = vec!();
        access_keys.push(structs::configuration::ClientAccessKeys { access_key: "PrivateCommunity".into(), registration_access_key: "JohnSmith123".into(), local_community_id: 1, server_ip: String::from("127.0.0.1"), server_port: 16700, delay_by_nano_seconds: 5000, aes_password_with_no_rsa: "".into() });
        access_keys.push(structs::configuration::ClientAccessKeys { access_key: "PublicCommunity".into(), registration_access_key: "".into(), local_community_id: 2, server_ip: String::from("0.0.0.0"), server_port: 16700, delay_by_nano_seconds: 5000, aes_password_with_no_rsa: "".into() });
        access_keys.push(structs::configuration::ClientAccessKeys { access_key: "MyPersonalServer".into(), registration_access_key: "".into(), local_community_id: 3, server_ip: String::from("192.168.0.100"), server_port: 3500, delay_by_nano_seconds: 5000, aes_password_with_no_rsa: "".into() });

        let new_configuration = super::structs::configuration::Client {
            access_keys: access_keys,
            logging_enabled: true
        };

        let serialised_configuration = serde_json::to_string_pretty(&new_configuration).unwrap();
        if write_to_file(&path,&serialised_configuration.as_bytes().to_vec(), false) {
            unsafe{crate::ENABLE_LOGGING==new_configuration.logging_enabled};
            return new_configuration;
        } else {
            crate::display!("Could not create the client configuration file, exiting", path);
        }
    }
    std::process::exit(1);
}

/// Get the RSA private and public keys from file, if it doesn't exist the create one with default settings information
pub fn get_rsa_keys_server() -> super::structs::encryption::RSAKeyPairs {
    let result_path = local_file_exists(&crate::RSA_KEYS_FILE_NAME_SERVER);
    if result_path.is_ok() {
        let path = result_path.unwrap();
        let serialised_rsa_key_pairs = read_from_file_as_string(&path);
        if serialised_rsa_key_pairs.is_some() {
            let result_rsa_key_pairs = serde_json::from_str::<super::structs::encryption::RSAKeyPairs>(&serialised_rsa_key_pairs.unwrap());
            if result_rsa_key_pairs.is_ok() {
                return result_rsa_key_pairs.unwrap();
            } else {
                crate::display!("Error in the RSA server configuration file, exiting", path);
            }
        } else {
            crate::display!("Could not read the RSA server configuration file, exiting", path);
        }
    } else {
        let path = result_path.unwrap_err();
        crate::display!("Create RSA server file", path);
        let new_rsa_key_pairs = super::structs::encryption::RSAKeyPairs::new(true, &1);
        let serialised_rsa_key_pairs = serde_json::to_string_pretty(&new_rsa_key_pairs).unwrap();
        if write_to_file(&path, &serialised_rsa_key_pairs.as_bytes().to_vec(), false) {
            return new_rsa_key_pairs;
        } else {
            crate::display!("Could not create the RSA server configuration file, exiting", path);
        }
    }
    std::process::exit(1);
}

/// Get the RSA public keys from file
pub fn get_rsa_keys_client() -> super::structs::encryption::AllRSAKeyPairs {
    let result_path = local_file_exists(&crate::RSA_KEYS_FILE_NAME_CLIENT);
    if result_path.is_ok() {
        let path = result_path.unwrap();
        let serialised_rsa_key_pairs = read_from_file_as_string(&path);
        if serialised_rsa_key_pairs.is_some() {
            let result_rsa_key_pairs = serde_json::from_str::<super::structs::encryption::AllRSAKeyPairs>(&serialised_rsa_key_pairs.unwrap());
            if result_rsa_key_pairs.is_ok() {

                return result_rsa_key_pairs.unwrap();
            } else {
                crate::display!("Error in the RSA client configuration file, exiting", path);
            }
        } else {
            crate::display!("Could not read the RSA client configuration file, exiting", path);
        }
    } else {
        let path = result_path.unwrap_err();
        crate::display!("Create RSA clients file", path);

        let new_rsa_key_pairs = super::structs::encryption::AllRSAKeyPairs::new();
        let serialised_rsa_key_pairs = serde_json::to_string_pretty(&new_rsa_key_pairs).unwrap();
        if write_to_file(&path,&serialised_rsa_key_pairs.as_bytes().to_vec(), false) {
            return new_rsa_key_pairs;
        } else {
            crate::display!("Could not create the RSA client configuration file, exiting", path);
        }
    }
    std::process::exit(1);
}
/// Get the private client RSA public keys from file (Clients own RSA pair and other clients' public keys for private encryptions)
pub fn get_rsa_keys_client_private() -> super::structs::encryption::PrivateCommunicationRSAKeyPairs {
    let result_path = local_file_exists(&crate::RSA_KEYS_FILE_NAME_CLIENT_PRIVATE);
    if result_path.is_ok() {
        let path = result_path.unwrap();
        let serialised_rsa_key_pairs = read_from_file_as_string(&path);
        if serialised_rsa_key_pairs.is_some() {
            let result_rsa_key_pairs = serde_json::from_str::<super::structs::encryption::PrivateCommunicationRSAKeyPairs>(&serialised_rsa_key_pairs.unwrap());
            if result_rsa_key_pairs.is_ok() {

                return result_rsa_key_pairs.unwrap();
            } else {
                crate::display!("Error in the RSA clients' private RSA keys file, exiting", path);
            }
        } else {
            crate::display!("Could not read the clients' private RSA keys file, exiting", path);
        }
    } else {
        let path = result_path.unwrap_err();
        println!();
        crate::display!("Create clients' private RSA keys file", path);
        println!();

        let new_private_rsa_key_pairs = super::structs::encryption::PrivateCommunicationRSAKeyPairs::new();
        let serialised_private_rsa_key_pairs = serde_json::to_string_pretty(&new_private_rsa_key_pairs).unwrap();
        if write_to_file(&path,&serialised_private_rsa_key_pairs.as_bytes().to_vec(), false) {
            return new_private_rsa_key_pairs;
        } else {
            crate::display!("Could not create the RSA client configuration file, exiting", path);
        }
    }
    std::process::exit(1);
}

/// Get the predicted compression and encryption times from file, if it does not exist the create it and execute an initial benchmark
pub fn get_benchmarks() -> std::sync::Mutex<super::structs::Benchmarks> {
    let result_path = local_file_exists(&crate::BENCHMARK_FILE_NAME);
    if result_path.is_ok() {
        let path = result_path.unwrap();
        let serialised_benchmarks = read_from_file_as_string(&path);
        if serialised_benchmarks.is_some() {
            let result_benchmarks = serde_json::from_str::<std::sync::Mutex<super::structs::Benchmarks>>(&serialised_benchmarks.unwrap());
            if result_benchmarks.is_ok() {
                return result_benchmarks.unwrap();
            } else {
                log!("Error in the benchmarks file, exiting", path);
            }
        } else {
            log!("Could not read the benchmarks file, exiting", path);
        }
    } else {
        let path = result_path.unwrap_err();
        crate::trace!("Create benchmarks file", path);
        let new_benchmarks = std::sync::Mutex::new(super::structs::Benchmarks::new());
        let serialised_benchmarks = serde_json::to_string_pretty(&new_benchmarks).unwrap();
        if write_to_file(&path,&serialised_benchmarks.as_bytes().to_vec(), false) {
            return new_benchmarks;
        } else {
            log!("Could not create the benchmarks file, exiting", path);
        }
    }
    std::process::exit(1);
}
