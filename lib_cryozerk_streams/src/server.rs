/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Server specific functionaity.
//!
//! A server application wull call these library functions and they will communicate events back to it via callback as specified in [callbacks](structs::callbacks)

use tokio::net::{TcpListener};
use std::net::{IpAddr};
use ansi_term::Colour;
use super::enums;
use super::files;
use super::log;
use super::streams;
use super::structs;
use super::general;
use chrono::{DateTime, Local};

/// Starts a new server instance and listens for and processes new incoming connections
pub async fn start_new_server<'a>(
        message_handlers: &'static structs::callbacks::MessageHandlers,
        banning_handlers: &'static structs::callbacks::BanningHandlers,
        delay_by: Option<std::time::Duration>) -> Result<String, String> {

    // Delete all timed-out uploads older than their respective time-out periods
    let mut access_key_timeouts: Vec<(Vec<u8>, i64, DateTime<Local>)> = vec![];
    let _timedout_uploads_thread = std::thread::spawn(move || {
        for server_access in crate::CONFIGURATION_SERVER.access_keys.clone() {
            let access_key = general::append_vector_to_length(server_access.access_key.as_bytes().to_vec(),crate::MAXIMUM_LENGTH_ACCESS_KEY);
            access_key_timeouts.push((access_key, server_access.automatic_deletion_after_seconds as i64, general::get_current_datestamp()));
        }
        loop {
            std::thread::sleep(std::time::Duration::new(30, 0));
            for time_out in access_key_timeouts.iter_mut() {
                if time_out.1 > 0 && general::get_current_datestamp().signed_duration_since(time_out.2) > chrono::Duration::seconds(time_out.1) {
                    (message_handlers.on_timedout_delete)(time_out.0.to_vec(), &time_out.1);
                    time_out.2 = general::get_current_datestamp();
                }
            }
        }
    });

    let result_listener = TcpListener::bind(&format!("{}:{}", crate::CONFIGURATION_SERVER.listening_ip, crate::CONFIGURATION_SERVER.listening_port )).await;
    if result_listener.is_ok() {
        let _ = &crate::RSA_KEY_PAIRS_SERVER.rsa_4096_key_pair;
        let _ = &crate::RSA_KEY_PAIRS_SERVER.rsa_2048_key_pair;
        crate::display!("CryoZerk server ", crate::VERSION_INFO);
        crate::display!(&format!("Ready: {}:{}", crate::CONFIGURATION_SERVER.listening_ip, crate::CONFIGURATION_SERVER.listening_port));
        if unsafe{crate::DARK_MODE == false} {
            let enabled = "Enabled";
            let disabled = "Disabled";
            if cfg!(unix) {
                println!("Logging to log file: {}", unsafe{match crate::ENABLE_LOGGING {true=>Colour::Green.paint(enabled), false => Colour::Yellow.paint(disabled)}});
            } else {
                println!("Logging to log file: {}", unsafe{match crate::ENABLE_LOGGING {true=>enabled, false => disabled}});
            }
            if cfg!(unix) {
                println!("Dark mode: {}", disabled);
            } else {
                println!("Dark mode: {}", disabled);
            }
        } else {
            let message = "No logging of any kind";
            if cfg!(unix) {
                println!("Dark mode active: {}", Colour::Red.paint(message));
            } else {
                println!("Dark mode active: {}", message);
            }
        }
        let listener = result_listener.unwrap();
        loop {
            let (tcp_stream, address) = listener.accept().await.unwrap();
            let _ = std::thread::spawn(move || {
                let _ = process_a_connection(&message_handlers, &banning_handlers, address.ip(), &mut structs::ConnectionParams::new(delay_by, tcp_stream));
            });
        }
    } else {
        let error_message = format!("Could not start server: {}:{}", crate::CONFIGURATION_SERVER.listening_ip, crate::CONFIGURATION_SERVER.listening_port);
        crate::display!(&error_message);
        crate::display!(&result_listener.unwrap_err().to_string());
        return Err(error_message);
    }
}

/// Process an incoming connection
#[tokio::main]
async fn process_a_connection<'a>(
        message_handlers: &'static structs::callbacks::MessageHandlers,
        banning_handlers: &'static structs::callbacks::BanningHandlers,
        ip_address: IpAddr,
        connection_params: &mut structs::ConnectionParams) {

    if (banning_handlers.on_check_if_banned)(&ip_address, &None) { 
        return;
    }

    crate::log!("New", ip_address.to_string());
    let mut progress_bar = structs::ProgressBar::new(&crate::LABEL_RECEIVING.to_owned(), 0_64);
    let mut receive_handler = structs::BytesReceiving::new(&mut progress_bar, true);
    let dummy_handshake = structs::InitialHandshake::new_null_session();
    let result_data_packet_received = streams::receive_bytes_from_stream(&structs::BytesControl::new(0, crate::MAXIMUM_LENGTH_HANDSHAKE_PACKET as usize), &enums::Application::Server, &enums::PacketType::InitialHandshake, &mut receive_handler, &dummy_handshake, connection_params).await;
    if result_data_packet_received.is_ok() {
        let bytes = result_data_packet_received.unwrap();
        // We expect an InitialHandshake request from client
        let result_new_handshake = structs::InitialHandshake::from_bytes(&bytes);
        if result_new_handshake.is_ok() {
            let mut new_handshake = result_new_handshake.unwrap();
            
            // Check if already is banned
            if (banning_handlers.on_check_if_banned)(&ip_address, &Some(new_handshake.access_key.to_owned())) { println!("Banned!"); return; }
            // Check if Ip white list is ok
            if check_access_against_access_key_and_white_list(&ip_address, &new_handshake.access_key, &crate::CONFIGURATION_SERVER.access_keys)==false { return; }

            let option_result = (message_handlers.on_new_connection)(&new_handshake);
            if option_result.is_ok() {
                let result = option_result.unwrap();
                let total_allowed_upload_for_access_key = result.0.max_total_of_all_uploads_bytes as usize;
                let total_bytes_already_uploaded = result.1;
                crate::log!(&format!("Current bytes for access_key {:?}, total_bytes_already_uploaded: {}, total_allowed_upload_for_access_key: {}", result.0.access_key, general::format_count_to_string(total_bytes_already_uploaded), general::format_count_to_string(total_allowed_upload_for_access_key)));
                let bytes_control = structs::BytesControl::new(total_bytes_already_uploaded, total_allowed_upload_for_access_key);
                // Send back a success Message packet
                new_handshake.session_id = general::append_vector_to_length(general::get_random_alphanumeric_characters(crate::MAXIMUM_LENGTH_SESSION_KEY as u16).as_bytes().to_vec(), crate::MAXIMUM_LENGTH_SESSION_KEY as usize);
                let result_send = streams::send_data_packet_to_stream(&enums::PacketType::Message, &new_handshake.session_id, &new_handshake.clone(), connection_params).await;
                if result_send.is_ok() {
                    let mut received_handler = structs::BytesReceiving::new(&mut progress_bar, true);
                    // We expect the client to send two messages, the first is a session_id verification step
                    let mut result_data_packet = streams::receive_data_packet_from_stream(&bytes_control,&enums::Application::Server, &enums::PacketType::Message, &mut received_handler, &new_handshake, connection_params).await;
                    if result_data_packet.is_ok() {
                        let packet_data = result_data_packet.unwrap().data;
                        let session_id_verification_result = structs::packet_data_types::Message::from_bytes(packet_data);
                        if session_id_verification_result.is_ok() {
                            let session_id_verification = session_id_verification_result.unwrap();
                            if session_id_verification.bytes != new_handshake.session_id {
                                
                                crate::display!("Invalid session_id verification", "Trace 3");
                                return;
                            } else {
                                let registration_access_key = general::strip_zeros(&new_handshake.registration_access_key);
                                if !registration_key_is_ok(&new_handshake.access_key, &registration_access_key) {
                                    crate::display!("The client was not registered", general::vector_stripped_to_string(&new_handshake.registration_access_key));
                                    return;
                                }
                            }
                        } else {
                            crate::display!("Invalid session_id verification", "Trace 2");
                            return;
                        }
                    } else {
                        crate::display!("Invalid session_id verification", result_data_packet.unwrap_err());
                        return;
                    }
                    //Send back an Ok
                    let _ = streams::send_ok("Session established", &mut new_handshake, connection_params).await;

                    let mut progress_bar = structs::ProgressBar::new(&crate::LABEL_RECEIVING.to_owned(), 0_64);
                    let mut received_handler = structs::BytesReceiving::new(&mut progress_bar, true);
                    result_data_packet = streams::receive_data_packet_from_stream(&bytes_control, &enums::Application::Server, &enums::PacketType::Message, &mut received_handler, &new_handshake, connection_params).await;
                    if result_data_packet.is_ok() {
                        let data_packet = result_data_packet.unwrap();
                        match data_packet.packet_type {
                            enums::PacketType::File => {
                                extensions::new_file_arrived(data_packet, &message_handlers, &new_handshake, connection_params).await;
                            },
                            enums::PacketType::Update => {
                                extensions::update_file(data_packet, &message_handlers, &new_handshake, connection_params).await;
                            },
                            enums::PacketType::Retrieve => {
                                extensions::retrieve_file(data_packet, &message_handlers, &new_handshake, connection_params).await;
                            },
                            enums::PacketType::ListUploads => {
                                let result_list_request = structs::packet_data_types::ListUploads::from_bytes(data_packet.data);
                                if result_list_request.is_ok() {
                                    let list_request = result_list_request.unwrap();
                                    extensions::list_uploads(&list_request.layer, &message_handlers, &new_handshake, connection_params).await;
                                } else {
                                    log!("Error deserialising ListUploads data packet", result_list_request.unwrap_err());
                                }
                            },
                            enums::PacketType::PacketDetails => {
                                let response_packet_details = structs::packet_data_types::Message::from_bytes(data_packet.data.to_owned());
                                let result_package_details = (message_handlers.on_packet_details)(new_handshake.access_key.to_owned(), response_packet_details.unwrap().bytes);
                                if result_package_details.is_ok(){
                                    let packet_details = result_package_details.unwrap();
                                    let _ = streams::send_data_packet_to_stream(&enums::PacketType::PacketDetails, &packet_details.into_bytes().unwrap(), &new_handshake.clone(), connection_params).await;
                                } else {
                                    log!("Error retrieving data packet details", result_package_details.unwrap_err());
                                }
                            },
                            enums::PacketType::Drop => {
                                extensions::delete_upload(data_packet, &message_handlers, &new_handshake.clone(), connection_params).await;
                            },
                            enums::PacketType::RsaPublicKeyGeneric=> {
                                crate::display!("RSA Registration");
                                send_back_rsa_public_key(&data_packet, &new_handshake.clone(), connection_params).await;
                            },
                            enums::PacketType::Message=> {
                                extensions::message_arrived(data_packet, &message_handlers, &new_handshake.clone(), connection_params).await;
                            }
                            _ => log!("Unknown data packet type received", format!("{:?}", data_packet.packet_type))
                        }
                    } else {
                        log!("Could not receive data from stream", result_data_packet.unwrap_err());
                    }
                } else {
                    log!("An error occured when sending an InitialHandshake request response", result_send.unwrap_err());
                }
            } else {
                (banning_handlers.on_update_ban_details)(&ip_address, &Some(new_handshake.access_key));
                let message = option_result.unwrap_err();
                log!("Handshake not allowed", &message);
            }
        } else {
            (banning_handlers.on_update_ban_details)(&ip_address, &None);
            log!("An error occured when deserialsing a InitialHandshake", result_new_handshake.unwrap_err());
        }
    } else {        
        //(banning_handlers.on_update_ban_details)(&ip_address, &None);
        log!("An error occured when receiving a data packet", result_data_packet_received.unwrap_err());
    }
}

/// Lets make sure the user has been FULLY registered (if required) before allowing access to the system
pub fn registration_key_is_ok(access_key_vec: &Vec<u8>, registration_access_key_vec: &Vec<u8>) -> bool {
    let access_key = general::vector_stripped_to_string(access_key_vec);
    let option_access_key = crate::CONFIGURATION_SERVER.access_keys.iter().find(|a|a.access_key==access_key);
    if option_access_key.is_some() {
        let access_key = option_access_key.unwrap();
        if access_key.allowed_registration_access_keys.len()>0 {
            let registration_access_key = general::vector_stripped_to_string(registration_access_key_vec);
            
            return access_key.allowed_registration_access_keys.iter().find(|r|r.id==registration_access_key).is_some();
        } else {

            return true;
        }
    }
    
    false
}

/// Check the current connected clients' ip address for the access_key it is using to see if a white list of ip addresses exist which contains this address.
/// If the white list is empty allow access, if the ip address is not in the list deny access.
///
/// Regular expressions are supported
pub fn check_access_against_access_key_and_white_list(ip_address: &IpAddr, access_key: &Vec<u8>, all_server_access: &Vec<structs::configuration::ServerAccess>) -> bool {
    let access_key_string = general::vector_stripped_to_string(&access_key);
    let option_found_server_access = all_server_access.iter().find(|a| a.access_key == access_key_string);
    if option_found_server_access.is_none() {
        crate::display!("Invalid access key: {}", super::general::vector_stripped_to_string(access_key));
        // Invalid access key
        return false;
    } else {
        let found_server_access = option_found_server_access.unwrap();
        if found_server_access.ip_address_white_list.len()==0 {
            // No white list so allow all Ip Addresses
            return true;
        } else {
            let option_found_ip_address=found_server_access.ip_address_white_list.iter().find(|ip|regex::Regex::new(&ip.ip_address).unwrap().is_match(&ip_address.to_string()));
            if option_found_ip_address.is_none() {
                crate::display!("Ip address blocked by white listing: {}", ip_address);
                return false
            } else {
                return true
            }
        }
    }
}

/// Send a Message to the client
pub async fn send_error_message(error_message: &str, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
    let _ = streams::send_data_packet_to_stream(&enums::PacketType::Message, &error_message.as_bytes().to_vec(), &initial_handshake, connection_params).await;
}

/// Write a File type data packet to disk
pub fn write_file(file: &structs::packet_data_types::File) {
    let removed_any_directory_info = files::get_only_file_name(&general::vector_stripped_to_string(&file.file_name));
    if files::write_to_file(&removed_any_directory_info, &file.bytes, false) {
        crate::display!("Wrote file to disk", removed_any_directory_info);
        let hash = crate::encryption::generate_sha_512_hash(&file.bytes);
        if crate::general::strip_zeros(&file.hash).len() != 0 && file.hash != hash {
            crate::display!("Warning: file hash does not match. This file was altered from the original!");
            crate::display!(&format!("Original: {:?}, downloaded file: {:?}", file.hash, hash));
        }
        if file.description!="" {
            crate::display!("Description", file.description)
        }
    } else {
        crate::display!("Could not write file to disk", removed_any_directory_info);
    }
}

/// Send the requested RSA public key back to the client
async fn send_back_rsa_public_key(data_packet: &structs::DataPacket, initial_handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
    if crate::CONFIGURATION_SERVER.allow_new_registrations {
        let mut rsa_public_key = structs::packet_data_types::RsaPublicKey::from_bytes(&data_packet.data);
        rsa_public_key.bytes = match rsa_public_key.rsa_key_length {
            enums::RsaKeyLength::RSA2048 => {
                crate::RSA_KEY_PAIRS_SERVER.rsa_2048_key_pair.public_key.to_vec()
            }
            enums::RsaKeyLength::RSA4096 => {
                crate::RSA_KEY_PAIRS_SERVER.rsa_4096_key_pair.public_key.to_vec()
            }
            _ => {
                crate::trace!("Unknown public key requested", format!("{:#?}", rsa_public_key));
                vec!()
            }
        };

        let result_write = streams::send_data_packet_to_stream(&enums::PacketType::RsaPublicKeyGeneric, &rsa_public_key.bytes, &initial_handshake.clone(), connection_params).await;
        if result_write.is_ok() {
            crate::trace!(&format!("Sent {:?} public key to client", rsa_public_key.rsa_key_length));
        } else {
            log!(&format!("Error sending {:?} public key to client", rsa_public_key.rsa_key_length), result_write.unwrap_err());
        }
    } else {
        let _ = streams::send_err("Registration not allowed" , &initial_handshake, connection_params).await;
    }
}

mod extensions {
    use crate::log;
    use super::structs;
    use super::enums;
    use super::streams;

    /// A new file is being uploaded from the client
    pub async fn new_file_arrived(data_packet: structs::DataPacket, message_handlers: &structs::callbacks::MessageHandlers, handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
        let result_response: Result<structs::packet_data_types::UploadResponse, String> = (message_handlers.on_arrived)(data_packet, handshake.to_owned());
        let serialised_response = match result_response.is_ok() {
            true => result_response.unwrap().into_bytes(),
            false => {
                log!("Upload file error", result_response.unwrap_err());
                structs::packet_data_types::UploadResponse::new(enums::Status::Error, &vec![], &"A server error occured".as_bytes().to_vec(), true).into_bytes()
            }
        };
        let _ = streams::send_data_packet_to_stream(&enums::PacketType::UploadResponse, &serialised_response.unwrap(), &handshake.clone(), connection_params).await;
    }

    /// Update the file byte data on an already exiting upload
    pub async fn update_file(data_packet: structs::DataPacket, message_handlers: &structs::callbacks::MessageHandlers, handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
        let result_response: Result<structs::packet_data_types::UploadResponse, String> = (message_handlers.on_update)(data_packet, handshake.to_owned());
        let serialised_response = match result_response.is_ok() {
            true => result_response.unwrap().into_bytes(),
            false => {
                let error_message = result_response.unwrap_err();
                log!("Upload file error for updating existing file data", error_message);
                structs::packet_data_types::UploadResponse::new(enums::Status::Error, &vec![], &error_message.as_bytes().to_vec(), true).into_bytes()
            }
        };
        let _ = streams::send_data_packet_to_stream(&enums::PacketType::UploadResponse, &serialised_response.unwrap(), &handshake.clone(), connection_params).await;
    }

    /// Retrieve and return an upload back to a client
    pub async fn retrieve_file(data_packet: structs::DataPacket, message_handlers: &structs::callbacks::MessageHandlers, handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
        let result_response_retrieve = structs::packet_data_types::Message::from_bytes(data_packet.data);
        if result_response_retrieve.is_ok() {
            let response_retrieve = result_response_retrieve.unwrap();
            let verification_key = response_retrieve.bytes[0..crate::MAXIMUM_LENGTH_VERIFICATION_KEY].to_vec();
            let protected_key = response_retrieve.bytes[crate::MAXIMUM_LENGTH_VERIFICATION_KEY..crate::MAXIMUM_LENGTH_VERIFICATION_KEY*2].to_vec();
            let unique_code = response_retrieve.bytes[crate::MAXIMUM_LENGTH_VERIFICATION_KEY*2..response_retrieve.bytes.len()].to_vec();

            let option_response = (message_handlers.on_retrieve)(handshake.packet_symmetric_encryption_key.to_owned(), handshake.access_key.to_owned(), unique_code, verification_key, protected_key);
            if option_response.is_ok(){
                let (_, bytes) = option_response.unwrap();
                let _ = streams::send_data_packet_to_stream(&enums::PacketType::File, &bytes, &handshake.clone(), connection_params).await;
            } else {
                log!("Error retrieving data", option_response.unwrap_err());
            }
        } else {
            log!("Error reading the data packet", result_response_retrieve.unwrap_err());
        }
    }

    /// Return a list of information on all uploads for a clients access_key
    pub async fn list_uploads(layer: &Vec<u8>, message_handlers: &structs::callbacks::MessageHandlers, handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
        let result_request = (message_handlers.on_list_uploads)(&handshake.access_key, &layer);
        if result_request.is_ok(){
            let list_uploads = result_request.unwrap();
            let _ = streams::send_data_packet_to_stream(&enums::PacketType::ListUploads, &list_uploads.into_bytes().unwrap(), &handshake.clone(), connection_params).await;
        } else {
            log!("Error retrieving list data", result_request.unwrap_err());
        }
    }

    /// Delete a previous upload
    pub async fn delete_upload(data_packet: structs::DataPacket, message_handlers: &structs::callbacks::MessageHandlers, handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
        let result_message = structs::packet_data_types::Message::from_bytes(data_packet.data);
        if result_message.is_ok() {
            let message = result_message.unwrap();
            let verification_key = message.bytes[0..crate::MAXIMUM_LENGTH_VERIFICATION_KEY].to_vec();
            let protected_key = message.bytes[crate::MAXIMUM_LENGTH_VERIFICATION_KEY..crate::MAXIMUM_LENGTH_VERIFICATION_KEY*2].to_vec();
            let unique_code = message.bytes[crate::MAXIMUM_LENGTH_VERIFICATION_KEY*2..message.bytes.len()].to_vec();
            let option_response = (message_handlers.on_delete)(handshake.access_key.to_vec(), unique_code.to_owned(), verification_key.to_owned(), protected_key.to_owned());
            if option_response.is_none() {
                let _ = streams::send_data_packet_to_stream(&enums::PacketType::Message, &"Success".as_bytes().to_vec(), &handshake.clone(), connection_params).await;
            } else {
                let response = option_response.unwrap();
                let _ = streams::send_data_packet_to_stream(&enums::PacketType::Message, &response.as_bytes().to_vec(), &handshake.clone(), connection_params).await;
            }
        } else {
            log!("Error reading data packet", result_message.unwrap_err())
        }
    }

    /// A generic message has arrived
    pub async fn message_arrived(data_packet: structs::DataPacket, message_handlers: &structs::callbacks::MessageHandlers, handshake: &structs::InitialHandshake, connection_params: &mut structs::ConnectionParams) {
        let result_message = structs::packet_data_types::Message::from_bytes(data_packet.data);
        if result_message.is_ok() {
            let message = result_message.unwrap();
            if message.packet_type == enums::PacketType::Upgrade {
                let result = (message_handlers.on_message)(&handshake.access_key, &message);
                if result.is_ok() {
                    let file_tuple = result.unwrap();
                    let _ = streams::send_data_packet_to_stream(&enums::PacketType::File, &file_tuple.1, &handshake.clone(), connection_params).await;
                } else {
                    let _ = super::send_error_message(&result.unwrap_err(), &handshake, connection_params).await;
                }
            } else {
                let _ = super::send_error_message(&"An unhandled packet type was sent to the server", &handshake, connection_params).await;
            }
        } else {
            let _ = super::send_error_message(&"There was an error in the bytes sent to the server", &handshake, connection_params).await;
        }
    }
}
