/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! A command line arguments processor.

use std::env;
use super::cz;

/// Represents a specific commmand-line argument and its value/
///
/// Only used in this declarting module
#[derive(Clone, Debug)]
pub struct Argument {
    pub name: String,
    pub value: String
}

impl Argument {
    pub fn new(name: &str, value: &str) -> Result<Argument, String> {
        let validation_regex: String = match name {
            "-p" => format!("^[ -~]{{{},{}}}$", cz::MINIMUM_LENGTH_OF_USER_PASSWORD, cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY),
            "-c" => format!("^[0-9a-zA-Z]{{6,{}}}$", cz::MAXIMUM_LENGTH_OF_UNIQUE_CODE),
            "-f" => format!("^[ -~]{{1,{}}}$", cz::MAXIMUM_LENGTH_FILE_NAME),
            "-l" => format!("^[0-9a-zA-Z/]{{0,{}}}$", cz::MAXIMUM_LENGTH_LAYER),
            "--community" => format!("^[0-9a-zA-Z]{{1,{}}}$", cz::MAXIMUM_LENGTH_ACCESS_KEY).into(),
            "--px" => format!("^[ -~]{{{},{}}}$", cz::MINIMUM_LENGTH_OF_USER_PASSWORD, cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY),
            "--pkname" => format!("^[0-9a-zA-Z]{{1,{}}}$", cz::MAXIMUM_LENGTH_P2P_KEY_NAME),
            "--regkey" => format!("^[0-9a-zA-Z]{{{},{}}}$", cz::MINIMUM_LENGTH_REGISTRATION_KEY, cz::MAXIMUM_LENGTH_REGISTRATION_KEY),
            _ => "".into()
        };

        let regex_validator = regex::Regex::new(&validation_regex);

        if regex_validator.unwrap().is_match(value) || validation_regex.len()==0 {
            Ok(Argument {
                name: name.into(),
                value: value.into()
            }) 
        } else {
            Err(format!("Validation failed for argument '{}', allowed regex is {}", name, validation_regex))
        }
    }
}

pub struct Command {
    required_arguments: Vec<String>,
    additional_arguments: Vec<String>
}
impl Command {
    pub fn new(command: crate::enums::MessageType) -> Command {
        Command {
            required_arguments: match command {
                crate::enums::MessageType::Delete => { vec!["-p".into(), "-c".into()] },
                crate::enums::MessageType::Get => { vec!["-p".into(), "-c".into()] },
                crate::enums::MessageType::Keys => { vec![] },
                crate::enums::MessageType::List => { vec![] },
                crate::enums::MessageType::RSARegistration => { vec![] },
                crate::enums::MessageType::Send => { vec!["-p".into(), "-f".into()] },
                crate::enums::MessageType::GetPublicKey => { vec!["-p".into(), "-c".into(), "--pkname".into()] },
                crate::enums::MessageType::SendPublicKey => { vec!["-p".into()] },
                crate::enums::MessageType::DeleteLocalPublicKey => { vec!["--pkname".into()] },
                crate::enums::MessageType::ListLocalPublicKeys => { vec![] },
                crate::enums::MessageType::Update => { vec!["-p".into(), "-c".into(), "-f".into()] },
                crate::enums::MessageType::Upgrade => vec![],
                crate::enums::MessageType::Install =>  vec![]
            },
            additional_arguments: match command {
                crate::enums::MessageType::Delete => { vec!["-v".into(), "--community".into(), "--px".into()] },
                crate::enums::MessageType::Get => { vec!["-v".into(), "--community".into(), "--px".into(), "--display".into()] },
                crate::enums::MessageType::Keys => { vec!["-v".into()] },
                crate::enums::MessageType::List => { vec!["-v".into(), "--community".into()] },
                crate::enums::MessageType::RSARegistration => { vec!["-v".into(), "--community".into(), "--regkey".into()] },
                crate::enums::MessageType::Send => { vec!["-v".into(), "-c".into(), "--community".into(), "-d".into(), "-l".into(), "-x".into(), "--px".into(), "--pkname".into(), "--nohash".into()] },
                crate::enums::MessageType::GetPublicKey => { vec!["-v".into(), "--community".into()] },
                crate::enums::MessageType::SendPublicKey => { vec!["-v".into(), "--community".into(), "-d".into(), "-l".into(), "-x".into(), "--px".into()] },
                crate::enums::MessageType::DeleteLocalPublicKey => { vec!["-v".into()] },
                crate::enums::MessageType::ListLocalPublicKeys => { vec!["-v".into()] },
                crate::enums::MessageType::Update => { vec!["-v".into(), "--community".into(), "--nohash".into()] },
                crate::enums::MessageType::Upgrade => { vec!["-v".into()] },
                crate::enums::MessageType::Install =>  { vec!["-v".into()] }
            }
        }
    }

    pub fn printed_arguments (command: crate::enums::MessageType) -> String {
        let command_args = Command::new(command);
        let mut required: String = "".into();
        for arg in command_args.required_arguments.iter() {
            if required.len() > 0 {
                required = format!("{}, {}", required, arg);
            } else {
                required = arg.to_owned();
            }
        }

        let mut allowed: String = "".into();
        for arg in command_args.additional_arguments.iter() {
            if allowed.len() > 0 {
                allowed = format!("{}, {}", allowed, arg);
            } else {
                allowed = arg.to_owned();
            }
        }

        format!("({}{})"
        , required
        , match allowed.len() > 0 { 
            true => match required.len()>0 {
                true => format!(" [{}]", allowed),
                _ => format!("[{}]", allowed)
            }, 
            _ => "".into()
        })
    }
}

/// Get arguments from command-line
pub fn get_arguments() -> Result<(crate::enums::MessageType, Vec<Argument>), ()> {            
    let args_array: Vec<String> = env::args().collect::<Vec<String>>();
    let arguments: Result<(crate::enums::MessageType, Vec<Argument>), ()> = get_settings(args_array);

    arguments
}

fn check_arguments(args_array: &Vec<String>) -> bool {
    let mut next_must_be_argument = true;
    for i in 2..args_array.len() {
        if next_must_be_argument && !args_array[i].starts_with("-") {
            println!("Invalid arguments given (-h for help).");
            return false;
        } else {
            if args_array[i].starts_with("-") {
                next_must_be_argument = false;
            } else {
                next_must_be_argument = !next_must_be_argument;
            }
        }
    }

    true
}

/// If an argument exists on the command line then add it and its value to Argument Vec to return
fn get_settings(args_array: Vec<String>) -> Result<(crate::enums::MessageType, Vec<Argument>), ()> {    
    let mut arguments_to_return: Vec<Argument> = vec!();

    if args_array.len() == 1 {    
        println!("{}", get_help_screen_text());
        return Err(())
    }

    let mut index_p_exists: bool = false;
    let mut index_f_exists: bool = false;
    let mut index_c_exists: bool = false;
    let index_l_exists: bool = false;
    let mut index_x_exists: bool = false;
    let mut index_display_exists: bool = false;
    let mut index_registration_key_exists: bool = false;

    let command: crate::enums::MessageType;

    if get_argument("send", &args_array).is_some() {
        command = crate::enums::MessageType::Send;
    } else if get_argument("update", &args_array).is_some() {
        command = crate::enums::MessageType::Update;
    } else if get_argument("get", &args_array).is_some() {
        command = crate::enums::MessageType::Get;
    } else if get_argument("register", &args_array).is_some() {
        command = crate::enums::MessageType::RSARegistration;
    } else if get_argument("list", &args_array).is_some() {
        command = crate::enums::MessageType::List;
    } else if get_argument("delete", &args_array).is_some() {
        command = crate::enums::MessageType::Delete;
    } else if get_argument("upgrade", &args_array).is_some() {
        command = crate::enums::MessageType::Upgrade;
    } else if get_argument("keys", &args_array).is_some() {
        command = crate::enums::MessageType::Keys;
    } else if get_argument("spk", &args_array).is_some() {
        command = crate::enums::MessageType::SendPublicKey;
    } else if get_argument("gpk", &args_array).is_some() {
        command = crate::enums::MessageType::GetPublicKey;
    } else if get_argument("dpk", &args_array).is_some() {
        command = crate::enums::MessageType::DeleteLocalPublicKey;
    } else if get_argument("lpk", &args_array).is_some() {
        command = crate::enums::MessageType::ListLocalPublicKeys;
    } else if get_argument("install", &args_array).is_some() {
        command = crate::enums::MessageType::Install;
    } else {
        println!("No command was given to indicate what action you wish to take (-h for help).");
        return Err(())
    }

    if !check_arguments(&args_array) { return Err(()) }

    let index_h = get_argument("-h", &args_array); // Help screen
    let index_h_help = get_argument("--help", &args_array); // Help screen
    let index_s = get_argument("-s", &args_array); // Smile
    let index_p = get_argument("-p", &args_array); // Password for additional AES256 encryption of message data
    let index_f = get_argument("-f", &args_array); // A name of a file you wish to send
    let index_c = get_argument("-c", &args_array); // The code of the file to download
    let index_v = get_argument("-v", &args_array); // Run in more verbose mode
    let index_l = get_argument("-l", &args_array); // Add a description to a 'send'
    let index_d = get_argument("-d", &args_array); // The hierarchical layer a files resides in
    let index_x = get_argument("-x", &args_array); // Do not delete file when downloaded
    let index_k = get_argument("--community", &args_array); // Specify the id of an access_key to use
    let index_protected = get_argument("--px", &args_array); // A second password to prevent a file from being deleted with the same password as was used to upload/treive it
    let index_display = get_argument("--display", &args_array); // Display the contents of a file instead of saving a download to disk
    let index_p2p_name = get_argument("--pkname", &args_array); // The user-friendly name of a public key
    let index_registration_key = get_argument("--regkey", &args_array); // The key needed to register with a community

    if index_h.is_some() || index_h_help.is_some() {
        println!("{}", get_help_screen_text());
        return Err(());
    } else {
        if index_s.is_some() {
            println!("{}", get_smile_screen_text());
        };
        if index_p.is_some() {
            index_p_exists = true;
            arguments_to_return.push(index_p.unwrap());
        };
        if index_f.is_some() {
            index_f_exists = true;
            arguments_to_return.push(index_f.unwrap());
        };
        if index_c.is_some() {
            index_c_exists = true;
            arguments_to_return.push(index_c.unwrap());
        };
        if index_v.is_some() {
            arguments_to_return.push(index_v.unwrap());
        };
        if index_l.is_some() {
            index_p_exists = true;
            arguments_to_return.push(index_l.unwrap());
        };
        if index_k.is_some() {
            arguments_to_return.push(index_k.unwrap());
        };
        if index_d.is_some() {
            arguments_to_return.push(index_d.unwrap());
        };
        if index_x.is_some() {
            index_x_exists = true;
            arguments_to_return.push(index_x.unwrap());
        };
        if index_display.is_some() {
            index_display_exists = true;
            arguments_to_return.push(index_display.unwrap());
        };
        if index_protected.is_some() {
            arguments_to_return.push(index_protected.unwrap());
        };
        if index_p2p_name.is_some() {
            arguments_to_return.push(index_p2p_name.unwrap());
        };
        if index_registration_key.is_some() {
            index_registration_key_exists = true;
            arguments_to_return.push(index_registration_key.unwrap());
        }
    }

    if index_h.is_some() {
        println!("{}", get_help_screen_text());
        return Err(());
    } else {
        if index_s.is_some() {
            println!("{}", get_smile_screen_text());
        };
        let temp_command = &command;
        if temp_command.clone() as u8 == crate::enums::MessageType::Send as u8 && !index_f_exists {
            println!("A file name was not supplied with argument '-f' (-h for help)");
            return Err(());
        } else if temp_command.clone() as u8 == crate::enums::MessageType::Get as u8 && !index_c_exists {
            println!("A download code was not supplied with argument '-c' (-h for help)");
            return Err(());
        }

        if index_x_exists && command != crate::enums::MessageType::Send {
            println!("Argument '-x' may only be supplied when doing a 'send' (-h for help)");
            return Err(());
        }
        if index_l_exists && command != crate::enums::MessageType::Send {
            println!("Argument '-l' may only be supplied when doing a 'send' (-h for help)");
            return Err(());
        }
        if index_l_exists {
            let argument_value = get_argument_value("-l", &arguments_to_return);
            if argument_value.is_some() {
                let value = argument_value.unwrap();
                if value.len() == 0 && command==crate::enums::MessageType::Send {
                    println!("The layer specified using argument '-l' was not supplied (-h for help)");
                    return Err(());
                } else if value.len() > cz::MAXIMUM_LENGTH_LAYER as usize {
                    println!("The layer, using '-l', was supplied but it was longer than the maximum allowed length of {} (-h for help)", cz::MAXIMUM_LENGTH_LAYER);
                    return Err(());  
                }
            }
        };

        if index_p_exists {
            let argument_value = get_argument_value("-p", &arguments_to_return);
            if argument_value.is_some() {
                let value = argument_value.unwrap();
                if value.len()!=0 {
                    if value.len() < cz::MINIMUM_LENGTH_OF_USER_PASSWORD || value.len() > cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY {
                        println!("The encryption password, using '-p', was supplied but the given password was not from {} to {} alphanumeric characters in length (-h for help).", cz::MINIMUM_LENGTH_OF_USER_PASSWORD ,cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
                        return Err(());  
                    }
                }
            }
        };

        if index_f_exists {
            let argument_value = get_argument_value("-f", &arguments_to_return);       
            if argument_value.is_some() {
                let mut value = argument_value.unwrap();                
                if value.len() == 0 {
                    println!("The file you wish to send using argument '-f' was not supplied (-h for help).");
                    return Err(());
                } else if value.starts_with(".\\") || value.starts_with("./") {
                    value.replace_range(..2, "");
                    arguments_to_return = set_argument_value("-f", &value, &mut arguments_to_return);
                    println!("Name changed to {}.", value);
                } else if value.starts_with(".") {
                    println!("The file you wish to send using argument '-f' may not start with a '.' character (-h for help).");
                    return Err(());
                }
            } else {
                println!("Argument error for '-f' (-h for help).");
                return Err(());  
            }
        };

        if index_c_exists {    
            let argument_value = get_argument_value("-c", &arguments_to_return);
            if argument_value.is_some() {
                let value = argument_value.unwrap();
                if value.len() == 0 {
                    println!("No code was given to the argument '-c' (-h for help).");
                    return Err(());  
                } else if !index_p_exists {
                    println!("If argument '-c' is supplied then argument '-p' must also be supplied (-h for help).");
                    return Err(());  
                }
            } else {
                println!("Argument error for '-c' (-h for help).");
                return Err(());  
            }
        };

        if index_registration_key_exists {
            let argument_value = get_argument_value("--regkey", &arguments_to_return);
            if argument_value.is_some() {
                let value = argument_value.unwrap();
                if value.len() == 0 {
                    println!("No value was given to the argument '--regkey' (-h for help).");
                    return Err(());  
                }
            } else {
                println!("Argument error for '--regkey' (-h for help).");
                return Err(());  
            }
        }

        if index_display_exists && command != crate::enums::MessageType::Get {
            println!("Argument '--display' may only be supplied when doing a 'get' (-h for help)");
            return Err(());
        }
    }

    Ok((command, arguments_to_return))
}

/// Get the index of the argument we want in the command line arguments array 
fn get_argument(arg_id: &str, args_array: &Vec<String>) -> Option<Argument> {
    let arg_index = args_array.iter().position(|arg| arg.starts_with(arg_id)).unwrap_or(0);    

    if arg_index >= 1 {        
        let result = get_arg_value(arg_index, args_array);
        if result.is_ok() {
            let result_argument = Argument::new(arg_id, &result.unwrap());
            if result_argument.is_ok() {
                return Some(result_argument.unwrap());
            } else {
                println!("{}", result_argument.unwrap_err());
                std::process::exit(1);
            }
        }
    }

    None
}

/// Get a specific argument switches' value
fn get_arg_value(index: usize, arg_array: &Vec<String>) -> Result<String,()> {    
    if arg_array.len() > index + 1 {
        let next_value = arg_array[index + 1].to_owned();
        if next_value.starts_with("-") {
            Ok(String::new())
        } else {            
            Ok(next_value)
        }
    } else {
        return Ok(String::new());
    }
}

/// Set an arguments value
pub fn set_argument_value(argument_name: &str, new_value: &str, arguments: &mut Vec<Argument>) -> Vec<Argument> {
    arguments.retain(|arg| arg.name != String::from(argument_name));

    arguments.push(Argument{name: String::from(argument_name), value: String::from(new_value)});    

    arguments.clone()
}

/// Get the value of an argument if it was supplied on the command line
pub fn get_argument_value(argument_name: &str, arguments: &Vec<Argument>) -> Option<String> {
    for argument in arguments {
        if argument.name == argument_name {
            return Some(argument.value.to_owned());
        }
    };
    None
}

/// Builds a string containing the Help screen text.
///
/// Displayed when the user runs the client with no arguments, makes a mistake with arguments entered or explicitly invoking Help.
fn get_help_screen_text() -> String {
    let mut help_screen_text = format!("");
    help_screen_text = help_screen_text + "     ____                 _____         _    ";
    help_screen_text = help_screen_text + "\n    / ___|_ __ _   _  ___|__  /___ _ __| | __";
    help_screen_text = help_screen_text + "\n   | |   | '__| | | |/ _ \\ / // _ \\ '__| |/ /";
    help_screen_text = help_screen_text + "\n   | |___| |  | |_| | (_) / /|  __/ |  |   < ";
    help_screen_text = help_screen_text + "\n    \\____|_|   \\__, |\\___/____\\___|_|  |_|\\_\\";
    help_screen_text = help_screen_text + "\n               |___/";
    help_screen_text = help_screen_text + "\n\nTransfer files to anywhere in the world using strong end-to-end encryption.";
    help_screen_text = help_screen_text + "\nWritten by Dean Komen 2020";
    help_screen_text = help_screen_text + "\n\nLICENSE: Dimension 15 General Public License v1 <https://www.dimension15.co.za/>";
    help_screen_text = help_screen_text + "\nSource code: https://www.gitlab.com/dkomen/cryozerk";
    help_screen_text = help_screen_text + "\nWebsite: https://www.dimension15.co.za/products/cryozerk";
    help_screen_text = help_screen_text + "\n\nCommands:";
    help_screen_text = help_screen_text + &format!("\n    register: Register the client for secure server communications {}", Command::printed_arguments(crate::enums::MessageType::RSARegistration));
    help_screen_text = help_screen_text + &format!("\n        keys: Show a list of all the keys available for use in the configuration file {}", Command::printed_arguments(crate::enums::MessageType::Keys));
    help_screen_text = help_screen_text + &format!("\n        send: Send a file to the server {}", Command::printed_arguments(crate::enums::MessageType::Send));
    help_screen_text = help_screen_text + &format!("\n         get: Retrieve a previously sent file from the server {}", Command::printed_arguments(crate::enums::MessageType::Get));
    help_screen_text = help_screen_text + &format!("\n      update: Update the file contents on the server of a previously sent file {}", Command::printed_arguments(crate::enums::MessageType::Update));
    help_screen_text = help_screen_text + &format!("\n        list: Retrieve a list of all files uploaded for your access key {}", Command::printed_arguments(crate::enums::MessageType::List));
    help_screen_text = help_screen_text + &format!("\n      delete: Delete a file with a specific unique code from the server {}", Command::printed_arguments(crate::enums::MessageType::Delete));
    help_screen_text = help_screen_text + &format!("\n         spk: Send your public key to the server for a remote client to get {}", Command::printed_arguments(crate::enums::MessageType::SendPublicKey));
    help_screen_text = help_screen_text + &format!("\n         gpk: Get a remote clients' public key from the server {}", Command::printed_arguments(crate::enums::MessageType::GetPublicKey));
    help_screen_text = help_screen_text + &format!("\n         dpk: Delete a remote clients' public key from local storage {}", Command::printed_arguments(crate::enums::MessageType::DeleteLocalPublicKey));
    help_screen_text = help_screen_text + &format!("\n         lpk: List all the remote clients' public keys in local storage {}", Command::printed_arguments(crate::enums::MessageType::ListLocalPublicKeys));
    //help_screen_text = help_screen_text + "\n     upgrade: Retrieve the lastest client from the server";
    help_screen_text = help_screen_text + "\n\nArguments:";
    help_screen_text = help_screen_text + "\nArgument values supplied must be inside quotes if they contain special characters - '3eCRet!file@.txt'";
    help_screen_text = help_screen_text + "\n         -h: 'Help': Display this help screen";
    help_screen_text = help_screen_text + "\n         -f: 'Input file': The file you wish to upload/send";
    help_screen_text = help_screen_text + &format!("\n         -p: 'Password': A {0} to {1} character password 'regex==^[ -~]{{{0},{1}}}$' you wish", cz::MINIMUM_LENGTH_OF_USER_PASSWORD, cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
    help_screen_text = help_screen_text + "\n             to encrypt your data with.";
    help_screen_text = help_screen_text + "\n             If you only indicate '-p' then you will be prompted for a password - as you type the";
    help_screen_text = help_screen_text + "\n             password will not be echo'd to the screen";
    help_screen_text = help_screen_text + "\n         -c: 'Random download code': A code will be returned back to you after a 'send' was done.";
    help_screen_text = help_screen_text + "\n             This code is required if you want to get, delete, update etc the file again";
    help_screen_text = help_screen_text + &format!("\n         -d: 'File description': You will be asked to enter a single line description of 1 to {}", cz::MAXIMUM_LENGTH_DESCRIPTION_FIELD);
    help_screen_text = help_screen_text + "\n             characters for your upload";
    help_screen_text = help_screen_text + "\n         -v: 'Verbose mode': The client will run in a more verbose mode where more status messages";
    help_screen_text = help_screen_text + "\n             are displayed on the console. In Microsoft Windows verbose mode is always enabled";
    help_screen_text = help_screen_text + "\n             by default";
    help_screen_text = help_screen_text + "\n         -x: Do not delete': This tells the server not to delete an uploaded file when it is retrieved";
    help_screen_text = help_screen_text + "\n             with the 'get' command. This argument is used in conjunction with the 'send' command and";
    help_screen_text = help_screen_text + "\n             can be used for long term-storage if allowed by the server).";
    help_screen_text = help_screen_text + "\n             To delete a file you use the '-d' argument";
    help_screen_text = help_screen_text + "\n         -l: 'Upload into layer or list contents': Upload a file into a specific hierarchy layer (case";
    help_screen_text = help_screen_text + "\n             sensitive)";
    help_screen_text = help_screen_text + "\n             Layers are akin to typical computer file system directories";
    help_screen_text = help_screen_text + "\n             When foing a 'list' command then if you specify '-l' with no value then a list of layers";
    help_screen_text = help_screen_text + "\n             for your 'access-key'";
    help_screen_text = help_screen_text + "\n             will be retrieved. Only alphanumeric characters[A..Z, a..z, 0..9] and '/' allowed";
    help_screen_text = help_screen_text + "\n             If you used with the 'list' command then it will return a list of uploads in and/or";
    help_screen_text = help_screen_text + "\n             below the supplied layer";
    help_screen_text = help_screen_text + "\n--community: 'Use a non-default specific access key': Perform commands using the given `access_key`";
    help_screen_text = help_screen_text + "\n             instead of the default `access_key`";
    help_screen_text = help_screen_text + "\n             Access keys are denoted with a 'community_id' number as indicated in the cz_client.json";
    help_screen_text = help_screen_text + "\n             configuration file, \"1\" == default key. This allows a client to belong to more than one";
    help_screen_text = help_screen_text + "\n             community";
    help_screen_text = help_screen_text + "\n   --pkname: 'Public RSA key name': used to specify the name of a different clients' RSA public key";
    help_screen_text = help_screen_text + "\n             This name is unique accross all cryozerk access_keys`";
    help_screen_text = help_screen_text + "\n       --px: 'Additional password given': An additional password may must be supplied to prevent";
    help_screen_text = help_screen_text + "\n             actions like delete";
    help_screen_text = help_screen_text + "\n   --nohash: 'Do not generate a hash': Do not generate a hash of the file bytes to being sent/updated";
    help_screen_text = help_screen_text + "\n             This is typically only used with large files where hash generation may be slow.";
    help_screen_text = help_screen_text + "\n  --display: 'Display file contents': Instead of saving a retrieved file to disk rather only display";
    help_screen_text = help_screen_text + "\n             its' contents";
    //help_screen_text = help_screen_text + "\n    -s    : To see a lovely smile";
    help_screen_text = help_screen_text + "\n\nExamples";
    help_screen_text = help_screen_text + "\n    1. To create a new local configuration file with initial default settings just run the executable";
    help_screen_text = help_screen_text + "\n       `cryozerk`";
    help_screen_text = help_screen_text + "\n    2. Register with a CryoZerk server - this enables RSA handshaking with the server for secure";
    help_screen_text = help_screen_text + "\n       communications over the wire";
    help_screen_text = help_screen_text + "\n       `cryozerk register`";
    help_screen_text = help_screen_text + "\n       You will need the CryoZerk servers' IP address and port and update them in the new config";
    help_screen_text = help_screen_text + "\n       file (" + &cz::CONFIG_FILE_NAME_CLIENT + ") created in step 1";
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n    3. Send file Specifications.docx and supply a password and description when prompted";
    help_screen_text = help_screen_text + "\n       `cryozerk send -f Specifications.pdf -p -d`";
    help_screen_text = help_screen_text + "\n       You will receive a code and password that may be used to retreive the file again with the 'get'";
    help_screen_text = help_screen_text + "\n       command (example: `cryozerk get -c cryptom734 -p [and your password]`). This code (cryptom734)";
    help_screen_text = help_screen_text + "\n       and password (SuperSecretPassword) must be securely given to anyone that you want to download";
    help_screen_text = help_screen_text + "\n       the file";
    help_screen_text = help_screen_text + "\n    4. Send file Specifications.pdf with your own password for server side encryption (must be a 12";
    help_screen_text = help_screen_text + &format!("\n       to {} character alphanumeric password)", cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
    help_screen_text = help_screen_text + "\n       `cryozerk send -f Specifications.pdf -p SuperSecretPassword`";
    help_screen_text = help_screen_text + "\n    5. Send file Specifications.docx and supply a password and allow only client named JohnSmith";
    help_screen_text = help_screen_text + "\n       to extract the file on download";
    help_screen_text = help_screen_text + "\n       `cryozerk send -f Specifications.pdf -p 's#ecretEnryP0ss235' --pk JohnSmith`";
    help_screen_text = help_screen_text + "\n    6. Get Specifications.pdf from the CryoZerk server";
    help_screen_text = help_screen_text + "\n       `cryozerk get -c JohnG957 -p SuperSecretPassword`";
    help_screen_text = help_screen_text + "\n    7. Send file BankInfo.zip and put it into a layer named `documents/banking`";
    help_screen_text = help_screen_text + "\n       `cryozerk send -f BankInfo.zip -p -l 'documents/banking'`";
    help_screen_text = help_screen_text + "\n       This will effectively create a descriptive path named: documents/banking/[unique code]";
    help_screen_text = help_screen_text + "\n    8. Send file BankInfo.zip and allow anyone with password to retreive it, but only a new additional";
    help_screen_text = help_screen_text + "\n       password may delete it";
    help_screen_text = help_screen_text + "\n       `cryozerk send -f BankInfo.zip -p --px P0sswor4ForD3l3tes";
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n    9. Retrieve a list of all uploaded files";
    help_screen_text = help_screen_text + "\n       `cryozerk list`";
    help_screen_text = help_screen_text + "\n    10. Retrieve a list of all uploaded files in a specific virtual layer";
    help_screen_text = help_screen_text + "\n       `cryozerk list -l documents`";
    help_screen_text = help_screen_text + "\n    11. Retrieve a list of all uploaded files in a specific virtual layer and in a community";
    help_screen_text = help_screen_text + "\n       denoted by an access key retreived from the local configuration file: cz_client.json";
    help_screen_text = help_screen_text + "\n       `cryozerk list -l documents -k 2`";
    help_screen_text = help_screen_text + "\n    12.Retrieve a list of all uploaded files in a specific virtual layer and below";
    help_screen_text = help_screen_text + "\n       `cryozerk list -l 'documents/'`";
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n    13. Send your personal public key to the server";
    help_screen_text = help_screen_text + "\n       `cryozerk spk -p 'REg%^3dfgdhdS'`";
    help_screen_text = help_screen_text + "\n    14. Import a clients' personal RSA public key from the server and name it JohnsKey";
    help_screen_text = help_screen_text + "\n       `cryozerk gpk -c codeX123 -p 'REg%^3dfgdhdS' --pkname JohnsKey`";
    help_screen_text = help_screen_text + "\n    15. Send a file that only John can decrypt";
    help_screen_text = help_screen_text + "\n       `cryozerk send -f Specifications.docx -p 'SuperSecetP@ssw0rd' --pkname JohnsKey`";

    help_screen_text = help_screen_text + "\n";

    help_screen_text
}

/// An easter egg
fn get_smile_screen_text() -> String {
    let mut help_screen_text = format!("\n");
    help_screen_text = help_screen_text + "\n    (ʘ‿ʘ)";
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n\n\n";
    help_screen_text
}