/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Enums specific to the client.
//!
//! Other enums are in the [lib_cryozerk_streams](lib_cryozerk_streams) library

#[derive(Debug, Clone, PartialEq)]
/// The type of message being used for client to and server communications.
pub enum MessageType {
    Send=1,
    Update=2,
    Get=3,
    List=4,
    Delete=5,
    /// Display a list off all the clients local access keys
    Keys=6,
    RSARegistration=10,
    Upgrade=11,
    /// Send the clients p2p public key to the server
    SendPublicKey=50,
    /// Import a p2p clients public key from the server
    GetPublicKey=51,
    /// Delete an existing public key of a remote client from local configuration file
    DeleteLocalPublicKey=52,
    ListLocalPublicKeys=53,
    ///If the current Linux user is root then do the log file creation for user
    Install=100
}