/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! The CryoZerk client
//!
//! Runs on a users machine. All encrpytions of upload files are performed on the client and their personal passwords are never uploaded to the server.

extern crate lazy_static;
extern crate tokio;
use ansi_term::{Colour, Style};
use cz::display;
use rpassword;
use std::io;
use users::{get_user_by_uid, get_current_uid};
use lib_cryozerk_streams as cz;
use lib_cryozerk_streams::log;
use lib_cryozerk_streams::enums::EncryptionScheme as scheme;
mod arguments;
pub mod enums;
pub mod structs;


/// The main start function of the client
#[tokio::main]
async fn main() {
    
    if !did_initial_install() {
        println!("Continue..");
        unsafe{
            cz::APPLICATION_INSTANCE = cz::enums::Application::Client;
        }
        cz::display!("CryoZerk client", cz::VERSION_INFO);

        println!("Communities:");
        for key in cz::CONFIGURATION_CLIENT.access_keys.iter() {
            cz::display!(&format!("    {}: {}", key.local_community_id, key.access_key));
        }
        if cfg!(unix) {
            println!("Logging: {}", unsafe{match cz::ENABLE_LOGGING {true=>Colour::Green.paint("Enabled"), false => Colour::Yellow.paint("Disabled")}});
        } else {
            println!("Logging: {}", unsafe{match cz::ENABLE_LOGGING {true=>"Enabled", false => "Disabled"}});
        }
        
        let result_arguments =  arguments::get_arguments();
        if result_arguments.is_ok() {
            let mut arguments = result_arguments.unwrap();
            unsafe {
                if arguments::get_argument_value("-v", &arguments.1).is_some() {
                    cz::QUIET_MODE = 0;
                }
            }

            // Get password (-p)
            let option_user_password = arguments::get_argument_value("-p", &arguments.1);
            let needs_password: bool = match enums::MessageType::from(arguments.0.clone()) {
                enums::MessageType::Delete | enums::MessageType::Get | enums::MessageType::Send | 
                enums::MessageType::SendPublicKey | enums::MessageType::GetPublicKey => true,
                _ => false
            };
            if option_user_password.is_none() && needs_password {
                cz::display!("A password was not supplied, please use the '-p' argument");
                return;
            } else if option_user_password.is_some() && option_user_password.unwrap()==String::new() {
                let option_cloaked_user_password = enter_password("Please enter a password", enums::MessageType::from(arguments.0.clone())==enums::MessageType::Send);
                if option_cloaked_user_password.is_some(){
                    arguments::set_argument_value("-p", &option_cloaked_user_password.unwrap(), &mut arguments.1);
                } else {
                    return;
                }
            }

            // Get protection password (--px)
            let option_protected_password = arguments::get_argument_value("--px", &arguments.1);
            if option_protected_password.is_some() && option_protected_password.clone().unwrap()==String::new() {
                let option_cloaked_protected_password = enter_password("Please enter a delete protection password", enums::MessageType::from(arguments.0.clone())==enums::MessageType::Send);
                if option_cloaked_protected_password.is_some(){
                    arguments::set_argument_value("--px", &option_cloaked_protected_password.unwrap(), &mut arguments.1);
                } else {
                    return;
                }
            }

            // Get p2p name (--pkname)
            let option_p2p_client_name = arguments::get_argument_value("--pkname", &arguments.1);

            // Get access_key (--community)
            let local_local_community_id: u16;
            let option_community = arguments::get_argument_value("--community", &arguments.1);
            let option_local_local_community_id = get_access_key(&option_community);

            if option_local_local_community_id.is_none() {
                cz::display!("Could not find the requested community id in the configuration file");
                return;
            } else {
                local_local_community_id = option_local_local_community_id.unwrap();
            }

            match enums::MessageType::from(arguments.0.clone()) {
                enums::MessageType::Install => { }
                enums::MessageType::Send | enums::MessageType::SendPublicKey => {
                    let option_password = arguments::get_argument_value("-p", &arguments.1);
                    let option_code = arguments::get_argument_value("-c", &arguments.1);
                    let option_description = arguments::get_argument_value("-d", &arguments.1);
                    let option_layer = arguments::get_argument_value("-l", &arguments.1);
                    let drop_on_retrieval = arguments::get_argument_value("-x", &arguments.1).is_none();
                    let generate_hash = arguments::get_argument_value("--nohash", &arguments.1).is_none();

                    if option_description.is_some() && option_description.unwrap()==String::new() {
                        let option_new_description = enter_description();
                        if option_new_description.is_some(){
                            arguments::set_argument_value("-d", &option_new_description.unwrap(), &mut arguments.1);
                        } else {
                            return;
                        }
                    }

                    let user_password_vec: Vec<u8>;
                    if option_password.is_some() {
                        user_password_vec = option_password.unwrap().as_bytes().to_vec();
                    } else {
                        cz::display!("Error: No password was supplied. See argument '-p'");
                        return;
                    }
                    let protected_password = if option_protected_password.is_some() {
                        option_protected_password.unwrap()
                    } else {
                        String::new()
                    };

                    let protected_password_vec = protected_password.as_bytes().to_vec();
                    let option_description = arguments::get_argument_value("-d", &arguments.1);
                    let description = match option_description.is_some() {
                        true => option_description.unwrap(),
                        false => "".into()
                    };
                    let layer = match option_layer.is_some() {
                        true => option_layer.unwrap(),
                        false => "/".into()
                    };

                    let result = match enums::MessageType::from(arguments.0) {
                        enums::MessageType::Send => 
                        {
                            let option_file_name = arguments::get_argument_value("-f", &arguments.1);
                            if option_file_name.is_some() {
                                send(local_local_community_id, &option_file_name.unwrap(), option_code, &layer, &description, &drop_on_retrieval, 
                                user_password_vec, protected_password_vec, option_p2p_client_name, 
                                generate_hash, on_benchmarks_handler).await
                            } else {
                                Err("No file name to send was supplied (-f)".into())
                            }
                        },
                        enums::MessageType::SendPublicKey => send_client_public_key(local_local_community_id, option_code, &layer, &description, &drop_on_retrieval, user_password_vec, protected_password_vec, option_p2p_client_name, generate_hash, on_benchmarks_handler).await,
                        _ => Err("Unhandled SEND type!".into())
                    };

                    if result.is_err() {
                        cz::display!("Send failed", &result.unwrap_err());
                    }
                },
                enums::MessageType::Update => {
                    let option_file_name = arguments::get_argument_value("-f", &arguments.1);
                    let option_code = arguments::get_argument_value("-c", &arguments.1);
                    let option_user_password = arguments::get_argument_value("-p", &arguments.1);
                    let generate_hash = arguments::get_argument_value("--nohash", &arguments.1).is_none();
                    
                    let protected_password = if option_protected_password.is_some() {
                        arguments::get_argument_value("--px", &arguments.1).unwrap()
                    } else {
                        String::new()
                    };
                    if option_file_name.is_some() && option_code.is_some() && option_user_password.is_some() {
                        let result = update(local_local_community_id, &option_file_name.unwrap(), &option_code.unwrap(), 
                        &option_user_password.unwrap(), &protected_password, 
                        generate_hash, on_benchmarks_handler).await;
                        if result.is_err() {
                            cz::display!("Update failed", &result.unwrap_err());
                        }
                    } else {
                        cz::display!("Values for the minimum command set of line arguments (-f, -c and -p) must be supplied");
                    }
                },
                enums::MessageType::Get | enums::MessageType::GetPublicKey => {
                    let option_code = arguments::get_argument_value("-c", &arguments.1);
                    let option_user_password = arguments::get_argument_value("-p", &arguments.1);
                    let user_password = option_user_password.unwrap();
                    let protected_password = if option_protected_password.is_some() {
                        arguments::get_argument_value("--px", &arguments.1).unwrap()
                    } else {
                        String::new()
                    };
                    let result = match enums::MessageType::from(arguments.0) {
                        enums::MessageType::Get => get(local_local_community_id, &option_code.unwrap(), &user_password, &protected_password, &arguments::get_argument_value("--display", &arguments.1).is_some(), on_benchmarks_handler).await,
                        enums::MessageType::GetPublicKey => get_public_key(local_local_community_id, &option_code.unwrap(), option_p2p_client_name, &user_password, &protected_password, on_benchmarks_handler).await,
                        _ => Err("Unhandled GET type!".into())
                    };

                    if result.is_err() {
                        cz::display!("Get failed", &result.unwrap_err());
                    }
                },
                enums::MessageType::List => {
                    let mut layer: String = arguments::get_argument_value("-l", &arguments.1).unwrap_or("".into());
                    if layer == String::new() { layer="ALL_LAYERS".into(); }
                    let result = cz::client::get_list_of_uploads(&local_local_community_id, &layer).await;
                    if result.is_err() {
                        cz::display!("Retrieving the list of uploads failed", &result.unwrap_err());
                    } else {
                        let mut list_item = result.unwrap();
                        list_item.list.sort_by(|a, b| a.layer.cmp(&b.layer));
                        if list_item.list.len() > 0 {
                            if arguments::get_argument_value("-l", &arguments.1).is_some() && arguments::get_argument_value("-l", &arguments.1).unwrap()==String::new() {
                                list_item.list.dedup_by(|a,b|a.layer==b.layer);
                                for item in list_item.list.iter() {
                                    let layer = cz::general::strip_zeros(&item.layer);
                                    let layer_string = std::str::from_utf8(&layer).unwrap();
                                    if layer.len() == 0 {
                                        println!("Layer: /", );
                                    } else {
                                        println!("Layer: {}", layer_string);
                                    }
                                }
                            } else {
                                if cfg!(windows) {
                                    println!();
                                }
                                let mut total_bytes_length: usize = 0;
                                for item in list_item.list.iter() {
                                    total_bytes_length += item.bytes_length as usize;
                                    if item.unique_code.len()>0 {
                                        if cfg!(unix) {
                                            println!{"{}", Colour::Fixed(12).paint("------------------------------")};
                                        } else {
                                            println!{"------------------------------"};
                                        }
                                        println!("    Description: {}", std::str::from_utf8(&cz::general::strip_zeros(&item.description)).unwrap());
                                        if cfg!(unix) {
                                            println!("    Unique code: {}", Colour::Cyan.bold().paint(std::str::from_utf8(&cz::general::strip_zeros(&item.unique_code)).unwrap()));
                                        } else {
                                            println!("    Unique code: {}", std::str::from_utf8(&cz::general::strip_zeros(&item.unique_code)).unwrap());
                                        }
                                        println!("          Layer: {}", std::str::from_utf8(&cz::general::strip_zeros(&item.layer)).unwrap());
                                        println!("   Size on disk: {}", cz::general::format_count_to_string(item.bytes_length as usize));
                                        println!("        Created: {}", std::str::from_utf8(&cz::general::strip_zeros(&item.creation_time_stamp)).unwrap());
                                        println!(" Auto delete on: {}", std::str::from_utf8(&cz::general::strip_zeros(&item.auto_deletion_time_stamp)).unwrap());
                                        println!("      Protected: {}", match item.protected { true => Colour::Green.bold().paint("Yes").to_string(), _ => "No".into()} );
                                        if cfg!(unix) {
                                            println!("Delete on 'get': {}", match item.drop_on_retrieval { true => Style::default().paint("Yes"), _ => Colour::Green.bold().paint("No")} );
                                        } else {
                                            println!("Delete on 'get': {}", match item.drop_on_retrieval { true => "Yes", _ => "No"} );
                                        }                                    
                                    }
                                }
                                println!();
                                println!("Listing {} of {}", list_item.list.len()-list_item.list.iter().filter(|c|c.unique_code.len()==0).count(), list_item.list.len());
                                println!("Total size on server: {}", cz::general::format_count_to_string(total_bytes_length));
                            }
                        } else {
                            cz::display!("No uploads found")
                        }
                    }
                },
                enums::MessageType::Keys => {
                    if cfg!(windows) {
                        println!();
                    } 

                    for item in cz::CONFIGURATION_CLIENT.access_keys.iter() {
                        if cfg!(unix) {
                            println!{"{}", Colour::Fixed(12).paint("------------------------------")};
                        } else {
                            println!{"------------------------------"};
                        }
                        if item.local_community_id==1 {
                            if cfg!(unix) {
                                println!("  Community id: {} ({})", item.local_community_id, Colour::Yellow.paint("default"));
                            } else {
                                println!("  Community id: {}(default)", item.local_community_id);
                            }
                        } else {
                            println!("  Community id: {}", item.local_community_id);
                        }
                        println!("    Access key: {}", item.access_key);
                        println!("        Server: {}:{}", item.server_ip, item.server_port);
                    }
                    println!();
                    if cfg!(unix) {
                        println!("Registration access keys are {} displayed", Colour::Red.paint("not"));
                    } else {
                        println!("Registration access keys are not displayed");
                    }
                },
                enums::MessageType::Delete => {
                    let option_code = arguments::get_argument_value("-c", &arguments.1);
                    let option_user_password = arguments::get_argument_value("-p", &arguments.1);
                    let unique_code = option_code.unwrap().as_bytes().to_vec();
                    let user_password = option_user_password.unwrap().as_bytes().to_vec();
                    let protected_password = if option_protected_password.is_some() {
                        arguments::get_argument_value("--px", &arguments.1).unwrap()
                    } else {
                        String::new()
                    };

                    let result = cz::client::delete_upload(&local_local_community_id, &unique_code, &user_password, &protected_password.as_bytes().to_vec()).await;
                    if result.is_err() {
                        cz::display!("Delete request failed", &result.unwrap_err());
                    } else {
                        cz::display!(&result.unwrap());
                    }
                },
                enums::MessageType::RSARegistration => {
                    cz::log!("Registering...");
                    build_initial_benchmarks();
                    let _ = get_server_public_key_for_server_communications(&local_local_community_id).await;
                    let _ = get_client_personal_keys();
                },
                enums::MessageType::Upgrade => {
                    cz::log!("Upgrading...");
                    let result = upgrade(local_local_community_id).await;
                    if result.is_err() {
                        cz::display!("Upgrade failed", &result.unwrap_err());
                    }
                },
                enums::MessageType::DeleteLocalPublicKey => {
                    if option_p2p_client_name.is_some() {
                        let pk_name = option_p2p_client_name.unwrap();
                        if pk_name.len() > 0 {
                            let mut updated_keys_struct =  cz::RSA_KEY_PAIRS_CLIENT_PRIVATE.clone();
                            let keys_count = updated_keys_struct.public_rsa_4096_keys.len();
                            updated_keys_struct.public_rsa_4096_keys.retain(|k| k.name!=pk_name);

                            let result = updated_keys_struct.persist(&cz::RSA_KEYS_FILE_NAME_CLIENT_PRIVATE);
                            if result.is_ok() {
                                if keys_count != updated_keys_struct.public_rsa_4096_keys.len() {
                                    cz::display!("Deleted key successfully", pk_name);
                                } else {
                                    cz::display!("Key name not found", pk_name);
                                }
                            } else {
                                cz::display!(&result.unwrap_err());
                            }
                        } else {
                            cz::display!("You did not supply the name of the key to delete, example --pkname 'KeyNameToDelete'");    
                        }
                    } else {
                        cz::display!("You did not supply the name of the key to delete (--pkname)");
                    }
                },
                enums::MessageType::ListLocalPublicKeys => {
                    for key in cz::RSA_KEY_PAIRS_CLIENT_PRIVATE.public_rsa_4096_keys.iter() {
                        cz::display!(&key.name);
                    }
                }
            }
        }
    } else {
        println!("Here");
    }
}

fn did_initial_install() -> bool {
    let mut automated_install_needed = false;
    let result_arguments = arguments::get_arguments();
    if result_arguments.is_ok() {
        let arguments = result_arguments.unwrap();
        if enums::MessageType::from(arguments.0.clone()) == enums::MessageType::Install {
            if cfg!(unix) {
                let user = get_user_by_uid(get_current_uid()).unwrap();
                let user_name = user.name().to_string_lossy();
                if user_name == "root" {
                    if enums::MessageType::from(arguments.0.clone()) == enums::MessageType::Install {
                        automated_install_needed = true;
                        println!();
                        println!("Installation commands that will be executed:");
                        println!("   1) mkdir /var/log/cryozerk");
                        println!("   2) touch /var/log/cryozerk/client-cryozerk.log");
                        println!("   3) chown [user]: /var/log/cryozerk/client-cryozerk.log");
                        println!();
                        println!("What is the name of the user that will be using the cryozerk client?");
                        let mut name = String::new();
                        let result_input = io::stdin().read_line(&mut name);
                        if result_input.is_ok() {
                            let s: Vec<&str> = name.split_terminator('\n').collect();
                            let _ = std::process::Command::new("mkdir").arg("/var/log/cryozerk").spawn().expect("Could not create directory");
                            std::thread::sleep(std::time::Duration::from_millis(850));
                            let _ = std::process::Command::new("touch").arg("/var/log/cryozerk/client-cryozerk.log").spawn().expect("Could not create log file");
                            std::thread::sleep(std::time::Duration::from_millis(850));
                            let _ = std::process::Command::new("chown").arg(s[0]).arg("/var/log/cryozerk/client-cryozerk.log").spawn().expect("Could not change log file owner");
                            println!();
                            println!("Initial installation is complete");
                            println!("You next step is most likely to execute the client again as the usual user");
                            println!("that will use the CryoZerk client so as to create the clients' configuration");
                            println!("files, then after that you may register the client with a CryoZerk server");
                            println!();
                        } else {
                            println!("Not a valid user name");
                        }
                    } else {
                        println!("The CryoCerk client should not be executed as root")
                    }
                } else {
                    println!("To do an automated installation of the cryozerk client you will need to execute");
                    println!("the client either as root directly or as root via sudo: sudo ./cryozerk install");
                }
                std::process::exit(0);
            } else {
                println!("Automated installation is only available for and needed by unix systems");
            }
        }
    } else {
        println!("Command-line arguments error")
    }

    automated_install_needed
}

/// Check that the requested community actually exists in the local configuration file. Default is community with id==1
fn get_access_key(option_community: &Option<String>) -> Option<u16> {
    let option_access_key;
    if option_community.is_some() {
        let community = option_community.clone().unwrap().parse::<u16>().unwrap_or(0);
        option_access_key = cz::CONFIGURATION_CLIENT.access_keys.iter().find(|a| a.local_community_id==community);
    } else {
        //Get default community
        option_access_key = cz::CONFIGURATION_CLIENT.access_keys.iter().find(|a| a.local_community_id==1);
    }

    if option_access_key.is_some() {
        Some(option_access_key.unwrap().local_community_id)
    } else {
        None
    }
}

/// Ask user for a file description
fn enter_description() -> Option<String> {
    println!("Please enter a description for your file (1 to {} characters): ", cz::MAXIMUM_LENGTH_DESCRIPTION_FIELD);
    let mut description: String = "".into();
    let _ =std::io::stdin().read_line(&mut description);

    description.truncate(description.len()-1);
    if description.len() > 1 && description.len() <= cz::MAXIMUM_LENGTH_DESCRIPTION_FIELD {

        Some(description)
    } else {
        println!("No description was given");

        None
    }
}

/// Ask user for a cloaked password
fn enter_password(password_texts: &str, ask_verification: bool) -> Option<String> {
    let mut password: String = "".into();
    println!("{} ({} to {} chars): ", password_texts, cz::MINIMUM_LENGTH_OF_USER_PASSWORD, cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
    let result_password = rpassword::read_password();
    if result_password.is_ok() {
        password = result_password.unwrap();

        if password.len() < cz::MINIMUM_LENGTH_OF_USER_PASSWORD || password.len() > cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY {
            println!("Password must be inclusively between {} and {} alphanumeric characters (-p)", cz::MINIMUM_LENGTH_OF_USER_PASSWORD, cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
            println!("-h for help");
            return None;
        }

        if ask_verification {
            println!("Please retype your password again: ");
            let result_password_verify = rpassword::read_password();
            if result_password_verify.is_ok() {
                let password_verify = result_password_verify.unwrap();
                if password != password_verify {
                    println!("Passwords do not match! Cannot continue.");
                    return None;
                }
            } else {
                println!("Unknown password verification error");
                return None
            }
        }
    }

    Some(password)
}

/// Do not allow compression of a few known compressed formats
fn allow_compression(file_name: &str) -> cz::enums::EnableCompression {
    let formats: Vec<&str> = vec![
        ".z", ".zip", ".7zip", ".7z", ".zz",".rar", ".bz", ".bz2", ".bzip", ".bzip2", ".gz", ".zip", ".sz", ".xz", ".car", ".lzx", ".lzh", ".lza",
        ".mp3", ".ogg",
        ".mp4", ".wmv", ".mov", ".flv", ".webm", ".mkv",
        ".jpg", ".jpeg", ".png", ".tiff", "tif",
        ".deb", ".rpm"
    ];
    let mut file_lower = file_name.to_owned();
    file_lower.make_ascii_lowercase();

    let option = formats.iter().find(
        |f| 
        f.len()<=file_lower.len() && 
        ***f == file_lower[file_lower.len()-f.len()..file_lower.len()]
    );

    if option.is_some() {
        cz::enums::EnableCompression::No
    } else {
        cz::enums::EnableCompression::Yes
    }
}

/// Send a new file to the server for later retrieval
async fn send(local_local_community_id: u16, file_name: &str, option_unique_code: Option<String>, layer: &str, description: &str, drop_on_retrieval: &bool, user_password: Vec<u8>, protected_password: Vec<u8>, 
    option_p2p_client_name: Option<String>, generate_file_hash: bool, on_benchmarks_handler: fn(cz::structs::BenchmarkResult)) -> Result<(), String> {
    let compress = allow_compression(&file_name);
    let mut session = cz::client::send_file(
        local_local_community_id, 
        &file_name, option_unique_code.clone(), &layer, &description, &drop_on_retrieval, 
        &compress, &cz::enums::EncryptionScheme::Rsa4096Aes256, 
        user_password, protected_password,
        option_p2p_client_name, generate_file_hash,
        on_benchmarks_handler
    )
    .await?;

    let data_packet = cz::streams::receive_data_packet(&cz::structs::BytesControl::new(0_usize, cz::MAXIMUM_LENGTH_DATA_PACKET as usize), &cz::LABEL_RECEIVING, unsafe { &cz::APPLICATION_INSTANCE }, &cz::enums::PacketType::Message, &mut session).await?;
    let upload_response = cz::structs::packet_data_types::UploadResponse::from_bytes(&data_packet.data)?;
    let unique_code_vector = cz::general::strip_zeros(&upload_response.unique_code);
    let returned_unique_code = std::str::from_utf8(&unique_code_vector).unwrap();

    if option_unique_code.is_some() && option_unique_code.clone().unwrap()!=returned_unique_code {
        if cfg!(unix) {
            println!("Unique code: {} (Your requested unique code of '{}' was {})", Colour::Cyan.bold().paint(returned_unique_code), option_unique_code.unwrap(), Colour::Red.bold().paint("declined"));
        } else {
            println!("Unique code: {} (Your requested unique code of '{}' was declined)", returned_unique_code, option_unique_code.unwrap());
        }
    } else {
        if cfg!(unix) {
            println!("Unique code: {}", Colour::Cyan.bold().paint(returned_unique_code));
        } else {
            println!("Unique code: {}", returned_unique_code);
        }
    }

    if !drop_on_retrieval {
        display!(&cz::general::vector_stripped_to_string(&upload_response.user_message));
    }

    Ok(())
}

/// Send a new file to the server for later retrieval - password re-encrypted using a remote client's public key
async fn send_client_public_key(local_local_community_id: u16, option_unique_code: Option<String>, layer: &str, description: &str, drop_on_retrieval: &bool, user_password: Vec<u8>, protected_password: Vec<u8>, option_p2p_client_name: Option<String>, 
    generate_file_hash: bool, on_benchmarks_handler: fn(cz::structs::BenchmarkResult)) 
    -> Result<(), String> {
    let mut session = cz::client::send_file_with_public_key(
        local_local_community_id, option_unique_code,
        &layer, &description, &drop_on_retrieval, 
        &cz::enums::EnableCompression::Yes, 
        &cz::enums::EncryptionScheme::Rsa4096Aes256, 
        user_password, protected_password,
        option_p2p_client_name,
        generate_file_hash, on_benchmarks_handler
    )
    .await?;

    let data_packet = cz::streams::receive_data_packet(&cz::structs::BytesControl::new(0_usize, cz::MAXIMUM_LENGTH_DATA_PACKET as usize), &cz::LABEL_RECEIVING, unsafe { &cz::APPLICATION_INSTANCE }, &cz::enums::PacketType::Message, &mut session).await?;
    let upload_response = cz::structs::packet_data_types::UploadResponse::from_bytes(&data_packet.data)?;
    let unique_code_vector = cz::general::strip_zeros(&upload_response.unique_code);
    let unique_code = std::str::from_utf8(&unique_code_vector).unwrap();

    println!("Unique code: {}", unique_code);

    if !drop_on_retrieval {
        display!(&cz::general::vector_stripped_to_string(&upload_response.user_message));
    }

    Ok(())
}

/// Update the file contents of an existing file on the server
async fn update(local_local_community_id: u16, file_name: &str, unique_code: &str, user_password: &str, protected_password: &str, 
    generate_file_hash: bool, on_benchmarks_handler: fn(cz::structs::BenchmarkResult)) 
    -> Result<(), String> {

    let compress = allow_compression(&file_name);
    
    let mut session = cz::client::update_file(&unique_code, local_local_community_id, file_name, &compress,
        &cz::enums::EncryptionScheme::Rsa4096Aes256, user_password.as_bytes().to_vec(), protected_password.as_bytes().to_vec(), 
        generate_file_hash, on_benchmarks_handler).await?;
    
    let data_packet = cz::streams::receive_data_packet(&cz::structs::BytesControl::new(0_usize, cz::MAXIMUM_LENGTH_DATA_PACKET as usize), &cz::LABEL_RECEIVING, unsafe { &cz::APPLICATION_INSTANCE }, &cz::enums::PacketType::Message, &mut session).await?;
    let response = cz::structs::packet_data_types::UploadResponse::from_bytes(&data_packet.data)?;

    std::thread::sleep(std::time::Duration::from_millis(750));
    let response_message = cz::general::vector_stripped_to_string(&response.user_message);
    display!(&response_message);

   Ok(())
}

/// Retrieve a file from the server
async fn get(local_local_community_id: u16, unique_code: &str, user_password: &str, protected_password: &str, display_contents: &bool, on_benchmarks_handler: fn(cz::structs::BenchmarkResult)) -> Result<(), String> {
    let mut session = cz::client::start_new_session(&local_local_community_id, user_password.as_bytes().to_vec(), &cz::enums::EnableCompression::No, &cz::enums::EncryptionScheme::Rsa4096Aes256).await?;
    let file = cz::client::retrieve_file(&unique_code, &user_password, &protected_password, &mut session, on_benchmarks_handler).await?;
    std::thread::sleep(std::time::Duration::from_millis(750));
    if *display_contents {
        println!();
        println!("{}", unsafe {std::str::from_utf8_unchecked(&file.bytes)});
    } else {
        cz::server::write_file(&file);
    }

    Ok(())
}

/// Retrieve a clients' RSA public key from the server and give it unique local name
async fn get_public_key(local_local_community_id: u16, unique_code: &str, option_p2p_client_name: Option<String>, user_password: &str, protected_password: &str, on_benchmarks_handler: fn(cz::structs::BenchmarkResult)) -> Result<(), String> {
    if option_p2p_client_name.is_some() {
        let p2p_client_name = option_p2p_client_name.unwrap();
        if cz::RSA_KEY_PAIRS_CLIENT_PRIVATE.public_rsa_4096_keys.iter().find(|k| k.name==p2p_client_name).is_none() {
            let mut session = cz::client::start_new_session(&local_local_community_id, user_password.as_bytes().to_vec(), &cz::enums::EnableCompression::No, &cz::enums::EncryptionScheme::Rsa4096Aes256).await?;
            let file = cz::client::retrieve_file(&unique_code, &user_password, &protected_password, &mut session, on_benchmarks_handler).await?;
            let new_key = cz::structs::encryption::PublicRSAKey {
                name: p2p_client_name,
                public_key: file.bytes
            };

            let mut updated_keys_struct =  cz::RSA_KEY_PAIRS_CLIENT_PRIVATE.clone();
            updated_keys_struct.public_rsa_4096_keys.push(new_key);

            let _ = updated_keys_struct.persist(&cz::RSA_KEYS_FILE_NAME_CLIENT_PRIVATE)?;

            std::thread::sleep(std::time::Duration::from_millis(500));
            cz::display!("Added new public key to local store successfully");

            Ok(())
        } else {
            Err(format!("Public key name already exists (--pkname): {}", p2p_client_name))
        }
    } else {
        Err(format!("Public key name was not supplied (--pkname)"))
    }
}

/// Retrieve the servers RSA public keys.
///
/// Without retrieving the RSA public keys the client will not be able to establish a session with the server.
async fn get_server_public_key_for_server_communications(local_local_community_id: &u16) -> Option<String> {
    let result = cz::client::register_with_server_for_rsa_communications(&local_local_community_id).await;
    if result.is_ok() {
        cz::display!("Successfully registered for encryped communication with the server.");        
        cz::display!("");
        cz::display!("Your data is now always strongly encrypted using RSA4096 and AES256 during any network");
        cz::display!("communications with the CryoZerk server.");
        return Some(String::from("Success"));
    } else {
        cz::display!("Error: Could not register for RSA communications");
        return None;
    }   
}

/// A clients' personal public and private RSA keys as well as the public keys of other clients for client specific encryptions
fn get_client_personal_keys() {
    let _ = cz::files::get_rsa_keys_client_private();
}


/// Retrieve the latest client from the server.
async fn upgrade(local_local_community_id: u16) -> Result<(), String> {
    let mut session = cz::client::start_new_session(&local_local_community_id, vec![], &cz::enums::EnableCompression::Yes, &cz::enums::EncryptionScheme::Rsa4096Aes256).await?;

    let _ = cz::streams::send_upgrade_message(&mut session).await?;
    let response = cz::streams::receive_data_packet(&cz::structs::BytesControl::new(0_usize, cz::MAXIMUM_LENGTH_DATA_PACKET as usize), &cz::LABEL_RECEIVING, 
    unsafe { &cz::APPLICATION_INSTANCE }, &cz::enums::PacketType::File, &mut session).await?;

    if response.packet_type==cz::enums::PacketType::File {
        let file = cz::structs::packet_data_types::File::from_bytes(response.data)?;
        cz::server::write_file(&file);
    } else if response.packet_type==cz::enums::PacketType::Message {
        return Err(format!("Server response: {}", std::str::from_utf8(&response.data).unwrap()));
    } else {
        return Err(format!("An unknown data packet type was received from the server: {:?}", response.packet_type));
    }

    Ok(())
}

/// When the user tries to do a ```cryozerk register``` then first also run a few benchmarks to use as a starting point for encryption and compression functions.
///
/// These results will be massaged to become more accurate the more the client does encryptions and compressions.
/// They are more specifically used by progress bars which currently are not diplayed on Windows client but are on Unix clients
fn build_initial_benchmarks()
{
    cz::display!("Running initial benchmarks...");
    // Encryption
    cz::display!("Encryption");
    cz::display!("Allocating random byte vector...");
    let mut size: usize;
    if cfg!(debug_assertions) { size = 1024*512; } else { size = 1024*1024*5; }

    let mut random_bytes: Vec<u8> = vec![];
    for _ in 0..size {
        random_bytes.push(cz::general::get_a_random_number_in_range(0, 255) as u8);
    }
    let password_key = cz::encryption::generate_random_vector((cz::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY/2) as u32);
    let salt = cz::encryption::generate_random_vector(cz::LENGTH_OF_ENCRYPTION_SALT as u32);
    let _ = cz::encryption::aes_encrypt_timed(cz::enums::EncryptionScheme::Rsa4096Aes256, &random_bytes, &salt, &password_key, on_benchmarks_handler);
    std::thread::sleep(std::time::Duration::from_millis(750));

    // Compression
    cz::display!("");
    cz::display!("Compression");
    cz::display!("Allocating random byte and word vector...");
    if cfg!(debug_assertions) { size = (1024 * 128) as usize; } else { size = 1024 * 512; }

    random_bytes.clear();
    for _ in 0..size as usize {
        random_bytes.push(cz::general::get_a_random_number_in_range(0, 255) as u8);
        random_bytes.append(&mut cz::general::get_random_word().as_bytes().to_vec());
        random_bytes.append(&mut cz::general::get_random_alphanumeric_characters(5).as_bytes().to_vec());
        random_bytes.append(&mut cz::general::get_random_word().as_bytes().to_vec());
    }
    std::thread::sleep(std::time::Duration::from_millis(750));
    let compressed_bytes = cz::enums::EnableCompression::compress_bytes(&random_bytes, on_benchmarks_handler).unwrap();
    std::thread::sleep(std::time::Duration::from_millis(750));
    let _ = cz::enums::EnableCompression::decompress_bytes(&compressed_bytes, on_benchmarks_handler).unwrap();
    std::thread::sleep(std::time::Duration::from_millis(750));

    cz::display!("Benchmarks done");
    cz::display!("");
}

/// Update the benchmarks struct for encryption\decryption and compression predictions
fn on_benchmarks_handler(benchmark_result: cz::structs::BenchmarkResult) {
    let _ = cz::BENCHMARKS.lock().unwrap().clone();
    let result_file_exists = cz::files::local_file_exists(&cz::BENCHMARK_FILE_NAME);
    if result_file_exists.is_ok() {
        let path = result_file_exists.unwrap();
        let option_string = cz::files::read_from_file_as_string(&path);
        if option_string.is_some() {
            let string = option_string.unwrap();
            let mut new_benchmarks =  serde_json::from_str::<cz::structs::Benchmarks>(&string).unwrap();
            if benchmark_result.result_type == cz::enums::BenchmarkType::Encryption {
                let encryption_scheme = scheme::from_byte(&benchmark_result.scheme);
                match encryption_scheme {
                    scheme::Aes256 | scheme::Rsa4096Aes256 | scheme::Rsa2048Aes256 => {
                        let new_total = ((new_benchmarks.aes_256_bytes_per_second * new_benchmarks.aes_256_counter as f32) + benchmark_result.bytes_per_second) / (new_benchmarks.aes_256_counter + 1) as f32;
                        new_benchmarks.aes_256_counter += 1;
                        new_benchmarks.aes_256_bytes_per_second = new_total;
                    },
                    scheme::Aes128 | scheme::Rsa4096Aes128 | scheme::Rsa2048Aes128 => {
                        let new_total = ((new_benchmarks.aes_128_bytes_per_second * new_benchmarks.aes_128_counter as f32) + benchmark_result.bytes_per_second) / (new_benchmarks.aes_128_counter + 1) as f32;
                        new_benchmarks.aes_128_counter += 1;
                        new_benchmarks.aes_128_bytes_per_second = new_total;
                    },
                    _ => ()
                }
            } else {
                // Compression
                match benchmark_result.result_type {
                    cz::enums::BenchmarkType::Compression => {
                        let new_total = ((new_benchmarks.compression_bytes_per_second * new_benchmarks.compression_count as f32) + benchmark_result.bytes_per_second) / (new_benchmarks.compression_count + 1) as f32;
                        new_benchmarks.compression_count += 1;
                        new_benchmarks.compression_bytes_per_second = new_total;
                    },
                    cz::enums::BenchmarkType::Decompression => {
                        let new_total = ((new_benchmarks.decompression_bytes_per_second * new_benchmarks.decompression_count as f32) + benchmark_result.bytes_per_second) / (new_benchmarks.decompression_count + 1) as f32;
                        new_benchmarks.decompression_count += 1;
                        new_benchmarks.decompression_bytes_per_second = new_total;
                    }
                    _ => ()
                }
            }

            let result_path = cz::files::local_file_exists(&cz::BENCHMARK_FILE_NAME);
            let path: String;
            if result_path.is_ok() {
                path = result_path.unwrap();
                let _ = std::fs::remove_file(path.to_owned());
            } else {
                path = result_path.unwrap_err();
            }
            let serialised_benchmarks = serde_json::to_string_pretty(&new_benchmarks).unwrap();
            if !cz::files::write_to_file(&path, &serialised_benchmarks.as_bytes().to_vec(), false) {
                cz::log!("Could not write to benchmarks file", path);
            }
            drop(new_benchmarks);
        } else {
            cz::log!("Could not read string for deserialisation from benchmarks file");
        }
    } else {
        cz::log!("Could not read benchmarks file", result_file_exists.unwrap_err());
    }
}